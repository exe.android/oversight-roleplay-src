﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.inventory;

namespace oversight.interfaces
{
    /// <summary>
    /// Интерфейс объекта, который может хранить в себе предметы
    /// </summary>
    interface IStoringObject
    {
        /// <summary>
        /// Сообщает, может ли объект вместить в себя указанный предмет
        /// </summary>
        /// <param name="item">Предмет, который нужно поместить в объект</param>
        /// <returns></returns>
        bool GetIsItemCanBeCarried(Item item);

        /// <summary>
        /// Возвращает количество возможно переносимого веса
        /// </summary>
        /// <returns></returns>
        int GetCarryingCapacity();

        /// <summary>
        /// Устанавливает имя объекта для клиента
        /// </summary>
        void SetUpPresentation();
    }
}

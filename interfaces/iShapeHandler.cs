﻿using GTANetworkServer;
using GTANetworkShared;
using oversight.system;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.interfaces
{
    interface iShapeHandler
    {
        void onEntityEnterColShapeHandler(ColShape shape, NetHandle entity);
        void onEntityExitColShapeHandler(ColShape shape, NetHandle entity);
        void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, Data.eMoveType type);
    }
}

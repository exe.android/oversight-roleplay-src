﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using oversight.system;
using MySql.Data.MySqlClient;
using GTANetworkShared;
using GTANetworkServer.Constant;
using oversight.entity;
using GTANetworkServer;
using oversight.entity.bank;
using oversight.entity.houses;
using oversight.exceptions;
using Entity = oversight.entity.Entity;

namespace oversight.entityGroup
{
    delegate void HousesLoadedEvent();

    class Houses
    {
        private static List<House> _Houses = new List<House>();

        public static HousesLoadedEvent OnHousesLoaded;

        /// <summary>
        /// Добавить дом в коллекцию
        /// </summary>
        /// <param name="house"></param>
        public static void Add(House house)
        {
            _Houses.Add(house);
        }

        /// <summary>
        /// Отдает дом по его ID
        /// </summary>
        /// <param name="houseId"></param>
        /// <returns></returns>
        public static House Get(int houseId)
        {
            return _Houses.Find(h => h.HouseId == houseId);
        }

        /// <summary>
        /// Отдает всю коллекцию домов
        /// </summary>
        /// <returns></returns>
        public static List<House> GetAllHouses()
        {
            return _Houses;
        }

        /// <summary>
        /// Отдает все дома, которые можно арендовать
        /// </summary>
        /// <returns></returns>
        public static List<RentalHouse> GetAllRentalHouses()
        {
            List<RentalHouse> list = new List<RentalHouse>();
            foreach (House house in GetAllHouses())
            {
                if (house.GetType() == typeof(RentalHouse))
                    list.Add((RentalHouse) house);
            }
            return list;
        }

        /// <summary>
        /// Отдает все обычные дома, которыми могут владеть как игроки, так и бизнесы
        /// </summary>
        /// <returns></returns>
        public static List<CommonHouse> GetAllCommonHouses()
        {
            List<CommonHouse> list = new List<CommonHouse>();
            foreach (House house in GetAllHouses())
            {
                if (house.GetType() == typeof(CommonHouse))
                    list.Add((CommonHouse)house);
            }
            return list;
        }

        /// <summary>
        /// Загружает все дома, помещает в коллекцию
        /// </summary>
        /// <returns></returns>
        public static void LoadHouses()
        {
            Main.Api.consoleOutput("[SERVER] Loading houses");

            MySqlCommand command = new MySqlCommand("SELECT * FROM `houses`", DbConnection.GetInstance().Connection);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> data = DbConnection.GetRowData(reader);
                    string houseType = (string) data.Get("type");
                    Vector3 enterPosition = new Vector3((float)data.Get("enter_pos_x"), (float)data.Get("enter_pos_y"), (float)data.Get("enter_pos_z"));
                    int ownerId = (int)(uint)data.Get("owner_id");
                    eEntityType ownerType =
                        (eEntityType) Enum.Parse(typeof(eEntityType), (string) data.Get("owner_type"));
                    int comfortLevel = (int) data.Get("comfort_level");

                    House baseHouse = null;
                    if (houseType == "common")
                    {
                        House house = new CommonHouse((int)(uint)data.Get("id"), enterPosition);
                        house.SetOwnState(ownerId == 0 ? House.EOwnState.Free : House.EOwnState.InOwn);
                        baseHouse = house;
                        
                    }

                    if (houseType == "rental")
                    {
                        RentalHouse house = new RentalHouse((int)(uint)data.Get("id"), enterPosition);
                        baseHouse = house;
                    }

                    if (baseHouse == null) continue;
                    baseHouse.SetOwnerId(ownerId);
                    baseHouse.SetOwnerType(ownerType);
                    baseHouse.SetComfortLevel(comfortLevel);
                }
            }
            Main.Api.consoleOutput(string.Format("[SERVER] {0} houses loaded", _Houses.Count));
            reader.Close();

            OnHousesLoaded();
        }

        /// <summary>
        /// Возвращает дом, который арендует игрок
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static RentalHouse GetPlayerRentingHouse(Player player)
        {
            try
            {
                return GetAllRentalHouses().First(house => house.RentingPlayers.Contains(player.Id));
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public static RentalHouse GetPlayerRentingHouse(int playerId)
        {
            try
            {
                return GetAllRentalHouses().First(house => house.RentingPlayers.Contains(playerId));
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Отдает дома, которыми владеет игрок
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static List<CommonHouse> GetPlayerOwningHouses(Player player)
        {
            return
                GetAllCommonHouses()
                    .Where(house => house.OwnerId == player.Id && house.OwnerType == eEntityType.player)
                    .ToList();
        }

        public static List<CommonHouse> GetPlayerOwningHouses(int playerId)
        {
            return
                GetAllCommonHouses()
                    .Where(house => house.OwnerId == playerId && house.OwnerType == eEntityType.player)
                    .ToList();
        }

        /// <summary>
        /// Ищет дом, в котором игрок может спауниться
        /// Это может быть как дом, которым игрок владеет, так и дом, который игрок арендует
        /// </summary>
        /// <returns></returns>
        public static House GetPlayerSpawningHouse(Player player)
        {
            List<CommonHouse> playerHouses = GetPlayerOwningHouses(player);
            try
            {
                return playerHouses.First(house => house.IsPlayerSpawningHere);
            }
            catch (InvalidOperationException)
            {
                // Нет домов, ищем дома, которые игрок арендует
                House rentalHouse = GetPlayerRentingHouse(player);
                return rentalHouse;
            }
        }

        /// <summary>
        /// Снимает почасовую оплату за аренду каждый час, пополняет энергию игроков каждые 10 минут
        /// </summary>
        /// <returns></returns>
        public static void StartHandlingThread()
        {
            new Thread(() =>
            {
                int tick = 0;
                while (true)
                {
                    // 10 минут
                    Thread.Sleep(60 * 1000 * 10);
                    // Thread.Sleep(1000);

                    // Каждый шестой тик снимаем оплату
                    if (tick == 6)
                    {
                        var rentalHouses = GetAllRentalHouses();
                        foreach (RentalHouse house in rentalHouses)
                        {
                            var rentingPlayers = house.RentingPlayers;
                            // Преобразуем коллекцию в массив, чтобы не возникло ошибки перечисления после изменения коллекции
                            foreach (int rentingPlayerId in rentingPlayers.ToArray())
                            {
                                int ownerBankAccountId = house.GetOwnerBankAccountId();
                                int rentingPlayerBankAccountId = BankAccount.GetAccountId(rentingPlayerId,
                                    eEntityType.player);
                                BankTransaction transaction = new BankTransaction(rentingPlayerBankAccountId, ownerBankAccountId, house.RentPrice, Data.GlobalTax, "Аренда жилья");
                                try
                                {
                                    transaction.Execute();
                                }
                                catch (BankTransactionException)
                                {
                                    // У игрока кончились деньги, нужно выселить его из арендуемого дома и отправить бомжевать
                                    Players.MoveOutFromRentalHouse(rentingPlayerId);
                                }
                            }
                        }
                    }

                    // Сначала проходимся по обычным домам, которые принадлежат игрокам
                    // и в которых игроки спаунятся, и пополняем их владельцам энергию
                    var commonHouses =
                        GetAllCommonHouses()
                            .Where(house => house.OwnerType == eEntityType.player && house.IsPlayerSpawningHere);
                    foreach (CommonHouse house in commonHouses)
                    {
                        int energyValue = house.GetTickEnergyValue();
                        Players.AddEnergyToPlayer(house.OwnerId, energyValue);
                    }

                    // Теперь проходимся по домам, которые можно арендовать, берем оттуда игроков и прибавляем им энергии
                    var rentalHousesForEnergy = GetAllRentalHouses();
                    foreach (RentalHouse house in rentalHousesForEnergy)
                    {
                        var rentingPlayers = house.RentingPlayers;
                        foreach (int playerId in rentingPlayers)
                        {
                            int energyValue = house.GetTickEnergyValue();
                            Players.AddEnergyToPlayer(playerId, energyValue);
                        }
                    }

                    tick = (tick == 6) ? 0 : tick + 1;
                }
            }).Start();
        }
    }
}

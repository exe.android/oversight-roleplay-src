﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using oversight.entity;
using oversight.entity.business;
using Entity = oversight.entity.Entity;

namespace oversight.entityGroup
{
    enum eEntityType
    {
        player,
        business
    }

    class EntityCollection
    {
        public static Entity GetEntityById(int id, eEntityType type)
        {
            switch (type)
            {
                case eEntityType.player:
                    return Players.Get(id);
                case eEntityType.business:
                    return Businesses.Get(id);
            }
            return new Entity();
        }
    }
}

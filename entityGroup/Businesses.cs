﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using oversight.entity;
using oversight.entity.business;
using oversight.system;

namespace oversight.entityGroup
{
    delegate void BusinessesLoaded();

    class Businesses
    {
        private static List<CommonBusiness> _Businesses = new List<CommonBusiness>();
        public static event BusinessesLoaded OnBusinesessLoaded;
        
        public static void Add(dynamic business)
        {
            _Businesses.Add(business);
        }

        public static CommonBusiness Get(int id)
        {
            return _Businesses.Find(b => b.Id == id);
        }

        public static List<CommonBusiness> GetAllBusinesess() => _Businesses;

        /// <summary>
        /// Загружает бизнесы из базы данных
        /// </summary>
        /// <returns></returns>
        public static void LoadBusinesses()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM `businesses`", DbConnection.GetInstance().Connection);
            command.Prepare();

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                reader.Close();
                return;
            }

            while (reader.Read())
            {
                var data = DbConnection.GetRowData(reader);

                string typeString = (string)data.Get("type");
                int id = (int)data.Get("id");
                string name = (string) data.Get("name");

                try
                {
                    Type type = Type.GetType("oversight.entity.business." + typeString);
                    var business = Activator.CreateInstance(type, id, name);
                    Add(business);
                    Main.Api.consoleOutput(string.Format("[SERVER] Business {0} loaded", name));
                }
                catch (ArgumentNullException e)
                {
                    Main.Api.consoleOutput(string.Format("[SERVER] Error while creating business - {0} ({1})", name,
                        typeString));
                    Main.Api.consoleOutput(e.Message.ToString());
                }
                catch (System.MissingMethodException e)
                {
                    Main.Api.consoleOutput(string.Format("[SERVER] Error while creating business - {0} ({1})", name,
                        typeString));
                    Main.Api.consoleOutput(e.Message.ToString());
                }
            }
            Main.Api.consoleOutput(string.Format("[SERVER] {0} businesses loaded", GetAllBusinesess().Count));
            reader.Close();

            LoadBusinessMarkers();
        }

        /// <summary>
        /// Создает все маркеры, принадлежащие бизнесам
        /// </summary>
        private static void LoadBusinessMarkers()
        {
            if (GetAllBusinesess().Count == 0)
                return;

            MySqlCommand command = new MySqlCommand("SELECT * FROM `markers` WHERE `owner_type` = 'business'", DbConnection.GetInstance().Connection);
            command.Prepare();

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                reader.Close();
                return;
            }

            int i = 0;
            while (reader.Read())
            {
                var data = DbConnection.GetRowData(reader);
                int id = (int)data.Get("id");
                int ownerId = (int) data.Get("owner_id");
                CommonBusiness business = Get(ownerId);
                if (business == null)
                    continue;

                double posX = (double)data.Get("pos_x");
                double posY = (double)data.Get("pos_y");
                double posZ = (double)data.Get("pos_z");
                Vector3 position = new Vector3(posX, posY, posZ);
                float range = (float)data.Get("range");
                float height = (float)data.Get("height");
                string markerName = (string)data.Get("name");
                string colorName = (string)data.Get("color");
                bool onlyShape = (bool) data.Get("only_shape");
                try
                {
                    CylinderColShape shape = Main.Api.createCylinderColShape(position, range, height);
                    business.Shapes.Add(markerName, shape);
                    
                    Color color = Data.Colors.Get(colorName);
                    if (color != null && !onlyShape)
                    {
                        Marker marker = Main.Api.createMarker(1, position, new Vector3(), new Vector3(),
                            new Vector3(range, range, range), 255, color.red, color.green, color.blue);
                        business.Markers.Add(markerName, marker);
                    }
                    i++;
                }
                catch (ArgumentNullException)
                {

                }
                catch (NullReferenceException)
                {
                    Main.Api.consoleOutput(string.Format("[SERVER] {0} marker was not loaded", markerName));
                }
            }
            Main.Api.consoleOutput(string.Format("[SERVER] {0} business markers loaded", i));
            foreach (CommonBusiness business in GetAllBusinesess())
            {
                business.OnMarkersLoaded();
            }
            reader.Close();
            OnBusinesessLoaded();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.entity;
using MySql.Data.MySqlClient;
using oversight.system;
using System.Threading;

namespace oversight.entityGroup
{
    class Vehicles
    {
        private static List<OsVehicle> _vehicles = new List<OsVehicle>();
        public static List<VehicleHash> Trailers = new List<VehicleHash>();

        public static void Add(OsVehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }

        public static OsVehicle Get(Vehicle vehicle)
        {
            return _vehicles.Find(v => v.Handler.handle.Value == vehicle.handle.Value);
        }

        public static OsVehicle Get(NetHandle handle)
        {
            return _vehicles.Find(v => v.Handler.handle.Value == handle.Value);
        }

        public static List<OsVehicle> GetAllVehicles()
        {
            
            return _vehicles;
        }

        public static void LoadVehicles(bool isReload)
        {
            Main.Api.consoleOutput("[SERVER] Loading vehicles...");
            MySqlCommand command = 
                new MySqlCommand(
                "SELECT *, `data`.id AS data_id, `vehicles`.id AS vehicle_id FROM `vehicles` " +
                "INNER JOIN `vehicles_model_data` ON `vehicles_model_data`.model_id = `vehicles`.model " +
                "INNER JOIN `vehicles_data` data ON `data`.`vehicle_id` = `vehicles`.`id`", DbConnection.GetInstance().Connection);
            command.Prepare();

            int vehiclesCount = 0;
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> data = DbConnection.GetRowData(reader);

                    Vector3 spawnPosition = new Vector3((double)data.Get("spawnpos_x"), (double)data.Get("spawnpos_y"), (double)data.Get("spawnpos_z"));
                    Vector3 rotation = new Vector3((double)data.Get("rotation_x"), (double)data.Get("rotation_y"), (double)data.Get("rotation_z"));
                    int color1 = (int)data.Get("color_1");
                    int color2 = (int)data.Get("color_2");

                    int model = (int)data.Get("model");
                    VehicleHash hash = (VehicleHash)Enum.Parse(typeof(VehicleHash), model.ToString());
                    Vehicle vehicle = Main.Api.createVehicle(hash, spawnPosition, rotation, color1, color2);
                    Main.Api.setVehicleEngineStatus(vehicle, false);
                    OsVehicle osVehicle = new OsVehicle(vehicle);
                    osVehicle.SetOwnerId((int)data.Get("owner_id"));
                    osVehicle.SetVehicleId((int)(uint)data.Get("vehicle_id"));

                    osVehicle.SetOwnerType((OsVehicle.EOwnerType)Enum.Parse(typeof(OsVehicle.EOwnerType), (string)data.Get("owner_type")));
                    osVehicle.SetDataId((uint)data.Get("data_id"));

                    osVehicle.Data.FuelDrainIdle = (double)data.Get("fuel_drain_idle");
                    osVehicle.Data.FuelDrainInmove = (double)data.Get("fuel_drain_inmove");
                    osVehicle.Data.FuelTank = (int)data.Get("fuel_tank");
                    osVehicle.Data.CurrentFuelAmount = (double)data.Get("fuel");
                    osVehicle.Data.Mileage = (double)data.Get("mileage") * 1000;
                    osVehicle.SetModelHash(hash);

                    // Для обмена с клиентом
                    Main.Api.setEntitySyncedData(vehicle, "MODEL_ID", model);
                    Main.Api.setEntitySyncedData(vehicle, "CLASS_NAME", Main.Api.getVehicleClassName(Main.Api.getVehicleClass(hash)));
                    Main.Api.setEntitySyncedData(vehicle, "ENGINE_STATE", "off");
                    Main.Api.setEntitySyncedData(vehicle, "MILEAGE", osVehicle.Data.Mileage);
                    Main.Api.setEntitySyncedData(vehicle, "FUEL_PERCENT", Math.Round(osVehicle.Data.CurrentFuelAmount / osVehicle.Data.FuelTank * 100));

                    osVehicle.SetNumberPlate((string)data.Get("number_plate"), (int)data.Get("number_plate_style"), false);

                    Add(osVehicle);
                    vehiclesCount++;
                }
            }
            reader.Close();
            Main.Api.consoleOutput($"[SERVER] {vehiclesCount} vehicles loaded");

            if (!isReload)
            {
                // Составляем список трейлеров, которые можно прицепить к грузовикам
                Trailers.Add(VehicleHash.TRFlat);
                Trailers.Add(VehicleHash.TrailerLogs);
            }
        }

        public static void ReloadVehicles()
        {
            foreach (OsVehicle veh in GetAllVehicles())
            {
                if (veh.Handler != null)
                {
                    Main.Api.deleteEntity(veh.Handler);
                }               
            }
            GetAllVehicles().Clear();
            Main.Api.consoleOutput("[SERVER] Reloading vehicles");
            LoadVehicles(true);
        }

        // С клиентской части пришел ивент, что движок заведен, берем тачку, в которой сидит игрок и включаем ей двигатель
        // Вообще эту систему нужно поэтапно синхронизировать с сервером
        public static void OnVehicleEngineStateChanged(Client player, bool state)
        {
            // TODO: Проверка на наличие игрока за водительским сиденьем авто, а так же право заводить авто (взлом, ключи)
            OsVehicle vehicle = Get(Main.Api.getPlayerVehicle(player));
            Main.Api.setVehicleEngineStatus(vehicle.Handler, state);
        }

        public static void StartChekingMileageThread()
        {
            Thread mileageThread = new Thread(() =>
            {
                while (true)
                {
                    foreach (OsVehicle vehicle in _vehicles)
                    {
                        bool currentEngineState = Main.Api.getVehicleEngineStatus(vehicle.Handler);
                        
                        // Если двигатель не включен, не считаем ничего.
                        if (!currentEngineState)
                            continue;

                        // Получаем позиции авто
                        Vector3 currentPosition = Main.Api.getEntityPosition(vehicle.Handler);
                        Vector3 lastCheckedPosition = vehicle.Data.LastCheckedPosition;

                        // Если это первый тик для авто, записываем последнюю позицию и идём дальше
                        if (lastCheckedPosition == null)
                        {
                            vehicle.Data.LastCheckedPosition = currentPosition;
                            continue;
                        }

                        if (vehicle.Data.CurrentFuelAmount <= 0)
                        {
                            Main.Api.setEntitySyncedData(vehicle.Handler, "ENGINE_STATE", "off");
                            Main.Api.setVehicleEngineStatus(vehicle.Handler, false);
                            continue;
                        }

                        double traveledDistance = Utils.RangeOfTwoVectors(lastCheckedPosition, currentPosition);
                        double fuelDrainTick = 0;

                        // Если авто прошло более 1 метра, значит двигается, считаем расход в движении
                        if (traveledDistance > 0)
                        {
                            fuelDrainTick = vehicle.Data.FuelDrainInmove / 100000 * traveledDistance;
                        }
                        // Если стоит, то считаем по холостому ходу
                        if (traveledDistance <= 0)
                        {
                            fuelDrainTick = vehicle.Data.FuelDrainIdle / 36000;
                        }
                        
                        vehicle.Data.CurrentFuelAmount -= fuelDrainTick;
                        vehicle.Data.Mileage += traveledDistance;
                        Main.Api.setEntitySyncedData(vehicle.Handler, "MILEAGE", vehicle.Data.Mileage);
                        Main.Api.setEntitySyncedData(vehicle.Handler, "FUEL_PERCENT", Math.Round(vehicle.Data.CurrentFuelAmount / vehicle.Data.FuelTank * 100));

                        vehicle.Data.LastCheckedPosition = currentPosition;
                    }
                    Thread.Sleep(100);
                }

            });
            mileageThread.Start();
        }

        public static List<OsVehicle> GetNearestVehiclesToEntity(NetHandle entity, double range)
        {
            Vector3 entityPosition = Main.Api.getEntityPosition(entity);
            List<OsVehicle> nearestVehicles =
                _vehicles.Where(
                        v => Utils.RangeOfTwoVectors(entityPosition, Main.Api.getEntityPosition(v.Handler)) <= range)
                    .OrderBy(v => Utils.RangeOfTwoVectors(entityPosition, Main.Api.getEntityPosition(v.Handler)))
                    .ToList();
            return nearestVehicles;
        }
    }
}

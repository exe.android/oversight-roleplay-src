﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.entity;
using MySql.Data.MySqlClient;
using oversight.exceptions;
using oversight.system;

namespace oversight.entityGroup
{
    delegate void PlayerOpeningInventory(Player player);

    class Players
    {
        private static List<Player> _players = new List<Player>();

        public static Player Get(int playerId)
        {
            return _players.Find(u => u.Id == playerId);
        }

        public static Player Get(Client user)
        {
            Player player = _players.Find(u => u.Handler.handle.Value == user.handle.Value);
            return player;
        }

        public static List<Player> GetAllPlayers()
        {
            return _players;
        }

        public static void Add(Player player)
        {
            _players.Add(player);
        }

        public static void Remove(Player player)
        {
            _players.Remove(player);
        }

        public static void Remove(Client user)
        {
            Player account = _players.Find(p => p.Handler.handle.Value == user.handle.Value);
            if (account != null)
            {
                Remove(account);
            }
        }

        public static void StartListeners()
        {
            Main.OnPlayerOpeningInventory += OnPlayerOpeningInventory;
        }

        // CALLBACKS
        private static void OnPlayerOpeningInventory(Player player)
        {
            player.SyncPlayerInventory();
        }
        // ======================================================================================

        /// <summary>
        /// Загружает аккаунт игрока по его ID
        /// </summary>
        /// <param name="player"></param>
        public static void LoadAccount(Player player)
        {
            MySqlCommand command = new MySqlCommand("SELECT u.*, bc.id AS bank_account_id FROM `users` u " +
                                                    "RIGHT JOIN `bank_cards` bc ON `bc`.owner_id = u.id " +
                                                    "WHERE u.`id` = @UserId LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@UserId", player.Id);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                Dictionary<string, object> data = DbConnection.GetRowData(reader);

                Vector3 spawnPosition = new Vector3((float)data.Get("spawnpos_x"), (float)data.Get("spawnpos_y"), (float)data.Get("spawnpos_z"));
                player.SetSpawnPosition(spawnPosition);
                player.SetBankAccountId((int) data.Get("bank_account_id"));

                // НЕ ЗАБЫТЬ УДАЛИТЬ!!
                player.SetCurrentWork(Data.eWorks.LumberJack);
                
                // main.api.setPlayerSkin(player.Handler, (PedHash) Enum.Parse(typeof(PedHash), skinId.ToString()));
                Main.Api.setPlayerSkin(player.Handler, PedHash.FreemodeMale01);

                reader.Close();
                player.Spawn();
            }
            reader.Close();
        }

        /// <summary>
        /// Занято ли имя игрока
        /// </summary>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <returns></returns>
        public static bool IsPlayerWithNameExists(string name, string surname)
        {
            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `users` WHERE `name` = @name AND `surname` = @surname LIMIT 1");
            command.Prepare();

            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@surname", surname);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            bool result = reader.HasRows;
            reader.Close();
            return result;
        }
        
        /// <summary>
        /// Прибавляет энергию игроку.
        /// Если игрок онлайн, то делает это через вызов метода,
        /// если игрок оффлайн, то записывает в базу
        /// </summary>
        /// <param name="playerId"></param>
        public static void AddEnergyToPlayer(int playerId, int energyValue)
        {
            Player player = Get(playerId);
            // Если игрок в онлайне, прибавляем энергию через вызов метода
            if (player != null)
            {
                player.AddEnergy(energyValue);
            }
            // Если игрок в оффлайне, то прибавляем энергию в БД
            else
            {
                MySqlCommand command = new MySqlCommand("UPDATE `users` SET `energy` = @energy_value WHERE `id` = @player_id LIMIT 1", DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("@energy_value", energyValue);
                command.Parameters.AddWithValue("@player_id", playerId);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Выселяет игрока из арендуемого им дома
        /// </summary>
        public static void MoveOutFromRentalHouse(int playerId)
        {
            Player player = Get(playerId);
            RentalHouse rentalHouse = Houses.GetPlayerRentingHouse(playerId);
            // Игрок арендует дом
            if (rentalHouse != null)
            {
                try
                {
                    rentalHouse.MoveOutRentingPlayer(playerId);
                    // Если игрок онлайн
                    if (player != null)
                    {
                        Main.Api.sendNotificationToPlayer(player.Handler, "~y~Внимание:\nВы были выселены из снимаемого жилья");
                    }
                }
                catch (RentalHouseException)
                { }
            }
        }

        /// <summary>
        /// Возвращает игроков в радиусе от определенной точки
        /// </summary>
        /// <param name="point"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static List<Player> GetPlayersAtRangeOfPoint(Vector3 point, float range)
        {
            var players =
                GetAllPlayers()
                    .Where(player => Utils.RangeOfTwoVectors(point, Main.Api.getEntityPosition(player.Handler)) <= range)
                    .ToList();
            return players;
        }
    }
}

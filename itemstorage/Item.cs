﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using oversight.exceptions;
using oversight.system;

namespace oversight.inventory
{
    public enum eStorageOwnerType
    {
        noone,
        player,
        obj
    }

    class Item
    {
        public readonly ItemTemplate Template;

        private int? _Id;
        private int _Amount;
        private eStorageOwnerType _OwnerType;
        private int _OwnerId;

        // Количество предметов в складчине
        public int Amount => _Amount;

        // ID в базе данных
        public int? Id => _Id;
        
        // ID владельца
        public int OwnerId => _OwnerId;

        // Тип владельца
        public eStorageOwnerType OwnerType => _OwnerType;

        public Item(int id, ItemTemplate itemTemplate, int amount)
        {
            _Id = id;
            Template = itemTemplate;
            SetAmount(amount);
        }

        /// <summary>
        /// Конструктор при создании нового предмета, отсутствующего в базе данных
        /// </summary>
        /// <param name="itemTemplate"></param>
        /// <param name="amount"></param>
        public Item(ItemTemplate itemTemplate, int amount)
        {
            Template = itemTemplate;
            SetAmount(amount);
            SyncWithDatabase();
        }

        /// <summary>
        /// Прибавляет количество
        /// </summary>
        /// <param name="amount"></param>
        public void AddAmount(int amount)
        {
            int newValue = Amount + amount;
            SetAmount(newValue);
            SyncWithDatabase();
        }

        /// <summary>
        /// Уменьшает количество
        /// </summary>
        /// <param name="amount"></param>
        public void RemoveAmount(int amount)
        {
            int newValue = Amount + amount;
            SetAmount(newValue);
            SyncWithDatabase();
        }

        /// <summary>
        /// Приватный метод, устанавливает количество предметов
        /// </summary>
        /// <param name="amount"></param>
        private void SetAmount(int amount)
        {
            // Если предмету нельзя изменять количество, бросаем исключение
            if (!Template.Stackable && amount != 1)
            {
                amount = 1;
            }

            _Amount = amount;
        }

        /// <summary>
        /// Устанавливает владельца
        /// </summary>
        /// <param name="ownerType"></param>
        /// <param name="ownerId"></param>
        public void SetOwner(eStorageOwnerType ownerType, int ownerId)
        {
            _OwnerType = ownerType;
            _OwnerId = ownerId;
        }

        /// <summary>
        /// Синхронизирует своё состояние с базой данных
        /// </summary>
        public void SyncWithDatabase()
        {
            // Предмет не записан в базу данных
            if (Id == null)
            {
                MySqlCommand command =
                    new MySqlCommand("INSERT INTO `items` (`id`, `owner_type`, `owner_id`, `item_name`, `amount`) " +
                                     "VALUES (NULL, @owner_type, @owner_id, @item_name, @amount)", DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("@owner_type", OwnerType.ToString());
                command.Parameters.AddWithValue("@owner_id", OwnerId);
                command.Parameters.AddWithValue("@item_name", Template.Name);
                command.Parameters.AddWithValue("@amount", Amount);
                command.ExecuteNonQuery();
                _Id = (int) command.LastInsertedId;
                command.Dispose();
            }
            // Предмет есть в базе данных, обновим его данные
            else
            {
                MySqlCommand command = new MySqlCommand("UPDATE `items` SET `owner_type` = @owner_type, owner_id = @owner_id, `amount` = @amount WHERE `id` = @item_id LIMIT 1", DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("@owner_type", OwnerType.ToString());
                command.Parameters.AddWithValue("@owner_id", OwnerId);
                command.Parameters.AddWithValue("@amount", Amount);
                command.Parameters.AddWithValue("@item_id", Id);
                command.ExecuteNonQuery();
                command.Dispose();
            }
        }

        /// <summary>
        /// Удаляет предмет из базы данных
        /// </summary>
        public void Destroy()
        {
            MySqlCommand command = new MySqlCommand("DELETE FROM `items` WHERE `id` = @item_id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@item_id", Id);
            command.ExecuteNonQuery();
            command.Dispose();
        }
    }
}

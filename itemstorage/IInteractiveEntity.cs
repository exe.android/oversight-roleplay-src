﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.itemstorage
{
    /// <summary>
    /// Интерфейс объекта, который может взаимодействовать с предметами
    /// </summary>
    interface IInteractiveEntity
    {
    }
}

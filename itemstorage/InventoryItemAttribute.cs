﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.inventory
{
    class InventoryItemAttribute
    {
        public string Name;
        public int Value;
        public string Description;
    }
}

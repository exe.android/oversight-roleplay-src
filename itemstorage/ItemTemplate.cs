﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using oversight.itemstorage;

namespace oversight.inventory
{
    public enum eItemType
    {
        Consumable
    }

    class ItemTemplate
    {
        // ID в коллекции Mongo
        public ObjectId _id;

        // Тип предмета
        public eItemType Type;

        // Имя предмета
        public string Name;

        // Можно ли с предметом взаимодействовать
        public bool Interactive;

        // Отображаемое имя предмета
        public string DisplayName;

        // Описание предмета
        public string Description;

        // Может ли предмет складываться в один, прибавляя своё количество,
        // либо каждый предмет будет лежать отдельно в инвентаре
        public bool Stackable;

        // Иконка предмета
        public string Icon;

        // Вес предмета
        public float Weight;

        // Может ли использоваться сам по себе
        public bool SelfUsable;

        // Может ли использоваться на объект
        public bool UsableToObject;

        // Может быть передан
        public bool Exchangable;

        // Может быть выкинут
        public bool Wasteable;

        // Анимация при использовании
        public string UsageAnimation;

        // Аттрибуты предмета
        public List<InventoryItemAttribute> Attributes = new List<InventoryItemAttribute>();

        // Эффекты после использования предмета на себя
        public List<string> SelfUsageEffects = new List<string>();

        // Если превращается во что-то после использования, то во что
        public List<MorphingItem> MorpingTo = new List<MorphingItem>();

        /// <summary>
        /// Возвращает значение аттрибута
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public int? GetAttributeValue(string attributeName)
        {
            // Нет такого аттрибута у предмета
            if (!Attributes.Exists(attribute => attribute.Name.Equals(attributeName)))
                return null;
            try
            {
                return Attributes.First(attribute => attribute.Name.Equals(attributeName)).Value;
            }
            catch (InvalidOperationException)
            {
                return null;
            }            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;
using oversight.inventory;
using oversight.itemstorage.effects;

namespace oversight.itemstorage
{
    delegate void PlayerUsingItemSelf(Player player, int itemId);

    class ItemsEventsWorker
    {
        /// <summary>
        /// Слушает все взаимодействия объектов с предметами
        /// </summary>
        public static void StartListener()
        {
            Main.OnPlayerUsingItemSelf += OnPlayerUsingItemSelf;
        }

        /// <summary>
        /// Игрок использует предмет на себя
        /// </summary>
        /// <param name="player"></param>
        /// <param name="itemId"></param>
        private static void OnPlayerUsingItemSelf(Player player, int itemId)
        {
            if (!ItemsCollection.DoesObjectHasItem(eStorageOwnerType.player, player.Id, itemId))
            {
                Main.Api.sendNotificationToPlayer(player.Handler, "~r~Ошибка~w~\rВы не владеете этим предметом");
                return;
            }

            Item item = ItemsCollection.GetItem(itemId);

            // Если такое срабатывает, то наверняка игрок использует подмену пакетов,
            // т.к. клиент не отображает кнопку для использования предмета, если его нельзя использовать
            // Может быть, здесь стоит лепить бан игроку
            if (!item.Template.SelfUsable)
            {
                Main.Api.sendNotificationToPlayer(player.Handler, "~r~Ошибка~w~\rПредмет не может использоваться");
                return;
            }

            // Применяем все эффекты использования на себя от предмета
            if (item.Template.SelfUsageEffects.Count > 0)
            {
                foreach (string effect in item.Template.SelfUsageEffects)
                {
                    // Инстанцируем объект эффекта
                    Type effectType = Type.GetType("oversight.itemstorage.effects." + effect);
                    if (effectType != null)
                    {
                        IUsageEffect effectObject = (IUsageEffect) Activator.CreateInstance(effectType);
                        // Применяем эффект к игроку
                        effectObject.applyWithItemToPlayer(item, player);
                    }
                }
            }

            // Синхронизируем инвентарь игрока после всех операций
            player.SyncPlayerInventory();
        }
    }
}

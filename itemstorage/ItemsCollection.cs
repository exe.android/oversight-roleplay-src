﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using oversight.entity;
using oversight.entityGroup;
using oversight.exceptions;
using oversight.interfaces;
using oversight.itemstorage;
using oversight.system;

namespace oversight.inventory
{
    class ItemsCollection
    {
        // Коллекция всех предметов
        private static List<Item> Items = new List<Item>();

        /// <summary>
        /// Возвращает список предметов, принадлежащих объекту
        /// </summary>
        /// <param name="ownerType"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public static List<Item> GetItems(eStorageOwnerType ownerType, int ownerId)
        {
            return Items.Where(item => item.OwnerType.Equals(ownerType) && item.OwnerId.Equals(ownerId)).ToList();
        }

        /// <summary>
        /// Меняет владельца предмета
        /// </summary>
        /// <param name="item"></param>
        /// <param name="newOwnerType"></param>
        /// <param name="newOwnerId"></param>
        /// <param name="amount"></param>
        public static void ChangeItemOwner(Item item, eStorageOwnerType newOwnerType, int newOwnerId, int amount)
        {
            IStoringObject newOwner = StoragableObjectsFabric.GetObject(newOwnerType, newOwnerId);
            if (newOwner == null)
            {
                throw new ItemOwnerChangeException("Принимающий объект не найден");
            }
            if (!newOwner.GetIsItemCanBeCarried(item))
            {
                throw new ItemOwnerChangeException("Принимающий объект не может принять этот объект");
            }
            if (item.Amount > amount || item.Amount <= 0 || (amount > 1 && !item.Template.Stackable))
            {
                throw new ItemOwnerChangeException("Нельзя передать такое количество предметов");
            }

            // Если количество передаваемых предметов равно количеству предметов в наличии, тогда просто меняем владельца
            if (item.Amount == amount)
            {
                item.SetOwner(newOwnerType, newOwnerId);
                item.SyncWithDatabase();
            }
            // Если передаваемое количество меньше, чем передающий объект имеет в наличии, то создаем
            // еще один такого типа с оставшимся количеством и передаем его новому владельцу
            else
            {
                int estimatedAmount = amount - item.Amount;
                try
                {
                    Item newItem = CreateNewItem(item.Template, estimatedAmount);
                    newItem.SetOwner(newOwnerType, newOwnerId);
                    newItem.SyncWithDatabase();

                    item.RemoveAmount(amount);
                    item.SyncWithDatabase();
                }
                catch (ItemCreateException e)
                {
                    throw new ItemOwnerChangeException(e.Message);
                }
            }

            if (newOwnerType.Equals(eStorageOwnerType.player))
            {
                Player player = Players.Get(newOwnerId);
                Main.Api.sendNotificationToPlayer(player.Handler, string.Format("Вы получили: ~g~{0}~w~ ({1} шт.)", item.Template.DisplayName, amount));
            }
        }

        /// <summary>
        /// Создает новый предмет и кладёт его в коллекцию
        /// </summary>
        /// <param name="itemTemplate"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static Item CreateNewItem(ItemTemplate itemTemplate, int amount)
        {
            if (!itemTemplate.Stackable && amount > 1)
            {
                throw new ItemCreateException("Нельзя установить количество нескладывающемуся предмету");
            }

            Item item = new Item(itemTemplate, amount);
            Items.Add(item);

            return item;
        }

        /// <summary>
        /// Уничтожает предмет или его определенное количество
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        public static void DestroyItem(Item item, int amount)
        {
            if (item.Template.Stackable)
            {
                item.RemoveAmount(amount);
            }
            else
            {
                Items.Remove(item);
                item.Destroy();
            }
        }

        /// <summary>
        /// Возвращает предмет из коллекции по его ID
        /// </summary>
        /// <param name="itemId"></param>
        public static Item GetItem(int itemId)
        {
            try
            {
                return Items.First(item => item.Id.Equals(itemId));
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Загружает все предметы всех игроков в память
        /// </summary>
        public static void LoadCollection()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM `items`", DbConnection.GetInstance().Connection);
            command.Prepare();

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            int badItemsAmount = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var data = DbConnection.GetRowData(reader);
                    string itemName = (string) data.Get("item_name");

                    ItemTemplate itemTemplate = ItemsTemplateWorker.GetItemTemplate(itemName);
                    // Предмет существует в коллекции MongoDB
                    if (itemTemplate != null)
                    {
                        int id = (int) data.Get("id");
                        int amount = (int) data.Get("amount");
                        eStorageOwnerType ownerType = (eStorageOwnerType) Enum.Parse(typeof(eStorageOwnerType), (string) data.Get("owner_type"));
                        int ownerId = (int) data.Get("owner_id");

                        Item item = new Item(id, itemTemplate, amount);
                        item.SetOwner(ownerType, ownerId);
                        Items.Add(item);
                    }
                    else
                    {
                        badItemsAmount++;
                    }
                }
            }
            Main.Api.consoleOutput(string.Format("[SERVER] {0} items loaded", Items.Count));
            if (badItemsAmount > 0)
                Main.Api.consoleOutput(string.Format("[SERVER] {0} items was not loaded due to not exsists in mongo collection", badItemsAmount));
            reader.Close();
        }

        /// <summary>
        /// Имеет ли объект указанный предмет
        /// </summary>
        /// <param name="ownerType"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public static bool DoesObjectHasItem(eStorageOwnerType ownerType, int ownerId, int itemId)
        {
            Item item = GetItem(itemId);
            return item.OwnerType.Equals(ownerType) && item.OwnerId.Equals(ownerId);
        }

    }
}

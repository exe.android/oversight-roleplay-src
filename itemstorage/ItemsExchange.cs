﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.exceptions;
using oversight.inventory;

namespace oversight.itemstorage
{
    /// <summary>
    /// Все передачи предметов нужно производить через этот класс
    /// </summary>
    class ItemsExchange
    {
        private eStorageOwnerType OldOwnerType;
        private int OldOwnerId;
        private eStorageOwnerType NewOwnerType;
        private int NewOwnerId;
        private Item ExchangableItem;
        private int Amount;

        public ItemsExchange(eStorageOwnerType oldOwnerType, int oldOwnerId, eStorageOwnerType newOwnerType, int newOwnerId, Item exchangableItem, int amount)
        {
            OldOwnerType = oldOwnerType;
            OldOwnerId = oldOwnerId;
            NewOwnerType = newOwnerType;
            NewOwnerId = newOwnerId;
            ExchangableItem = exchangableItem;
            Amount = amount;

            if (oldOwnerType != exchangableItem.OwnerType && oldOwnerId != exchangableItem.OwnerId)
            {
                throw new ItemOwnerChangeException("Передающий объект не владеет этим предметом");
            }
        }

        public void Execute()
        {
            ItemsCollection.ChangeItemOwner(ExchangableItem, NewOwnerType, NewOwnerId, Amount);
        }
    }
}

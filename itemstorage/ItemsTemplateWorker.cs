﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using oversight.system;

namespace oversight.inventory
{
    class ItemsTemplateWorker
    {
        // Коллекция предметов из Mongo
        private static IMongoCollection<ItemTemplate> InventoryItems;

        /// <summary>
        /// Отдает предмет инвентаря по его имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ItemTemplate GetItemTemplate(string name)
        {
            try
            {
                return InventoryItems.AsQueryable().First(item => item.Name.Equals(name));
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Загружает предметы из MongoDB в коллекцию
        /// </summary>
        public static void InitInventory()
        {
            // Загружаем все существующие предметы в коллекцию
            InventoryItems = DbConnection.GetInstance().MongoDatabase.GetCollection<ItemTemplate>("items");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;
using oversight.inventory;

namespace oversight.itemstorage.effects
{
    class Morph: IUsageEffect
    {
        public void applyWithItemToPlayer(Item item, Player player)
        {
            foreach (MorphingItem morphingItem in item.Template.MorpingTo)
            {
                ItemTemplate newItemTemplate = ItemsTemplateWorker.GetItemTemplate(morphingItem.ItemName);
                if (newItemTemplate != null)
                {
                    Item newItem = ItemsCollection.CreateNewItem(newItemTemplate, morphingItem.Amount);
                    ItemsCollection.ChangeItemOwner(newItem, eStorageOwnerType.player, player.Id, morphingItem.Amount);
                }
            }
        }
    }
}

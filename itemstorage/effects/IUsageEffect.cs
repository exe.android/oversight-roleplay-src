﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;
using oversight.inventory;

namespace oversight.itemstorage.effects
{
    interface IUsageEffect
    {
        void applyWithItemToPlayer(Item item, Player player);
    }
}

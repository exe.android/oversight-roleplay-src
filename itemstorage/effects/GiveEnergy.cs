﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;
using oversight.inventory;

namespace oversight.itemstorage.effects
{
    class GiveEnergy: IUsageEffect
    {
        public void applyWithItemToPlayer(Item item, Player player)
        {
            int? effectAttributeValue = item.Template.GetAttributeValue("GiveEnergy");
            if (effectAttributeValue != null)
            {
                player.AddEnergy(effectAttributeValue.Value);
            }     
        }
    }
}

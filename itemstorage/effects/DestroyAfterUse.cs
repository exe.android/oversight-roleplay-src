﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;
using oversight.inventory;

namespace oversight.itemstorage.effects
{
    class DestroyAfterUse: IUsageEffect
    {
        public void applyWithItemToPlayer(Item item, Player player)
        {
            ItemsCollection.DestroyItem(item, 1);
        }
    }
}

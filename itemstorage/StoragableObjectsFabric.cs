﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entityGroup;
using oversight.interfaces;
using oversight.inventory;

namespace oversight.itemstorage
{
    class StoragableObjectsFabric
    {
        public static IStoringObject GetObject(eStorageOwnerType ownerType, int ownerId)
        {
            if (ownerType.Equals(eStorageOwnerType.player))
            {
                IStoringObject player = Players.Get(ownerId);
                return player;
            }
            return null;
        }
    }
}

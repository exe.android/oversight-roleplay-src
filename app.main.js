class MyTestClass {
    constructor() {
        this.test = "HELLO WORLD!";
        this.testMethod = () => {
            var c = new CefHelper("/login.html", true);
            c.show();
        };
    }
}
API.onResourceStart.connect(() => {
    var cls = new MyTestClass();
    cls.testMethod();
});
const BrowserLoadTime = 1000;
class CefHelper {
    constructor(pagePath, local = true) {
        this._isCreated = false;
        this._isOpen = false;
        this._local = true;
        this._eventQueue = new Array();
        this._creationTime = 0;
        this.onResourceStop = () => {
            this.destroy();
        };
        this.onUpdate = () => {
            if (!this._isOpen)
                return;
            if (this._eventQueue.length > 0 && this._creationTime > 0 && API.getGlobalTime() - this._creationTime >= BrowserLoadTime) {
                this._eventQueue.shift()(this._browserControl);
            }
            API.disableAllControlsThisFrame();
        };
        this.create = () => {
            if (this._isCreated)
                return;
            API.sendChatMessage("CREATED");
            var resolution = API.getScreenResolution();
            this._browserControl = API.createCefBrowser(resolution.Width, resolution.Height, this._local);
            API.waitUntilCefBrowserInit(this._browserControl);
            API.setCefBrowserPosition(this._browserControl, 0, 0);
            API.loadPageCefBrowser(this._browserControl, this._pagePath);
            this._creationTime = API.getGlobalTime();
            this._isCreated = true;
        };
        this.destroy = () => {
            if (!this._isCreated || typeof (this._browserControl) == "undefined" || this._browserControl == null)
                return;
            this.hide();
            API.sendChatMessage("destroy");
            API.destroyCefBrowser(this._browserControl);
            this._browserControl.Dispose();
            this._browserControl = null;
            this._isCreated = false;
        };
        this.show = () => {
            if (this._isOpen)
                return;
            this.create();
            this._isOpen = true;
            API.setCefBrowserHeadless(this._browserControl, false);
            API.showCursor(true);
            API.setCanOpenChat(false);
        };
        this.hide = () => {
            if (!this._isOpen)
                return;
            this._isOpen = false;
            API.setCanOpenChat(true);
            API.showCursor(false);
            API.setCefBrowserHeadless(this._browserControl, true);
        };
        this.callMethod = (caller) => {
            this._eventQueue.push(caller);
        };
        this._pagePath = pagePath;
        this._local = local;
        API.sendChatMessage("CREATED!");
        API.onResourceStop.connect(this.onResourceStop);
        API.onUpdate.connect(this.onUpdate);
    }
    get IsOpen() { return this._isOpen; }
}

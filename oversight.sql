/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : oversight

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2017-03-07 22:12:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bank_cards
-- ----------------------------
DROP TABLE IF EXISTS `bank_cards`;
CREATE TABLE `bank_cards` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `owner_type` enum('player','business') DEFAULT NULL,
  `owner_id` int(5) DEFAULT NULL,
  `bank_id` int(3) unsigned DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `card_number` char(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_bank_cards` (`owner_type`,`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank_cards
-- ----------------------------
INSERT INTO `bank_cards` VALUES ('1', 'business', '1', '1', '19', '0000000000000000');
INSERT INTO `bank_cards` VALUES ('2', 'business', '2', '1', '76', '0000000000000000');
INSERT INTO `bank_cards` VALUES ('3', 'player', '1', '1', '1295', '0000000000000000');

-- ----------------------------
-- Table structure for bank_operations_logs
-- ----------------------------
DROP TABLE IF EXISTS `bank_operations_logs`;
CREATE TABLE `bank_operations_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `tax_amount` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_bank_operations_logs_to` (`to`),
  KEY `FK_bank_operations_logs_from` (`from`),
  CONSTRAINT `FK_bank_operations_logs_from` FOREIGN KEY (`from`) REFERENCES `bank_cards` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `FK_bank_operations_logs_to` FOREIGN KEY (`to`) REFERENCES `bank_cards` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank_operations_logs
-- ----------------------------
INSERT INTO `bank_operations_logs` VALUES ('1', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:10:29');
INSERT INTO `bank_operations_logs` VALUES ('2', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:10:29');
INSERT INTO `bank_operations_logs` VALUES ('3', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:11:39');
INSERT INTO `bank_operations_logs` VALUES ('4', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:11:39');
INSERT INTO `bank_operations_logs` VALUES ('5', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:13:47');
INSERT INTO `bank_operations_logs` VALUES ('6', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:13:47');
INSERT INTO `bank_operations_logs` VALUES ('7', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:13:54');
INSERT INTO `bank_operations_logs` VALUES ('8', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:13:54');
INSERT INTO `bank_operations_logs` VALUES ('9', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:14:01');
INSERT INTO `bank_operations_logs` VALUES ('10', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:14:01');
INSERT INTO `bank_operations_logs` VALUES ('11', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:14:08');
INSERT INTO `bank_operations_logs` VALUES ('12', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:14:08');
INSERT INTO `bank_operations_logs` VALUES ('13', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:23:32');
INSERT INTO `bank_operations_logs` VALUES ('14', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:23:32');
INSERT INTO `bank_operations_logs` VALUES ('15', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:23:39');
INSERT INTO `bank_operations_logs` VALUES ('16', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:23:39');
INSERT INTO `bank_operations_logs` VALUES ('17', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:23:46');
INSERT INTO `bank_operations_logs` VALUES ('18', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:23:46');
INSERT INTO `bank_operations_logs` VALUES ('19', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:23:53');
INSERT INTO `bank_operations_logs` VALUES ('20', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:23:53');
INSERT INTO `bank_operations_logs` VALUES ('21', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:29:50');
INSERT INTO `bank_operations_logs` VALUES ('22', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:29:50');
INSERT INTO `bank_operations_logs` VALUES ('23', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:29:57');
INSERT INTO `bank_operations_logs` VALUES ('24', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:29:57');
INSERT INTO `bank_operations_logs` VALUES ('25', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:30:04');
INSERT INTO `bank_operations_logs` VALUES ('26', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:30:04');
INSERT INTO `bank_operations_logs` VALUES ('27', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:30:11');
INSERT INTO `bank_operations_logs` VALUES ('28', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:30:11');
INSERT INTO `bank_operations_logs` VALUES ('29', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:31:49');
INSERT INTO `bank_operations_logs` VALUES ('30', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:31:49');
INSERT INTO `bank_operations_logs` VALUES ('31', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:31:56');
INSERT INTO `bank_operations_logs` VALUES ('32', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:31:56');
INSERT INTO `bank_operations_logs` VALUES ('33', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:32:03');
INSERT INTO `bank_operations_logs` VALUES ('34', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:32:03');
INSERT INTO `bank_operations_logs` VALUES ('35', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-02-26 20:32:10');
INSERT INTO `bank_operations_logs` VALUES ('36', '2', '1', '1', '0', '0', 'Налог', '2017-02-26 20:32:10');
INSERT INTO `bank_operations_logs` VALUES ('37', '3', '2', '5', '13', '1', 'Аренда жилья', '2017-03-05 03:57:14');
INSERT INTO `bank_operations_logs` VALUES ('38', '2', '1', '1', '0', '0', 'Налог', '2017-03-05 03:57:14');

-- ----------------------------
-- Table structure for businesses
-- ----------------------------
DROP TABLE IF EXISTS `businesses`;
CREATE TABLE `businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` enum('Administration','Samwill','TransportCompany','Logging') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of businesses
-- ----------------------------
INSERT INTO `businesses` VALUES ('1', 'Administration', 'Administration');
INSERT INTO `businesses` VALUES ('2', 'TransportCompany', 'TransportCompany');
INSERT INTO `businesses` VALUES ('3', 'Logging', 'Logging');
INSERT INTO `businesses` VALUES ('4', 'Samwill', 'Samwill');

-- ----------------------------
-- Table structure for houses
-- ----------------------------
DROP TABLE IF EXISTS `houses`;
CREATE TABLE `houses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` enum('player','business') DEFAULT 'player',
  `type` enum('common','rental') DEFAULT 'common',
  `owner_id` int(5) unsigned DEFAULT '0',
  `class` enum('econom','common','good','rich','lux','super lux') DEFAULT 'common',
  `enter_pos_x` float(7,3) DEFAULT '0.000',
  `enter_pos_y` float(7,3) DEFAULT '0.000',
  `enter_pos_z` float(7,3) DEFAULT '0.000',
  `comfort_level` int(11) DEFAULT '0' COMMENT 'Уровень комфорта дома (максимум - 10)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of houses
-- ----------------------------
INSERT INTO `houses` VALUES ('1', 'player', 'common', '2', 'super lux', '-1499.666', '139.144', '54.653', '10');
INSERT INTO `houses` VALUES ('2', 'business', 'rental', '2', 'econom', '-749.604', '2607.160', '59.428', '2');
INSERT INTO `houses` VALUES ('3', 'business', 'rental', '2', 'econom', '-734.227', '2608.581', '59.419', '2');
INSERT INTO `houses` VALUES ('4', 'business', 'rental', '2', 'econom', '-739.576', '2599.035', '59.611', '2');
INSERT INTO `houses` VALUES ('5', 'business', 'rental', '2', 'econom', '-753.070', '2593.538', '59.457', '2');
INSERT INTO `houses` VALUES ('6', 'business', 'rental', '2', 'econom', '-730.137', '2555.266', '58.481', '2');
INSERT INTO `houses` VALUES ('7', 'business', 'rental', '2', 'econom', '-724.673', '2653.520', '56.843', '2');

-- ----------------------------
-- Table structure for houses_common_data
-- ----------------------------
DROP TABLE IF EXISTS `houses_common_data`;
CREATE TABLE `houses_common_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) unsigned DEFAULT NULL,
  `player_spawning_here` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_houses_common_data_house_id` (`house_id`),
  CONSTRAINT `FK_houses_common_data_house_id` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Информация, принадлежащая обычным домам';

-- ----------------------------
-- Records of houses_common_data
-- ----------------------------
INSERT INTO `houses_common_data` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for houses_rental_data
-- ----------------------------
DROP TABLE IF EXISTS `houses_rental_data`;
CREATE TABLE `houses_rental_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) unsigned DEFAULT NULL COMMENT 'ID в таблице houses',
  `name` varchar(50) DEFAULT '' COMMENT 'Название здания (отображается в заголовке интерфейса управления)',
  `rent_price` int(11) DEFAULT '0' COMMENT 'Стоимость аренды ($ в час)',
  `players_limit` int(5) DEFAULT '1' COMMENT 'Количество возможных мест для аренды',
  PRIMARY KEY (`id`),
  KEY `house_id` (`house_id`),
  CONSTRAINT `house_id` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of houses_rental_data
-- ----------------------------
INSERT INTO `houses_rental_data` VALUES ('1', '3', 'Вахтовка', '5', '10');
INSERT INTO `houses_rental_data` VALUES ('2', '2', 'Вахтовка', '5', '10');
INSERT INTO `houses_rental_data` VALUES ('3', '4', 'Вахтовка', '5', '10');
INSERT INTO `houses_rental_data` VALUES ('4', '5', 'Вахтовка', '5', '10');
INSERT INTO `houses_rental_data` VALUES ('5', '6', 'Вахтовка', '5', '10');
INSERT INTO `houses_rental_data` VALUES ('6', '7', 'Вахтовка', '5', '10');

-- ----------------------------
-- Table structure for houses_rental_players
-- ----------------------------
DROP TABLE IF EXISTS `houses_rental_players`;
CREATE TABLE `houses_rental_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) unsigned DEFAULT NULL COMMENT 'ID в таблице houses',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID игрока в таблице users',
  `since` datetime DEFAULT NULL COMMENT 'С какого времени проживает',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_houses_rental_players` (`house_id`,`player_id`),
  KEY `FK_houses_rental_players_player` (`player_id`),
  CONSTRAINT `FK_houses_rental_players_house` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_houses_rental_players_player` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Игроки, проживающие в зданиях, предназначенных для аренды';

-- ----------------------------
-- Records of houses_rental_players
-- ----------------------------
INSERT INTO `houses_rental_players` VALUES ('1', '4', '1', '2017-03-05 03:57:14');

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` enum('noone','player','obj') DEFAULT 'noone',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `item_name` varchar(255) NOT NULL DEFAULT '',
  `amount` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_users_inventory_item_name` (`item_name`),
  KEY `IDX_users_inventory_player_id` (`owner_id`),
  KEY `UK_users_inventory` (`owner_type`,`owner_id`,`item_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('5', 'player', '1', 'Dollar', '500');
INSERT INTO `items` VALUES ('8', 'player', '1', 'Pepsi', '1');
INSERT INTO `items` VALUES ('10', 'player', '1', 'PepsiEmpty', '1');

-- ----------------------------
-- Table structure for mapping
-- ----------------------------
DROP TABLE IF EXISTS `mapping`;
CREATE TABLE `mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hash` int(13) DEFAULT NULL,
  `pos_x` double(9,4) DEFAULT NULL,
  `pos_y` double(9,4) DEFAULT NULL,
  `pos_z` double(9,4) DEFAULT NULL,
  `rot_x` double(9,4) DEFAULT NULL,
  `rot_y` double(9,4) DEFAULT NULL,
  `rot_z` double(9,4) DEFAULT NULL,
  `dimension` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mapping
-- ----------------------------
INSERT INTO `mapping` VALUES ('1', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-680.9303', '2603.4580', '45.1545', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('2', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-668.5448', '2643.0460', '42.4886', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('3', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-688.5853', '2660.7040', '47.9600', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('4', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-663.8051', '2697.1290', '40.3917', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('5', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-703.4015', '2709.6490', '43.0921', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('6', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-679.9336', '2739.9240', '40.3582', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('7', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-602.1849', '2663.5470', '38.2523', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('8', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-591.7158', '2638.7000', '41.8972', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('9', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-636.3493', '2642.1830', '37.7631', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('10', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-653.0539', '2627.7820', '41.4185', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('11', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-648.7662', '2573.1190', '47.2232', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('12', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-627.9072', '2559.0140', '47.9663', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('13', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-622.7474', '2577.4850', '42.7338', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('14', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-665.4277', '2584.4400', '44.2669', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('15', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-706.1658', '2613.5420', '51.7404', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('16', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-711.8013', '2679.3740', '51.1535', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('17', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-679.6581', '2622.1250', '44.9639', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('18', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-689.8185', '2640.3240', '49.4762', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('19', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-722.9380', '2742.1190', '36.0176', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('20', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-674.1959', '2674.9450', '42.7968', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('21', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-647.8863', '2657.3840', '38.4992', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('22', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-672.2457', '2717.2620', '40.0477', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('23', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-653.5322', '2675.0990', '38.4417', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('24', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-645.2098', '2710.2130', '35.9492', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('25', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-657.3448', '2725.2510', '37.4419', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('26', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-662.8784', '2746.8680', '39.2391', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('27', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-726.8862', '2699.5500', '49.1348', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('28', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-751.7961', '2745.1080', '30.1570', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('29', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-722.3656', '2759.9870', '32.8730', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('30', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-708.9875', '2750.3550', '36.8473', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('31', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-695.8641', '2741.9740', '39.5991', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('32', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-689.8572', '2726.8850', '41.0446', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('33', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-691.0260', '2763.0540', '37.3334', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('34', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-655.6797', '2763.5440', '38.7478', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('35', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-674.1173', '2762.5240', '38.8037', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('36', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-684.8365', '2784.5420', '35.4129', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('37', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-670.7050', '2778.0940', '37.7684', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('38', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-633.3451', '2772.9090', '36.9816', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('39', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-648.7240', '2778.1960', '38.1529', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('40', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-655.9730', '2794.5650', '36.8084', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('41', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-614.4604', '2794.4270', '36.4240', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('42', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-621.8815', '2819.1130', '36.0251', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('43', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-597.7572', '2813.8230', '35.6341', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('44', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-577.1471', '2803.0240', '34.3036', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('45', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-579.3028', '2827.6010', '34.5121', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('46', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-600.1412', '2831.9920', '35.2684', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('47', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-599.5551', '2799.9210', '35.7085', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('48', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-636.3932', '2807.5090', '36.4215', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('49', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-651.7958', '2813.1560', '35.0876', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('50', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-690.2385', '2693.2980', '43.5263', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('51', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-705.5005', '2652.3100', '54.4574', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('52', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-712.8339', '2594.4150', '53.5477', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('53', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-758.5837', '2648.0310', '58.9899', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('54', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-756.5494', '2620.5180', '61.4815', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('55', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-772.3053', '2625.3160', '64.1891', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('56', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-788.5341', '2640.3700', '60.0380', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('57', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-779.9364', '2599.5820', '66.5271', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('58', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-813.9560', '2628.9530', '62.1315', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('59', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-790.5723', '2615.4390', '67.2189', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('60', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-811.4037', '2601.4080', '70.7948', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('61', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-834.6294', '2587.9610', '72.2560', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('62', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-796.8499', '2597.8650', '69.6202', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('63', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-820.5751', '2589.4470', '73.6734', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('64', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-805.0627', '2573.0630', '73.3442', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('65', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-829.7342', '2609.4570', '66.7884', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('66', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-825.4681', '2569.8060', '77.3656', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('67', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-852.8998', '2585.7570', '66.4213', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('68', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-844.3558', '2565.1710', '74.2941', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('69', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-817.2722', '2557.1530', '76.9977', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('70', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-834.2689', '2549.5800', '80.0777', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('71', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-854.1417', '2570.7690', '69.2372', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('72', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-869.1967', '2562.2210', '67.4640', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('73', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-857.4841', '2545.7420', '80.3990', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('74', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-845.2540', '2537.1030', '82.7373', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('75', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-820.6640', '2538.7950', '77.2553', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('76', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-833.5812', '2525.7710', '79.4457', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('77', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-848.2065', '2521.2320', '85.0641', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('78', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-856.8574', '2522.7900', '88.7706', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('79', 'LiveTree', 'prop_tree_olive_creator', '-155870793', '-873.5059', '2539.0680', '78.5524', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('80', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-805.6749', '2523.1330', '80.0747', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('81', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-782.3586', '2531.7310', '79.5972', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('82', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-788.5132', '2497.8300', '92.8420', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('83', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-782.3285', '2549.3020', '73.1501', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('84', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-756.1581', '2548.2860', '64.7019', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('85', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-763.5562', '2526.0470', '78.8447', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('86', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-783.9468', '2512.9570', '86.4007', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('87', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-746.6733', '2651.5240', '58.5022', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('88', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-656.3688', '2561.4190', '51.4158', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('89', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-674.8311', '2555.7620', '52.7958', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('90', 'LiveTree', 'prop_tree_olive_cr2', '-73584559', '-666.0836', '2542.8210', '57.7490', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('91', 'LiveTree', 'prop_tree_eng_oak_cr2', '-1279773008', '-723.1512', '2464.5520', '66.4890', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('92', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-718.3034', '2453.0160', '62.0009', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('93', 'LiveTree', 'prop_tree_eng_oak_creator', '1204839864', '-730.1603', '2442.6850', '65.8978', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('94', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-731.2178', '2608.6720', '59.0430', '0.0000', '0.0000', '-69.0003', '0');
INSERT INTO `mapping` VALUES ('95', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-737.5121', '2596.8180', '59.3980', '0.0000', '0.0000', '-115.9990', '0');
INSERT INTO `mapping` VALUES ('96', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-755.7332', '2595.1980', '59.5781', '0.0000', '0.0000', '77.9992', '0');
INSERT INTO `mapping` VALUES ('97', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-751.6869', '2609.0480', '59.6173', '0.0000', '0.0000', '68.9998', '0');
INSERT INTO `mapping` VALUES ('98', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-727.5541', '2556.6440', '58.5494', '0.0000', '0.0000', '-39.9999', '0');
INSERT INTO `mapping` VALUES ('99', 'WorkCabinHouse', 'prop_portacabin01', '-1098506160', '-721.5724', '2653.0310', '57.1066', '0.0000', '0.0000', '-78.0008', '0');
INSERT INTO `mapping` VALUES ('100', 'common', 'prop_portaloo_01a', '682074297', '-721.3725', '2647.2180', '57.2845', '0.0000', '0.0000', '12.0000', '0');
INSERT INTO `mapping` VALUES ('101', 'common', 'prop_portaloo_01a', '682074297', '-748.8211', '2614.0800', '59.3814', '0.0000', '0.0000', '157.0005', '0');
INSERT INTO `mapping` VALUES ('102', 'common', 'prop_portaloo_01a', '682074297', '-736.0344', '2602.6220', '59.4384', '0.0000', '0.0000', '-113.9995', '0');
INSERT INTO `mapping` VALUES ('103', 'common', 'prop_portaloo_01a', '682074297', '-732.7543', '2559.4420', '58.7112', '0.0000', '0.0000', '-129.9994', '0');
INSERT INTO `mapping` VALUES ('104', 'common', 'hei_prop_heist_binbag', '1138881502', '-741.1371', '2593.3110', '60.0805', '0.0000', '0.0000', '0.0000', '0');
INSERT INTO `mapping` VALUES ('105', 'common', 'hei_prop_heist_binbag', '1138881502', '-732.7969', '2607.9840', '59.8164', '0.0000', '0.0000', '0.0000', '0');

-- ----------------------------
-- Table structure for mapping_deleted_objects
-- ----------------------------
DROP TABLE IF EXISTS `mapping_deleted_objects`;
CREATE TABLE `mapping_deleted_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` int(11) DEFAULT NULL,
  `pos_x` double(9,4) DEFAULT NULL,
  `pos_y` double(9,4) DEFAULT NULL,
  `pos_z` double(9,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mapping_deleted_objects
-- ----------------------------
INSERT INTO `mapping_deleted_objects` VALUES ('1', '1853453107', '1719.5420', '-1608.6720', '111.4629');
INSERT INTO `mapping_deleted_objects` VALUES ('2', '1152297372', '1732.2530', '-1549.6940', '111.6817');
INSERT INTO `mapping_deleted_objects` VALUES ('3', '1152297372', '1740.0050', '-1526.7770', '111.6440');
INSERT INTO `mapping_deleted_objects` VALUES ('4', '531344027', '1744.8010', '-1515.6160', '111.8052');
INSERT INTO `mapping_deleted_objects` VALUES ('5', '1152297372', '1747.8250', '-1504.2180', '111.8182');
INSERT INTO `mapping_deleted_objects` VALUES ('6', '531344027', '1751.8050', '-1492.8110', '111.8406');
INSERT INTO `mapping_deleted_objects` VALUES ('7', '1152297372', '1693.1450', '-1522.8240', '111.8753');
INSERT INTO `mapping_deleted_objects` VALUES ('8', '531344027', '1689.5910', '-1534.0980', '111.8148');
INSERT INTO `mapping_deleted_objects` VALUES ('9', '1152297372', '1695.9870', '-1511.5780', '111.9594');
INSERT INTO `mapping_deleted_objects` VALUES ('10', '1152297372', '1704.8710', '-1488.6150', '111.9217');
INSERT INTO `mapping_deleted_objects` VALUES ('11', '1152297372', '1709.4630', '-1477.6410', '111.9237');
INSERT INTO `mapping_deleted_objects` VALUES ('12', '1888301071', '1662.8620', '-1519.6050', '111.6843');
INSERT INTO `mapping_deleted_objects` VALUES ('13', '-531344027', '1689.0000', '-1534.0000', '111.4629');
INSERT INTO `mapping_deleted_objects` VALUES ('14', '-531344027', '1751.0000', '-1492.0000', '111.4629');
INSERT INTO `mapping_deleted_objects` VALUES ('15', '-531344027', '1744.8000', '-1515.6000', '111.4629');
INSERT INTO `mapping_deleted_objects` VALUES ('16', '-328261803', '1002.3960', '-2486.1550', '27.2486');
INSERT INTO `mapping_deleted_objects` VALUES ('17', '1152297372', '977.9063', '-2481.2960', '27.2589');
INSERT INTO `mapping_deleted_objects` VALUES ('18', '1152297372', '971.0444', '-2498.8120', '27.3003');
INSERT INTO `mapping_deleted_objects` VALUES ('19', '1152297372', '957.6075', '-2479.4410', '27.2783');
INSERT INTO `mapping_deleted_objects` VALUES ('20', '1152297372', '793.4102', '-2511.6070', '20.4605');
INSERT INTO `mapping_deleted_objects` VALUES ('21', '1152297372', '943.8838', '-2532.3560', '27.3026');

-- ----------------------------
-- Table structure for markers
-- ----------------------------
DROP TABLE IF EXISTS `markers`;
CREATE TABLE `markers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `owner_type` enum('business') DEFAULT NULL,
  `owner_id` int(5) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pos_x` double(7,3) DEFAULT NULL,
  `pos_y` double(7,3) DEFAULT NULL,
  `pos_z` double(7,3) DEFAULT NULL,
  `range` float(3,1) DEFAULT NULL,
  `height` float(3,1) DEFAULT NULL,
  `color` enum('green','red','blue','') DEFAULT NULL,
  `only_shape` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of markers
-- ----------------------------
INSERT INTO `markers` VALUES ('1', 'business', '1', 'sumwill_enter', '-552.974', '5348.528', '73.763', '2.0', '5.0', 'blue', '0');
INSERT INTO `markers` VALUES ('2', 'business', '3', 'logging_enter', '-151.952', '-629.355', '46.420', '2.0', '5.0', 'blue', '0');
INSERT INTO `markers` VALUES ('3', 'business', '3', 'logging_wood_1', '-705.068', '2660.619', '54.651', '2.0', '5.0', 'blue', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(15) DEFAULT NULL,
  `name` char(10) DEFAULT NULL,
  `surname` char(10) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `spawnpos_x` float(7,3) DEFAULT NULL,
  `spawnpos_y` float(7,3) DEFAULT NULL,
  `spawnpos_z` float(7,3) DEFAULT NULL,
  `energy` int(11) DEFAULT '1000' COMMENT 'Энергия игрока. 1000 - максимум.',
  PRIMARY KEY (`id`),
  KEY `name,surname` (`name`,`surname`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Insight', null, null, '96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', null, '-726.066', '2661.151', '57.095', '16');
INSERT INTO `users` VALUES ('2', 'Asterious', 'Randall', 'Hound', '96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', 'exe.android@gmail.com', null, null, null, '80');
INSERT INTO `users` VALUES ('3', 'asterious1', 'Randall', 'Hound', '96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', 'exe.android@gmail.com', null, null, null, '1000');

-- ----------------------------
-- Table structure for users_customize
-- ----------------------------
DROP TABLE IF EXISTS `users_customize`;
CREATE TABLE `users_customize` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` int(6) NOT NULL,
  `GTAO_SHAPE_FIRST_ID` int(3) DEFAULT NULL,
  `GTAO_SHAPE_SECOND_ID` int(3) DEFAULT NULL,
  `GTAO_SKIN_FIRST_ID` int(3) DEFAULT NULL,
  `GTAO_HAIR` int(3) DEFAULT NULL,
  `GTAO_HAIR_COLOR` int(3) DEFAULT NULL,
  `GTAO_HAIR_HIGHLIGHT_COLOR` int(3) DEFAULT NULL,
  `GTAO_EYE_COLOR` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_TSHIRT` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_TSHIRT_VARIATION` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_CLOTHES` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_CLOTHES_VARIATION` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_TORSO` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_LEG` int(3) DEFAULT NULL,
  `GTAO_PED_COMPONENT_LEG_VARIATION` int(3) DEFAULT NULL,
  `SHAPES` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`player_id`),
  KEY `player_id` (`player_id`),
  CONSTRAINT `player_id` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_customize
-- ----------------------------
INSERT INTO `users_customize` VALUES ('3', '2', '0', '8', '10', '11', '63', '15', '23', '53', '0', '35', '0', '1', '3', '2', '1;0;0;0;0;0;0;0;1;0;0;0;0;0;0;1;2;0;1;-2;0');
INSERT INTO `users_customize` VALUES ('6', '3', '0', '11', '16', '11', '38', '3', '0', '26', '12', '106', '0', '1', '24', '0', '0;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;2;1;1;0;0');
INSERT INTO `users_customize` VALUES ('7', '1', '4', '1', '12', '14', '0', '0', '0', '23', '2', '20', '1', '1', '4', '0', '0;0;0;0;0;0;0;0;0;0;1;0;0;1;0;0;0;0;0;0;0');

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` enum('Fraction','Player','Business') DEFAULT NULL,
  `owner_id` int(5) DEFAULT NULL,
  `model` int(13) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_type` (`owner_type`) USING BTREE,
  KEY `model` (`model`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('1', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('2', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('3', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('4', 'Business', '1', '2112052861');
INSERT INTO `vehicles` VALUES ('5', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('6', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('7', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('8', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('9', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('10', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('11', 'Business', '1', '887537515');
INSERT INTO `vehicles` VALUES ('12', 'Player', '1', '1981688531');
INSERT INTO `vehicles` VALUES ('13', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('14', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('15', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('16', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('17', 'Business', '2', '-1352468814');
INSERT INTO `vehicles` VALUES ('18', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('19', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('20', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('21', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('22', 'Business', '2', '569305213');
INSERT INTO `vehicles` VALUES ('23', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('24', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('25', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('26', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('27', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('28', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('29', 'Business', '2', '1491375716');
INSERT INTO `vehicles` VALUES ('30', 'Business', '2', '1491375716');

-- ----------------------------
-- Table structure for vehicles_data
-- ----------------------------
DROP TABLE IF EXISTS `vehicles_data`;
CREATE TABLE `vehicles_data` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(5) unsigned DEFAULT NULL,
  `spawnpos_x` double(7,3) DEFAULT NULL,
  `spawnpos_y` double(7,3) DEFAULT NULL,
  `spawnpos_z` double(7,3) DEFAULT NULL,
  `rotation_x` double(7,3) DEFAULT NULL,
  `rotation_y` double(7,3) DEFAULT NULL,
  `rotation_z` double(7,3) DEFAULT NULL,
  `color_1` int(4) DEFAULT NULL,
  `color_2` int(4) DEFAULT NULL,
  `number_plate` varchar(8) DEFAULT NULL,
  `number_plate_style` int(2) DEFAULT NULL,
  `fuel` double(5,2) DEFAULT NULL,
  `mileage` double(6,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`),
  CONSTRAINT `vehicle_id` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles_data
-- ----------------------------
INSERT INTO `vehicles_data` VALUES ('1', '1', '-731.456', '2670.481', '56.266', '2.259', '-2.011', '68.116', '1', '1', 'TRUCK', '2', '20.00', '16.0');
INSERT INTO `vehicles_data` VALUES ('2', '2', '-736.456', '2666.481', '56.266', '2.259', '-2.011', '68.116', '1', '1', 'GUAD', '8', '20.00', '13.0');
INSERT INTO `vehicles_data` VALUES ('3', '3', '-731.456', '2660.481', '56.266', '-0.101', '-0.930', '-89.011', '5', '5', 'TRUCK', '3', '60.00', '72000.0');
INSERT INTO `vehicles_data` VALUES ('4', '3', '-1906.058', '2017.039', '140.539', '-0.207', '-3.757', '-91.171', '5', '5', 'TRUCK', '3', '60.00', '72000.0');
INSERT INTO `vehicles_data` VALUES ('5', '4', '-1904.400', '2012.925', '140.840', '-0.368', '-4.583', '-89.194', '5', '5', 'TRUCK', '3', '60.00', '72000.0');
INSERT INTO `vehicles_data` VALUES ('6', '6', '-1904.893', '2008.472', '141.166', '-0.858', '-3.590', '-87.902', '5', '5', 'TRUCK', '3', '55.00', '62500.0');
INSERT INTO `vehicles_data` VALUES ('7', '7', '-1904.088', '2004.354', '141.403', '-0.807', '-2.349', '-89.809', '5', '5', 'TRUCK', '3', '47.00', '48900.0');
INSERT INTO `vehicles_data` VALUES ('8', '8', '-1903.950', '2000.376', '141.516', '-1.680', '-1.369', '-93.256', '5', '5', 'TRUCK', '3', '59.00', '79600.0');
INSERT INTO `vehicles_data` VALUES ('9', '9', '-1888.211', '2030.069', '140.312', '-0.338', '-0.052', '160.756', '5', '5', 'TRUCK', '3', '67.00', '55900.0');
INSERT INTO `vehicles_data` VALUES ('10', '10', '-1892.027', '2031.806', '140.322', '-0.121', '-0.148', '161.099', '5', '5', 'TRUCK', '3', '54.00', '38650.0');
INSERT INTO `vehicles_data` VALUES ('11', '11', '-1896.028', '2033.102', '140.329', '-0.095', '-0.148', '162.235', '5', '5', 'TRUCK', '3', '46.00', '47900.0');
INSERT INTO `vehicles_data` VALUES ('12', '12', '-1272.975', '-2983.458', '13.000', '14.940', '0.000', '0.000', '5', '5', 'TRUCK', '3', '999.00', '61580.0');
INSERT INTO `vehicles_data` VALUES ('13', '13', '1735.306', '-1549.103', '112.260', '-0.343', '0.237', '-104.188', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('14', '14', '1742.659', '-1526.337', '112.199', '-0.500', '-0.479', '-115.116', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('15', '15', '1747.763', '-1508.865', '112.402', '-0.118', '-0.089', '-112.392', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('16', '16', '1752.175', '-1494.253', '112.404', '0.237', '1.052', '-108.856', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('17', '17', '1753.943', '-1484.996', '112.464', '-0.124', '0.111', '-109.461', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('18', '18', '1687.028', '-1537.995', '112.363', '0.121', '-0.592', '70.768', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('19', '19', '1692.030', '-1520.964', '112.506', '0.109', '-0.310', '73.284', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('20', '20', '1695.524', '-1508.573', '112.569', '0.236', '0.234', '71.817', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('21', '21', '1699.850', '-1494.256', '112.584', '0.246', '0.064', '68.025', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('22', '22', '1705.311', '-1482.049', '112.488', '0.171', '-0.092', '74.411', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('23', '23', '-748.351', '2622.448', '60.233', '-12.232', '2.083', '-95.416', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('24', '24', '-748.030', '2626.510', '60.358', '-11.495', '1.804', '-93.784', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('25', '25', '-747.287', '2631.814', '60.384', '-10.115', '1.209', '-95.060', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('26', '26', '-719.599', '2641.544', '57.658', '2.939', '-6.498', '150.041', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('27', '27', '-722.264', '2630.563', '57.776', '9.423', '7.367', '24.145', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('28', '28', '-741.808', '2573.644', '59.578', '-0.399', '-1.647', '20.659', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('29', '29', '-740.807', '2586.081', '59.766', '0.277', '-4.390', '136.292', '1', '1', 'RLP', '1', '10.00', '0.0');
INSERT INTO `vehicles_data` VALUES ('30', '30', '-756.275', '2570.113', '59.623', '-4.567', '3.910', '-36.412', '1', '1', 'RLP', '1', '10.00', '0.0');

-- ----------------------------
-- Table structure for vehicles_model_data
-- ----------------------------
DROP TABLE IF EXISTS `vehicles_model_data`;
CREATE TABLE `vehicles_model_data` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(13) DEFAULT NULL,
  `model_name` char(50) DEFAULT NULL,
  `fuel_drain_idle` double(3,1) DEFAULT NULL,
  `fuel_drain_inmove` double(3,1) DEFAULT NULL,
  `fuel_tank` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles_model_data
-- ----------------------------
INSERT INTO `vehicles_model_data` VALUES ('1', '-1700801569', 'Scrap', '5.0', '22.0', '120');
INSERT INTO `vehicles_model_data` VALUES ('2', '887537515', 'UtilliTruck2', '5.0', '18.0', '65');
INSERT INTO `vehicles_model_data` VALUES ('3', '1981688531', 'Titan', '20.0', '66.0', '999');
INSERT INTO `vehicles_model_data` VALUES ('4', '2112052861', 'Pounder', '4.0', '18.0', '120');
INSERT INTO `vehicles_model_data` VALUES ('5', '410882957', null, '2.0', '13.0', '60');
INSERT INTO `vehicles_model_data` VALUES ('6', '444583674', 'Handler', '2.0', '7.0', '25');
INSERT INTO `vehicles_model_data` VALUES ('7', '2016027501', 'TrailerLogs', '0.0', '0.0', '0');
INSERT INTO `vehicles_model_data` VALUES ('8', '516990260', 'UtilityTruck', '3.0', '28.0', '80');
INSERT INTO `vehicles_model_data` VALUES ('9', '569305213', 'Packer', '5.0', '36.0', '240');
INSERT INTO `vehicles_model_data` VALUES ('10', '-1352468814', 'TRFlat', '0.0', '0.0', '0');
INSERT INTO `vehicles_model_data` VALUES ('11', '1491375716', 'Forklift', '2.0', '9.0', '25');

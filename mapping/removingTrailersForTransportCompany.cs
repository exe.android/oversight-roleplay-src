using System;
using System.Collections.Generic;
using GTA;
using GTA.Math;
using GTA.Native;


public class MapEditorGeneratedMap : GTA.Script
{
    public MapEditorGeneratedMap()
    {
        List<int> Props = new List<int>();
        int LodDistance = 3000;            

        Func<int, Vector3, Vector3, bool, int> createProp = new Func<int, Vector3, Vector3, bool, int>(delegate(int hash, Vector3 pos, Vector3 rot, bool dynamic)
	    {
		    Model model = new Model(hash);
		    model.Request(10000);
		    Prop prop = GTA.World.CreateProp(model, pos, rot, dynamic, false);
		    prop.Position = pos;
            prop.LodDistance = LodDistance;
            if (!dynamic)
                prop.FreezePosition = true;
		    return prop.Handle;
	    });

        bool Initialized = false;


        base.Tick += delegate (object sender, EventArgs args)
        {
            if (!Initialized)
            {
                /* PROPS */
                Props.Add(createProp(-1098506160, new GTA.Math.Vector3(846.3602f, 2366.75f, 52.55337f), new GTA.Math.Vector3(0f, 0f, 48.18261f), true));
                Props.Add(createProp(682074297, new GTA.Math.Vector3(2718.456f, 2791.249f, 35.61909f), new GTA.Math.Vector3(0f, 0f, -54.99992f), true));


                /* VEHICLES */


                /* PEDS */


                /* PICKUPS */

                Initialized = true;
            }

            /* MARKERS */


            /* WORLD */
            Prop returnedProp;
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1719.542f, -1608.672f, 111.4629f, 1f, -1853453107, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1732.253f, -1549.694f, 111.6817f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1740.005f, -1526.777f, 111.644f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1744.801f, -1515.616f, 111.8052f, 1f, -531344027, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1747.825f, -1504.218f, 111.8182f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1751.805f, -1492.811f, 111.8406f, 1f, -531344027, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1693.145f, -1522.824f, 111.8753f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1689.591f, -1534.098f, 111.8148f, 1f, -531344027, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1695.987f, -1511.578f, 111.9594f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1704.871f, -1488.615f, 111.9217f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1709.463f, -1477.641f, 111.9237f, 1f, 1152297372, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();
            returnedProp = Function.Call<Prop>(Hash.GET_CLOSEST_OBJECT_OF_TYPE, 1662.862f, -1519.605f, 111.6843f, 1f, 1888301071, 0);
            if (returnedProp != null && returnedProp.Handle != 0 && !Props.Contains(returnedProp.Handle))
                returnedProp.Delete();

        };
    }
}
class API{
    /**
     * @param target object
     * @returns void
     */
    static startCoroutine(target){
    }
 
    /**
     * @param show bool
     * @returns void
     */
    static showCursor(show){
    }
 
    /**
     * @returns bool
     */
    static isCursorShown(){
    }
 
    /**
     * @param name string
     * @param value object
     * @returns void
     */
    static setSetting(name, value){
    }
 
    /**
     * @param name string
     * @returns object
     */
    static getSetting(name){
    }
 
    /**
     * @param name string
     * @returns bool
     */
    static doesSettingExist(name){
    }
 
    /**
     * @param name string
     * @returns void
     */
    static removeSetting(name){
    }
 
    /**
     * @returns JavascriptChat
     */
    static registerChatOverride(){
    }
 
    /**
     * @param position Vector3
     * @param rotation Vector3
     * @returns GlobalCamera
     */
    static createCamera(position, rotation){
    }
 
    /**
     * @param camera GlobalCamera
     * @returns void
     */
    static setActiveCamera(camera){
    }
 
    /**
     * @returns void
     */
    static setGameplayCameraActive(){
    }
 
    /**
     * @returns GlobalCamera
     */
    static getActiveCamera(){
    }
 
    /**
     * @param cam GlobalCamera
     * @param shakeType string
     * @param amplitute float
     * @returns void
     */
    static setCameraShake(cam, shakeType, amplitute){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns void
     */
    static stopCameraShake(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns bool
     */
    static isCameraShaking(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @param pos Vector3
     * @returns void
     */
    static setCameraPosition(cam, pos){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns Vector3
     */
    static getCameraPosition(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @param rotation Vector3
     * @returns void
     */
    static setCameraRotation(cam, rotation){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns Vector3
     */
    static getCameraRotation(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @param fov float
     * @returns void
     */
    static setCameraFov(cam, fov){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns float
     */
    static getCameraFov(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @param pos Vector3
     * @returns void
     */
    static pointCameraAtPosition(cam, pos){
    }
 
    /**
     * @param cam GlobalCamera
     * @param ent LocalHandle
     * @param offset Vector3
     * @returns void
     */
    static pointCameraAtEntity(cam, ent, offset){
    }
 
    /**
     * @param cam GlobalCamera
     * @param ent LocalHandle
     * @param bone int
     * @param offset Vector3
     * @returns void
     */
    static pointCameraAtEntityBone(cam, ent, bone, offset){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns void
     */
    static stopCameraPointing(cam){
    }
 
    /**
     * @param cam GlobalCamera
     * @param ent LocalHandle
     * @param offset Vector3
     * @returns void
     */
    static attachCameraToEntity(cam, ent, offset){
    }
 
    /**
     * @param cam GlobalCamera
     * @param ent LocalHandle
     * @param bone int
     * @param offset Vector3
     * @returns void
     */
    static attachCameraToEntityBone(cam, ent, bone, offset){
    }
 
    /**
     * @param cam GlobalCamera
     * @returns void
     */
    static detachCamera(cam){
    }
 
    /**
     * @param from GlobalCamera
     * @param to GlobalCamera
     * @param duration double
     * @param easepos bool
     * @param easerot bool
     * @returns void
     */
    static interpolateCameras(from, to, duration, easepos, easerot){
    }
 
    /**
     * @returns PointF
     */
    static getCursorPosition(){
    }
 
    /**
     * @returns PointF
     */
    static getCursorPositionMantainRatio(){
    }
 
    /**
     * @param pos Vector3
     * @returns PointF
     */
    static worldToScreen(pos){
    }
 
    /**
     * @param pos Vector3
     * @returns PointF
     */
    static worldToScreenMantainRatio(pos){
    }
 
    /**
     * @returns string
     */
    static getCurrentResourceName(){
    }
 
    /**
     * @param pos PointF
     * @param camPos Vector3
     * @param camRot Vector3
     * @returns Vector3
     */
    static screenToWorld(pos, camPos, camRot){
    }
 
    /**
     * @param pos PointF
     * @param camPos Vector3
     * @param camrot Vector3
     * @returns Vector3
     */
    static screenToWorldMantainRatio(pos, camPos, camrot){
    }
 
    /**
     * @param start Vector3
     * @param end Vector3
     * @param flag int
     * @param ignoreEntity LocalHandle?
     * @returns ScriptContext.Raycast
     */
    static createRaycast(start, end, flag, ignoreEntity){
    }
 
    /**
     * @returns Vector3
     */
    static getGameplayCamPos(){
    }
 
    /**
     * @returns Vector3
     */
    static getGameplayCamRot(){
    }
 
    /**
     * @returns Vector3
     */
    static getGameplayCamDir(){
    }
 
    /**
     * @param show bool
     * @returns void
     */
    static setCanOpenChat(show){
    }
 
    /**
     * @returns bool
     */
    static getCanOpenChat(){
    }
 
    /**
     * @param width double
     * @param height double
     * @param local bool
     * @returns Browser
     */
    static createCefBrowser(width, height, local){
    }
 
    /**
     * @param browser Browser
     * @returns void
     */
    static destroyCefBrowser(browser){
    }
 
    /**
     * @param browser Browser
     * @returns bool
     */
    static isCefBrowserInitialized(browser){
    }
 
    /**
     * @param browser Browser
     * @returns void
     */
    static waitUntilCefBrowserInit(browser){
    }
 
    /**
     * @param browser Browser
     * @param width double
     * @param height double
     * @returns void
     */
    static setCefBrowserSize(browser, width, height){
    }
 
    /**
     * @param browser Browser
     * @returns Size
     */
    static getCefBrowserSize(browser){
    }
 
    /**
     * @param browser Browser
     * @param headless bool
     * @returns void
     */
    static setCefBrowserHeadless(browser, headless){
    }
 
    /**
     * @param browser Browser
     * @returns bool
     */
    static getCefBrowserHeadless(browser){
    }
 
    /**
     * @param browser Browser
     * @param xPos double
     * @param yPos double
     * @returns void
     */
    static setCefBrowserPosition(browser, xPos, yPos){
    }
 
    /**
     * @param browser Browser
     * @returns Point
     */
    static getCefBrowserPosition(browser){
    }
 
    /**
     * @param browser Browser
     * @param x1 double
     * @param y1 double
     * @param x2 double
     * @param y2 double
     * @param x3 double
     * @param y3 double
     * @param x4 double
     * @param y4 double
     * @returns void
     */
    static pinCefBrowser(browser, x1, y1, x2, y2, x3, y3, x4, y4){
    }
 
    /**
     * @param browser Browser
     * @returns void
     */
    static clearCefPinning(browser){
    }
 
    /**
     * @returns void
     */
    static verifyIntegrityOfCache(){
    }
 
    /**
     * @param browser Browser
     * @param uri string
     * @returns void
     */
    static loadPageCefBrowser(browser, uri){
    }
 
    /**
     * @param browser Browser
     * @param html string
     * @returns void
     */
    static loadHtmlCefBrowser(browser, html){
    }
 
    /**
     * @param browser Browser
     * @returns bool
     */
    static isCefBrowserLoading(browser){
    }
 
    /**
     * @param hash string
     * @param args object[]
     * @returns void
     */
    static callNative(hash, args){
    }
 
    /**
     * @param hash string
     * @param returnType int
     * @param args object[]
     * @returns object
     */
    static returnNative(hash, returnType, args){
    }
 
    /**
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setUiColor(r, g, b){
    }
 
    /**
     * @param input string
     * @returns int
     */
    static getHashKey(input){
    }
 
    /**
     * @param entity LocalHandle
     * @param key string
     * @param data object
     * @returns bool
     */
    static setEntitySyncedData(entity, key, data){
    }
 
    /**
     * @param entity LocalHandle
     * @param key string
     * @returns void
     */
    static resetEntitySyncedData(entity, key){
    }
 
    /**
     * @param entity LocalHandle
     * @param key string
     * @returns bool
     */
    static hasEntitySyncedData(entity, key){
    }
 
    /**
     * @param entity LocalHandle
     * @param key string
     * @returns object
     */
    static getEntitySyncedData(entity, key){
    }
 
    /**
     * @param key string
     * @param data object
     * @returns bool
     */
    static setWorldData(key, data){
    }
 
    /**
     * @param key string
     * @returns void
     */
    static resetWorldData(key){
    }
 
    /**
     * @param key string
     * @returns bool
     */
    static hasWorldData(key){
    }
 
    /**
     * @param key string
     * @returns object
     */
    static getWorldData(key){
    }
 
    /**
     * @returns int
     */
    static getGamePlayer(){
    }
 
    /**
     * @returns LocalHandle
     */
    static getLocalPlayer(){
    }
 
    /**
     * @param entity LocalHandle
     * @returns Vector3
     */
    static getEntityPosition(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns Vector3
     */
    static getEntityRotation(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns Vector3
     */
    static getEntityVelocity(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns float
     */
    static getVehicleHealth(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns float
     */
    static getVehicleRPM(entity){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerInAnyVehicle(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerOnFire(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerParachuting(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerInFreefall(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerAiming(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerShooting(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerReloading(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerInCover(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerOnLadder(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns Vector3
     */
    static getPlayerAimingPoint(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static isPlayerDead(player){
    }
 
    /**
     * @param entity LocalHandle
     * @returns bool
     */
    static doesEntityExist(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @param invincible bool
     * @returns void
     */
    static setEntityInvincible(entity, invincible){
    }
 
    /**
     * @param entity LocalHandle
     * @returns bool
     */
    static getEntityInvincible(entity){
    }
 
    /**
     * @returns bool
     */
    static getLocalPlayerInvincible(){
    }
 
    /**
     * @param ptfxLibrary string
     * @param ptfxName string
     * @param position Vector3
     * @param rotation Vector3
     * @param scale float
     * @returns void
     */
    static createParticleEffectOnPosition(ptfxLibrary, ptfxName, position, rotation, scale){
    }
 
    /**
     * @param ptfxLibrary string
     * @param ptfxName string
     * @param entity LocalHandle
     * @param offset Vector3
     * @param rotation Vector3
     * @param scale float
     * @param boneIndex int
     * @returns void
     */
    static createParticleEffectOnEntity(ptfxLibrary, ptfxName, entity, offset, rotation, scale, boneIndex){
    }
 
    /**
     * @param explosionType int
     * @param position Vector3
     * @param damageScale float
     * @returns void
     */
    static createExplosion(explosionType, position, damageScale){
    }
 
    /**
     * @param owner LocalHandle
     * @param explosionType int
     * @param position Vector3
     * @param damageScale float
     * @returns void
     */
    static createOwnedExplosion(owner, explosionType, position, damageScale){
    }
 
    /**
     * @param weapon int
     * @param start Vector3
     * @param target Vector3
     * @param damage int
     * @param speed float
     * @param dimension int
     * @returns void
     */
    static createProjectile(weapon, start, target, damage, speed, dimension){
    }
 
    /**
     * @param owner LocalHandle
     * @param weapon int
     * @param start Vector3
     * @param target Vector3
     * @param damage int
     * @param speed float
     * @param dimension int
     * @returns void
     */
    static createOwnedProjectile(owner, weapon, start, target, damage, speed, dimension){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param livery int
     * @returns void
     */
    static setVehicleLivery(vehicle, livery){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleLivery(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param locked bool
     * @returns void
     */
    static setVehicleLocked(vehicle, locked){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getVehicleLocked(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns LocalHandle
     */
    static getVehicleTrailer(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns LocalHandle
     */
    static getVehicleTraileredBy(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getVehicleSirenState(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param tyre int
     * @returns bool
     */
    static isVehicleTyrePopped(vehicle, tyre){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param tyre int
     * @param pop bool
     * @returns void
     */
    static popVehicleTyre(vehicle, tyre, pop){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param door int
     * @returns bool
     */
    static isVehicleDoorBroken(vehicle, door){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param door int
     * @param open bool
     * @returns void
     */
    static setVehicleDoorState(vehicle, door, open){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param door int
     * @returns bool
     */
    static getVehicleDoorState(vehicle, door){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param door int
     * @param breakDoor bool
     * @returns void
     */
    static breakVehicleTyre(vehicle, door, breakDoor){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param window int
     * @returns bool
     */
    static isVehicleWindowBroken(vehicle, window){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param window int
     * @param breakWindow bool
     * @returns void
     */
    static breakVehicleWindow(vehicle, window, breakWindow){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @param enabled bool
     * @returns void
     */
    static setVehicleExtra(vehicle, slot, enabled){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @returns bool
     */
    static getVehicleExtra(vehicle, slot){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param plate string
     * @returns void
     */
    static setVehicleNumberPlate(vehicle, plate){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns string
     */
    static getVehicleNumberPlate(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param turnedOn bool
     * @returns void
     */
    static setVehicleEngineStatus(vehicle, turnedOn){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getEngineStatus(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param status bool
     * @returns void
     */
    static setVehicleSpecialLightStatus(vehicle, status){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getVehicleSpecialLightStatus(vehicle){
    }
 
    /**
     * @param entity LocalHandle
     * @param status bool
     * @returns void
     */
    static setEntityCollissionless(entity, status){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getEntityCollisionless(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @param modType int
     * @returns void
     */
    static setVehicleMod(vehicle, slot, modType){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @returns int
     */
    static getVehicleMod(vehicle, slot){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @returns void
     */
    static removeVehicleMod(vehicle, slot){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param bulletproof bool
     * @returns void
     */
    static setVehicleBulletproofTyres(vehicle, bulletproof){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns bool
     */
    static getVehicleBulletproofTyres(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param style int
     * @returns void
     */
    static setVehicleNumberPlateStyle(vehicle, style){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleNumberPlateStyle(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param color int
     * @returns void
     */
    static setVehiclePearlescentColor(vehicle, color){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehiclePearlescentColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param color int
     * @returns void
     */
    static setVehicleWheelColor(vehicle, color){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleWheelColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param type int
     * @returns void
     */
    static setVehicleWheelType(vehicle, type){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleWheelType(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleModColor1(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleModColor2(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleTyreSmokeColor(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param type int
     * @returns void
     */
    static setVehicleWindowTint(vehicle, type){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleWindowTint(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param mult float
     * @returns void
     */
    static setVehicleEnginePowerMultiplier(vehicle, mult){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns float
     */
    static getVehicleEnginePowerMultiplier(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param mult float
     * @returns void
     */
    static setVehicleEngineTorqueMultiplier(vehicle, mult){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns float
     */
    static getVehicleEngineTorqueMultiplier(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @param turnedOn bool
     * @returns void
     */
    static setVehicleNeonState(vehicle, slot, turnedOn){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param slot int
     * @returns bool
     */
    static getVehicleNeonState(vehicle, slot){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleNeonColor(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param type int
     * @returns void
     */
    static setVehicleDashboardColor(vehicle, type){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleDashboardColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param type int
     * @returns void
     */
    static setVehicleTrimColor(vehicle, type){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleTrimColor(vehicle){
    }
 
    /**
     * @param model int
     * @returns string
     */
    static getVehicleDisplayName(model){
    }
 
    /**
     * @param model int
     * @returns float
     */
    static getVehicleMaxSpeed(model){
    }
 
    /**
     * @param model int
     * @returns float
     */
    static getVehicleMaxBraking(model){
    }
 
    /**
     * @param model int
     * @returns float
     */
    static getVehicleMaxTraction(model){
    }
 
    /**
     * @param model int
     * @returns float
     */
    static getVehicleMaxAcceleration(model){
    }
 
    /**
     * @param model int
     * @returns float
     */
    static getVehicleMaxOccupants(model){
    }
 
    /**
     * @param model int
     * @returns int
     */
    static getVehicleClass(model){
    }
 
    /**
     * @returns void
     */
    static detonatePlayerStickies(){
    }
 
    /**
     * @param player LocalHandle
     * @param text string
     * @returns void
     */
    static setPlayerNametag(player, text){
    }
 
    /**
     * @param player LocalHandle
     * @returns void
     */
    static resetPlayerNametag(player){
    }
 
    /**
     * @param player LocalHandle
     * @param show bool
     * @returns void
     */
    static setPlayerNametagVisible(player, show){
    }
 
    /**
     * @param player LocalHandle
     * @returns bool
     */
    static getPlayerNametagVisible(player){
    }
 
    /**
     * @param player LocalHandle
     * @param r byte
     * @param g byte
     * @param b byte
     * @returns void
     */
    static setPlayerNametagColor(player, r, g, b){
    }
 
    /**
     * @param player LocalHandle
     * @returns void
     */
    static resetPlayerNametagColor(player){
    }
 
    /**
     * @param model int
     * @returns void
     */
    static setPlayerSkin(model){
    }
 
    /**
     * @returns void
     */
    static setPlayerDefaultClothes(){
    }
 
    /**
     * @param team int
     * @returns void
     */
    static setPlayerTeam(team){
    }
 
    /**
     * @returns int
     */
    static getPlayerTeam(){
    }
 
    /**
     * @param name string
     * @returns void
     */
    static playPlayerScenario(name){
    }
 
    /**
     * @param animDict string
     * @param animName string
     * @param flag int
     * @param duration int
     * @returns void
     */
    static playPlayerAnimation(animDict, animName, flag, duration){
    }
 
    /**
     * @returns void
     */
    static stopPlayerAnimation(){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param color int
     * @returns void
     */
    static setVehiclePrimaryColor(vehicle, color){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param color int
     * @returns void
     */
    static setVehicleSecondaryColor(vehicle, color){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleCustomPrimaryColor(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setVehicleCustomSecondaryColor(vehicle, r, g, b){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns Color
     */
    static getVehicleCustomPrimaryColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns Color
     */
    static getVehicleCustomSecondaryColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehiclePrimaryColor(vehicle){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns int
     */
    static getVehicleSecondaryColor(vehicle){
    }
 
    /**
     * @param player LocalHandle
     * @param slot int
     * @param drawable int
     * @param texture int
     * @returns void
     */
    static setPlayerClothes(player, slot, drawable, texture){
    }
 
    /**
     * @param player LocalHandle
     * @param slot int
     * @param drawable int
     * @param texture int
     * @returns void
     */
    static setPlayerAccessory(player, slot, drawable, texture){
    }
 
    /**
     * @param player LocalHandle
     * @param slot int
     * @returns void
     */
    static clearPlayerAccessory(player, slot){
    }
 
    /**
     * @param modelName string
     * @returns int
     */
    static vehicleNameToModel(modelName){
    }
 
    /**
     * @param modelName string
     * @returns int
     */
    static pedNameToModel(modelName){
    }
 
    /**
     * @param modelName string
     * @returns int
     */
    static weaponNameToModel(modelName){
    }
 
    /**
     * @param pos Vector3
     * @returns void
     */
    static loadInterior(pos){
    }
 
    /**
     * @returns void
     */
    static clearPlayerTasks(){
    }
 
    /**
     * @param entity LocalHandle
     * @param frozen bool
     * @returns void
     */
    static setEntityPositionFrozen(entity, frozen){
    }
 
    /**
     * @param entity LocalHandle
     * @param velocity Vector3
     * @returns void
     */
    static setEntityVelocity(entity, velocity){
    }
 
    /**
     * @param player LocalHandle
     * @returns int
     */
    static getPlayerVehicleSeat(player){
    }
 
    /**
     * @param weapon int
     * @param tint int
     * @returns void
     */
    static setPlayerWeaponTint(weapon, tint){
    }
 
    /**
     * @param weapon int
     * @returns int
     */
    static getPlayerWeaponTint(weapon){
    }
 
    /**
     * @param weapon int
     * @param component int
     * @returns void
     */
    static givePlayerWeaponComponent(weapon, component){
    }
 
    /**
     * @param weapon int
     * @param component int
     * @returns void
     */
    static removePlayerWeaponComponent(weapon, component){
    }
 
    /**
     * @param weapon int
     * @param component int
     * @returns bool
     */
    static hasPlayerWeaponComponent(weapon, component){
    }
 
    /**
     * @param weapon WeaponHash
     * @returns WeaponComponent[]
     */
    static getAllWeaponComponents(weapon){
    }
 
    /**
     * @returns int
     */
    static getPlayerCurrentWeapon(){
    }
 
    /**
     * @param reason string
     * @returns void
     */
    static disconnect(reason){
    }
 
    /**
     * @param ent LocalHandle
     * @param pos Vector3
     * @returns void
     */
    static setEntityPosition(ent, pos){
    }
 
    /**
     * @param ent LocalHandle
     * @param rot Vector3
     * @returns void
     */
    static setEntityRotation(ent, rot){
    }
 
    /**
     * @param vehicle LocalHandle
     * @param seat int
     * @returns void
     */
    static setPlayerIntoVehicle(vehicle, seat){
    }
 
    /**
     * @param health int
     * @returns void
     */
    static setPlayerHealth(health){
    }
 
    /**
     * @param player LocalHandle
     * @returns int
     */
    static getPlayerHealth(player){
    }
 
    /**
     * @param label LocalHandle
     * @param text string
     * @returns void
     */
    static setTextLabelText(label, text){
    }
 
    /**
     * @param textLabel LocalHandle
     * @param alpha int
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setTextLabelColor(textLabel, alpha, r, g, b){
    }
 
    /**
     * @param textLabel LocalHandle
     * @returns Color
     */
    static getTextLabelColor(textLabel){
    }
 
    /**
     * @param handle LocalHandle
     * @param seethrough bool
     * @returns void
     */
    static setTextLabelSeethrough(handle, seethrough){
    }
 
    /**
     * @param handle LocalHandle
     * @returns bool
     */
    static getTextLabelSeethrough(handle){
    }
 
    /**
     * @param entity LocalHandle
     * @param offset Vector3
     * @returns Vector3
     */
    static getOffsetInWorldCoords(entity, offset){
    }
 
    /**
     * @param entity LocalHandle
     * @param pos Vector3
     * @returns Vector3
     */
    static getOffsetFromWorldCoords(entity, pos){
    }
 
    /**
     * @param start Vector3
     * @param end Vector3
     * @param a int
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static drawLine(start, end, a, r, g, b){
    }
 
    /**
     * @param audioLib string
     * @param audioName string
     * @returns void
     */
    static playSoundFrontEnd(audioLib, audioName){
    }
 
    /**
     * @param text string
     * @param timeout int
     * @returns void
     */
    static showShard(text, timeout){
    }
 
    /**
     * @param config string
     * @returns XmlGroup
     */
    static loadConfig(config){
    }
 
    /**
     * @param json string
     * @returns object
     */
    static fromJson(json){
    }
 
    /**
     * @param data object
     * @returns string
     */
    static toJson(data){
    }
 
    /**
     * @returns SizeF
     */
    static getScreenResolutionMantainRatio(){
    }
 
    /**
     * @param text string
     * @returns void
     */
    static sendChatMessage(text){
    }
 
    /**
     * @returns Size
     */
    static getScreenResolution(){
    }
 
    /**
     * @param text string
     * @returns void
     */
    static sendNotification(text){
    }
 
    /**
     * @param text string
     * @param duration double
     * @returns void
     */
    static displaySubtitle(text, duration){
    }
 
    /**
     * @param ms double
     * @param format string
     * @returns string
     */
    static formatTime(ms, format){
    }
 
    /**
     * @param invinc bool
     * @returns void
     */
    static setPlayerInvincible(invinc){
    }
 
    /**
     * @param wantedLevel int
     * @returns void
     */
    static setPlayerWantedLevel(wantedLevel){
    }
 
    /**
     * @returns int
     */
    static getPlayerWantedLevel(){
    }
 
    /**
     * @returns bool
     */
    static getPlayerInvincible(){
    }
 
    /**
     * @param armor int
     * @returns void
     */
    static setPlayerArmor(armor){
    }
 
    /**
     * @param player LocalHandle
     * @returns int
     */
    static getPlayerArmor(player){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedPlayers(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedVehicles(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedObjects(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedPickups(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedPeds(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedMarkers(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getStreamedTextLabels(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllPlayers(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllVehicles(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllObjects(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllPickups(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllPeds(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllMarkers(){
    }
 
    /**
     * @returns LocalHandle[]
     */
    static getAllTextLabels(){
    }
 
    /**
     * @param player LocalHandle
     * @returns LocalHandle
     */
    static getPlayerVehicle(player){
    }
 
    /**
     * @param vehicle LocalHandle
     * @returns void
     */
    static explodeVehicle(vehicle){
    }
 
    /**
     * @param name string
     * @returns LocalHandle
     */
    static getPlayerByName(name){
    }
 
    /**
     * @param player LocalHandle
     * @returns string
     */
    static getPlayerName(player){
    }
 
    /**
     * @param force bool
     * @returns void
     */
    static forceSendAimData(force){
    }
 
    /**
     * @returns bool
     */
    static isAimDataForced(){
    }
 
    /**
     * @param player LocalHandle
     * @returns Vector3
     */
    static getPlayerAimCoords(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns int
     */
    static getPlayerPing(player){
    }
 
    /**
     * @param model int
     * @param pos Vector3
     * @param heading float
     * @returns LocalHandle
     */
    static createVehicle(model, pos, heading){
    }
 
    /**
     * @param model int
     * @param pos Vector3
     * @param heading float
     * @returns LocalHandle
     */
    static createPed(model, pos, heading){
    }
 
    /**
     * @param pos Vector3
     * @returns LocalHandle
     */
    static createBlip(pos){
    }
 
    /**
     * @param blip LocalHandle
     * @param pos Vector3
     * @returns void
     */
    static setBlipPosition(blip, pos){
    }
 
    /**
     * @param blip LocalHandle
     * @returns Vector3
     */
    static getBlipPosition(blip){
    }
 
    /**
     * @returns Vector3
     */
    static getWaypointPosition(){
    }
 
    /**
     * @returns bool
     */
    static isWaypointSet(){
    }
 
    /**
     * @param x double
     * @param y double
     * @returns void
     */
    static setWaypoint(x, y){
    }
 
    /**
     * @returns void
     */
    static removeWaypoint(){
    }
 
    /**
     * @param blip LocalHandle
     * @param color int
     * @returns void
     */
    static setBlipColor(blip, color){
    }
 
    /**
     * @param blip LocalHandle
     * @returns int
     */
    static getBlipColor(blip){
    }
 
    /**
     * @param blip LocalHandle
     * @param sprite int
     * @returns void
     */
    static setBlipSprite(blip, sprite){
    }
 
    /**
     * @param blip LocalHandle
     * @returns int
     */
    static getBlipSprite(blip){
    }
 
    /**
     * @param blip LocalHandle
     * @param name string
     * @returns void
     */
    static setBlipName(blip, name){
    }
 
    /**
     * @param blip LocalHandle
     * @returns string
     */
    static getBlipName(blip){
    }
 
    /**
     * @param blip LocalHandle
     * @param alpha int
     * @returns void
     */
    static setBlipTransparency(blip, alpha){
    }
 
    /**
     * @param blip LocalHandle
     * @returns int
     */
    static getBlipTransparency(blip){
    }
 
    /**
     * @param blip LocalHandle
     * @param shortRange bool
     * @returns void
     */
    static setBlipShortRange(blip, shortRange){
    }
 
    /**
     * @param blip LocalHandle
     * @returns bool
     */
    static getBlipShortRange(blip){
    }
 
    /**
     * @param blip LocalHandle
     * @param show bool
     * @returns void
     */
    static showBlipRoute(blip, show){
    }
 
    /**
     * @param blip LocalHandle
     * @param scale float
     * @returns void
     */
    static setBlipScale(blip, scale){
    }
 
    /**
     * @param blip LocalHandle
     * @returns float
     */
    static getBlipScale(blip){
    }
 
    /**
     * @param display bool
     * @returns void
     */
    static setChatVisible(display){
    }
 
    /**
     * @returns bool
     */
    static getChatVisible(){
    }
 
    /**
     * @returns float
     */
    static getAveragePacketSize(){
    }
 
    /**
     * @returns float
     */
    static getBytesSentPerSecond(){
    }
 
    /**
     * @returns float
     */
    static getBytesReceivedPerSecond(){
    }
 
    /**
     * @param player LocalHandle
     * @returns void
     */
    static requestControlOfPlayer(player){
    }
 
    /**
     * @param player LocalHandle
     * @returns void
     */
    static stopControlOfPlayer(player){
    }
 
    /**
     * @param visible bool
     * @returns void
     */
    static setHudVisible(visible){
    }
 
    /**
     * @returns bool
     */
    static isSpectating(){
    }
 
    /**
     * @returns bool
     */
    static getHudVisible(){
    }
 
    /**
     * @param markerType int
     * @param pos Vector3
     * @param dir Vector3
     * @param rot Vector3
     * @param scale Vector3
     * @param r int
     * @param g int
     * @param b int
     * @param alpha int
     * @returns LocalHandle
     */
    static createMarker(markerType, pos, dir, rot, scale, r, g, b, alpha){
    }
 
    /**
     * @param marker LocalHandle
     * @param type int
     * @returns void
     */
    static setMarkerType(marker, type){
    }
 
    /**
     * @param marker LocalHandle
     * @returns int
     */
    static getMarkerType(marker){
    }
 
    /**
     * @param marker LocalHandle
     * @param alpha int
     * @param r int
     * @param g int
     * @param b int
     * @returns void
     */
    static setMarkerColor(marker, alpha, r, g, b){
    }
 
    /**
     * @param marker LocalHandle
     * @returns Color
     */
    static getMarkerColor(marker){
    }
 
    /**
     * @param marker LocalHandle
     * @param scale Vector3
     * @returns void
     */
    static setMarkerScale(marker, scale){
    }
 
    /**
     * @param marker LocalHandle
     * @returns Vector3
     */
    static getMarkerScale(marker){
    }
 
    /**
     * @param marker LocalHandle
     * @param dir Vector3
     * @returns void
     */
    static setMarkerDirection(marker, dir){
    }
 
    /**
     * @param marker LocalHandle
     * @returns Vector3
     */
    static getMarkerDirection(marker){
    }
 
    /**
     * @param handle LocalHandle
     * @returns void
     */
    static deleteEntity(handle){
    }
 
    /**
     * @param ent1 LocalHandle
     * @param ent2 LocalHandle
     * @param bone string
     * @param positionOffset Vector3
     * @param rotationOffset Vector3
     * @returns void
     */
    static attachEntity(ent1, ent2, bone, positionOffset, rotationOffset){
    }
 
    /**
     * @param ent LocalHandle
     * @returns void
     */
    static detachEntity(ent){
    }
 
    /**
     * @param ent LocalHandle
     * @returns bool
     */
    static isEntityAttachedToAnything(ent){
    }
 
    /**
     * @param from LocalHandle
     * @param to LocalHandle
     * @returns bool
     */
    static isEntityAttachedToEntity(from, to){
    }
 
    /**
     * @param text string
     * @param pos Vector3
     * @param range float
     * @param size float
     * @returns LocalHandle
     */
    static createTextLabel(text, pos, range, size){
    }
 
    /**
     * @param start Vector3
     * @param end Vector3
     * @param currentTime double
     * @param duration double
     * @returns Vector3
     */
    static lerpVector(start, end, currentTime, duration){
    }
 
    /**
     * @param start double
     * @param end double
     * @param currentTime double
     * @param duration double
     * @returns double
     */
    static lerpFloat(start, end, currentTime, duration){
    }
 
    /**
     * @param entity Vector3
     * @param destination Vector3
     * @param range double
     * @returns bool
     */
    static isInRangeOf(entity, destination, range){
    }
 
    /**
     * @param path string
     * @param pos Point
     * @param size Size
     * @param id int
     * @returns void
     */
    static dxDrawTexture(path, pos, size, id){
    }
 
    /**
     * @param dict string
     * @param txtName string
     * @param x double
     * @param y double
     * @param width double
     * @param height double
     * @param heading double
     * @param r int
     * @param g int
     * @param b int
     * @param alpha int
     * @returns void
     */
    static drawGameTexture(dict, txtName, x, y, width, height, heading, r, g, b, alpha){
    }
 
    /**
     * @param xPos double
     * @param yPos double
     * @param wSize double
     * @param hSize double
     * @param r int
     * @param g int
     * @param b int
     * @param alpha int
     * @returns void
     */
    static drawRectangle(xPos, yPos, wSize, hSize, r, g, b, alpha){
    }
 
    /**
     * @param caption string
     * @param xPos double
     * @param yPos double
     * @param scale double
     * @param r int
     * @param g int
     * @param b int
     * @param alpha int
     * @param font int
     * @param justify int
     * @param shadow bool
     * @param outline bool
     * @param wordWrap int
     * @returns void
     */
    static drawText(caption, xPos, yPos, scale, r, g, b, alpha, font, justify, shadow, outline, wordWrap){
    }
 
    /**
     * @param caption string
     * @param x double
     * @param y double
     * @param scale double
     * @param r int
     * @param g int
     * @param b int
     * @param a int
     * @param font int
     * @param alignment int
     * @returns UIResText
     */
    static addTextElement(caption, x, y, scale, r, g, b, a, font, alignment){
    }
 
    /**
     * @returns int
     */
    static getGameTime(){
    }
 
    /**
     * @returns int
     */
    static getGlobalTime(){
    }
 
    /**
     * @param from Vector3
     * @param to Vector3
     * @returns double
     */
    static angleBetween(from, to){
    }
 
    /**
     * @param ent LocalHandle
     * @returns bool
     */
    static isPed(ent){
    }
 
    /**
     * @param ent LocalHandle
     * @returns bool
     */
    static isVehicle(ent){
    }
 
    /**
     * @param ent LocalHandle
     * @returns bool
     */
    static isProp(ent){
    }
 
    /**
     * @param d double
     * @returns float
     */
    static toFloat(d){
    }
 
    /**
     * @param value double
     * @returns ScriptContext.fArg
     */
    static f(value){
    }
 
    /**
     * @param ms int
     * @returns void
     */
    static sleep(ms){
    }
 
    /**
     * @param path string
     * @param looped bool
     * @returns void
     */
    static startAudio(path, looped){
    }
 
    /**
     * @returns void
     */
    static pauseAudio(){
    }
 
    /**
     * @returns void
     */
    static resumeAudio(){
    }
 
    /**
     * @param seconds double
     * @returns void
     */
    static setAudioTime(seconds){
    }
 
    /**
     * @returns double
     */
    static getAudioTime(){
    }
 
    /**
     * @returns bool
     */
    static isAudioPlaying(){
    }
 
    /**
     * @param vol double
     * @returns void
     */
    static setGameVolume(vol){
    }
 
    /**
     * @returns bool
     */
    static isAudioInitialized(){
    }
 
    /**
     * @returns void
     */
    static stopAudio(){
    }
 
    /**
     * @param eventName string
     * @param arguments object[]
     * @returns void
     */
    static triggerServerEvent(eventName, arguments){
    }
 
    /**
     * @param obj object
     * @returns string
     */
    static toString(obj){
    }
 
    /**
     * @param bone int
     * @returns string
     */
    static getBoneName(bone){
    }
 
    /**
     * @param weapon int
     * @returns string
     */
    static getWeaponName(weapon){
    }
 
    /**
     * @param model int
     * @returns string
     */
    static getVehicleModelName(model){
    }
 
    /**
     * @param subtitle string
     * @param x double
     * @param y double
     * @param anchor int
     * @returns UIMenu
     */
    static createMenu(subtitle, x, y, anchor){
    }
 
    /**
     * @param label string
     * @param description string
     * @returns UIMenuItem
     */
    static createMenuItem(label, description){
    }
 
    /**
     * @param label string
     * @param description string
     * @param hexColor string
     * @param hexHighlightColor string
     * @returns UIMenuColoredItem
     */
    static createColoredItem(label, description, hexColor, hexHighlightColor){
    }
 
    /**
     * @param label string
     * @param description string
     * @param isChecked bool
     * @returns UIMenuCheckboxItem
     */
    static createCheckboxItem(label, description, isChecked){
    }
 
    /**
     * @param label string
     * @param description string
     * @param items List<string>
     * @param index int
     * @returns UIMenuListItem
     */
    static createListItem(label, description, items, index){
    }
 
    /**
     * @returns MenuPool
     */
    static getMenuPool(){
    }
 
    /**
     * @param menu UIMenu
     * @returns void
     */
    static drawMenu(menu){
    }
 
    /**
     * @param menu UIMenu
     * @param spritedict string
     * @param spritename string
     * @returns void
     */
    static setMenuBannerSprite(menu, spritedict, spritename){
    }
 
    /**
     * @param menu UIMenu
     * @param path string
     * @returns void
     */
    static setMenuBannerTexture(menu, path){
    }
 
    /**
     * @param menu UIMenu
     * @param alpha int
     * @param red int
     * @param green int
     * @param blue int
     * @returns void
     */
    static setMenuBannerRectangle(menu, alpha, red, green, blue){
    }
 
    /**
     * @param menu UIMenu
     * @param title string
     * @returns void
     */
    static setMenuTitle(menu, title){
    }
 
    /**
     * @param menu UIMenu
     * @param text string
     * @returns void
     */
    static setMenuSubtitle(menu, text){
    }
 
    /**
     * @param defaultText string
     * @param maxlen int
     * @returns string
     */
    static getUserInput(defaultText, maxlen){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isControlJustPressed(control){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isControlPressed(control){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isDisabledControlJustReleased(control){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isDisabledControlJustPressed(control){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isDisabledControlPressed(control){
    }
 
    /**
     * @param control int
     * @returns bool
     */
    static isControlJustReleased(control){
    }
 
    /**
     * @param control int
     * @returns void
     */
    static disableControlThisFrame(control){
    }
 
    /**
     * @param control int
     * @returns void
     */
    static enableControlThisFrame(control){
    }
 
    /**
     * @returns void
     */
    static disableAllControlsThisFrame(){
    }
 
    /**
     * @param control int
     * @returns float
     */
    static getControlNormal(control){
    }
 
    /**
     * @param control int
     * @returns float
     */
    static getDisabledControlNormal(control){
    }
 
    /**
     * @param control int
     * @param value float
     * @returns void
     */
    static setControlNormal(control, value){
    }
 
    /**
     * @returns bool
     */
    static isChatOpen(){
    }
 
    /**
     * @param dict string
     * @returns void
     */
    static loadAnimationDict(dict){
    }
 
    /**
     * @param model int
     * @returns void
     */
    static loadModel(model){
    }
 
    /**
     * @param scaleformName string
     * @returns Scaleform
     */
    static requestScaleform(scaleformName){
    }
 
    /**
     * @param sc Scaleform
     * @param x double
     * @param y double
     * @param w double
     * @param h double
     * @returns void
     */
    static renderScaleform(sc, x, y, w, h){
    }
 
    /**
     * @param entity LocalHandle
     * @param alpha int
     * @returns void
     */
    static setEntityTransparency(entity, alpha){
    }
 
    /**
     * @param entity LocalHandle
     * @returns int
     */
    static getEntityType(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns int
     */
    static getEntityTransparency(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @param dimension int
     * @returns void
     */
    static setEntityDimension(entity, dimension){
    }
 
    /**
     * @param entity LocalHandle
     * @returns int
     */
    static getEntityDimension(entity){
    }
 
    /**
     * @param entity LocalHandle
     * @returns int
     */
    static getEntityModel(entity){
    }
 
    /**
     * @param weapon int
     * @param ammo int
     * @param equipNow bool
     * @param ammoLoaded bool
     * @returns void
     */
    static givePlayerWeapon(weapon, ammo, equipNow, ammoLoaded){
    }
 
    /**
     * @returns void
     */
    static removeAllPlayerWeapons(){
    }
 
    /**
     * @param weapon int
     * @returns bool
     */
    static doesPlayerHaveWeapon(weapon){
    }
 
    /**
     * @param weapon int
     * @returns void
     */
    static removePlayerWeapon(weapon){
    }
 
    /**
     * @param weather string
     * @returns void
     */
    static setWeather(weather){
    }
 
    /**
     * @returns string
     */
    static getWeather(){
    }
 
    /**
     * @returns void
     */
    static resetWeather(){
    }
 
    /**
     * @param hours double
     * @param minutes double
     * @returns void
     */
    static setTime(hours, minutes){
    }
 
    /**
     * @returns TimeSpan
     */
    static getTime(){
    }
 
    /**
     * @returns void
     */
    static resetTime(){
    }
 
 
}
/*
API.Functions = {"startCoroutine":{"returnedType":"void","args":["target"],"argsTypes":["object"]},"showCursor":{"returnedType":"void","args":["show"],"argsTypes":["bool"]},"isCursorShown":{"returnedType":"bool","args":[],"argsTypes":[]},"setSetting":{"returnedType":"void","args":["name","value"],"argsTypes":["string","object"]},"getSetting":{"returnedType":"object","args":["name"],"argsTypes":["string"]},"doesSettingExist":{"returnedType":"bool","args":["name"],"argsTypes":["string"]},"removeSetting":{"returnedType":"void","args":["name"],"argsTypes":["string"]},"registerChatOverride":{"returnedType":"JavascriptChat","args":[],"argsTypes":[]},"createCamera":{"returnedType":"GlobalCamera","args":["position","rotation"],"argsTypes":["Vector3","Vector3"]},"setActiveCamera":{"returnedType":"void","args":["camera"],"argsTypes":["GlobalCamera"]},"setGameplayCameraActive":{"returnedType":"void","args":[],"argsTypes":[]},"getActiveCamera":{"returnedType":"GlobalCamera","args":[],"argsTypes":[]},"setCameraShake":{"returnedType":"void","args":["cam","shakeType","amplitute"],"argsTypes":["GlobalCamera","string","float"]},"stopCameraShake":{"returnedType":"void","args":["cam"],"argsTypes":["GlobalCamera"]},"isCameraShaking":{"returnedType":"bool","args":["cam"],"argsTypes":["GlobalCamera"]},"setCameraPosition":{"returnedType":"void","args":["cam","pos"],"argsTypes":["GlobalCamera","Vector3"]},"getCameraPosition":{"returnedType":"Vector3","args":["cam"],"argsTypes":["GlobalCamera"]},"setCameraRotation":{"returnedType":"void","args":["cam","rotation"],"argsTypes":["GlobalCamera","Vector3"]},"getCameraRotation":{"returnedType":"Vector3","args":["cam"],"argsTypes":["GlobalCamera"]},"setCameraFov":{"returnedType":"void","args":["cam","fov"],"argsTypes":["GlobalCamera","float"]},"getCameraFov":{"returnedType":"float","args":["cam"],"argsTypes":["GlobalCamera"]},"pointCameraAtPosition":{"returnedType":"void","args":["cam","pos"],"argsTypes":["GlobalCamera","Vector3"]},"pointCameraAtEntity":{"returnedType":"void","args":["cam","ent","offset"],"argsTypes":["GlobalCamera","LocalHandle","Vector3"]},"pointCameraAtEntityBone":{"returnedType":"void","args":["cam","ent","bone","offset"],"argsTypes":["GlobalCamera","LocalHandle","int","Vector3"]},"stopCameraPointing":{"returnedType":"void","args":["cam"],"argsTypes":["GlobalCamera"]},"attachCameraToEntity":{"returnedType":"void","args":["cam","ent","offset"],"argsTypes":["GlobalCamera","LocalHandle","Vector3"]},"attachCameraToEntityBone":{"returnedType":"void","args":["cam","ent","bone","offset"],"argsTypes":["GlobalCamera","LocalHandle","int","Vector3"]},"detachCamera":{"returnedType":"void","args":["cam"],"argsTypes":["GlobalCamera"]},"interpolateCameras":{"returnedType":"void","args":["from","to","duration","easepos","easerot"],"argsTypes":["GlobalCamera","GlobalCamera","double","bool","bool"]},"getCursorPosition":{"returnedType":"PointF","args":[],"argsTypes":[]},"getCursorPositionMantainRatio":{"returnedType":"PointF","args":[],"argsTypes":[]},"worldToScreen":{"returnedType":"PointF","args":["pos"],"argsTypes":["Vector3"]},"worldToScreenMantainRatio":{"returnedType":"PointF","args":["pos"],"argsTypes":["Vector3"]},"getCurrentResourceName":{"returnedType":"string","args":[],"argsTypes":[]},"screenToWorld":{"returnedType":"Vector3","args":["pos","camPos","camRot"],"argsTypes":["PointF","Vector3","Vector3"]},"screenToWorldMantainRatio":{"returnedType":"Vector3","args":["pos","camPos","camrot"],"argsTypes":["PointF","Vector3","Vector3"]},"createRaycast":{"returnedType":"ScriptContext.Raycast","args":["start","end","flag","ignoreEntity"],"argsTypes":["Vector3","Vector3","int","LocalHandle?"]},"getGameplayCamPos":{"returnedType":"Vector3","args":[],"argsTypes":[]},"getGameplayCamRot":{"returnedType":"Vector3","args":[],"argsTypes":[]},"getGameplayCamDir":{"returnedType":"Vector3","args":[],"argsTypes":[]},"setCanOpenChat":{"returnedType":"void","args":["show"],"argsTypes":["bool"]},"getCanOpenChat":{"returnedType":"bool","args":[],"argsTypes":[]},"createCefBrowser":{"returnedType":"Browser","args":["width","height","local"],"argsTypes":["double","double","bool"]},"destroyCefBrowser":{"returnedType":"void","args":["browser"],"argsTypes":["Browser"]},"isCefBrowserInitialized":{"returnedType":"bool","args":["browser"],"argsTypes":["Browser"]},"waitUntilCefBrowserInit":{"returnedType":"void","args":["browser"],"argsTypes":["Browser"]},"setCefBrowserSize":{"returnedType":"void","args":["browser","width","height"],"argsTypes":["Browser","double","double"]},"getCefBrowserSize":{"returnedType":"Size","args":["browser"],"argsTypes":["Browser"]},"setCefBrowserHeadless":{"returnedType":"void","args":["browser","headless"],"argsTypes":["Browser","bool"]},"getCefBrowserHeadless":{"returnedType":"bool","args":["browser"],"argsTypes":["Browser"]},"setCefBrowserPosition":{"returnedType":"void","args":["browser","xPos","yPos"],"argsTypes":["Browser","double","double"]},"getCefBrowserPosition":{"returnedType":"Point","args":["browser"],"argsTypes":["Browser"]},"pinCefBrowser":{"returnedType":"void","args":["browser","x1","y1","x2","y2","x3","y3","x4","y4"],"argsTypes":["Browser","double","double","double","double","double","double","double","double"]},"clearCefPinning":{"returnedType":"void","args":["browser"],"argsTypes":["Browser"]},"verifyIntegrityOfCache":{"returnedType":"void","args":[],"argsTypes":[]},"loadPageCefBrowser":{"returnedType":"void","args":["browser","uri"],"argsTypes":["Browser","string"]},"loadHtmlCefBrowser":{"returnedType":"void","args":["browser","html"],"argsTypes":["Browser","string"]},"isCefBrowserLoading":{"returnedType":"bool","args":["browser"],"argsTypes":["Browser"]},"callNative":{"returnedType":"void","args":["hash","args"],"argsTypes":["string","object[]"]},"returnNative":{"returnedType":"object","args":["hash","returnType","args"],"argsTypes":["string","int","object[]"]},"setUiColor":{"returnedType":"void","args":["r","g","b"],"argsTypes":["int","int","int"]},"getHashKey":{"returnedType":"int","args":["input"],"argsTypes":["string"]},"setEntityData":{"returnedType":"bool","args":["entity","key","data"],"argsTypes":["LocalHandle","string","object"]},"resetEntityData":{"returnedType":"void","args":["entity","key"],"argsTypes":["LocalHandle","string"]},"hasEntityData":{"returnedType":"bool","args":["entity","key"],"argsTypes":["LocalHandle","string"]},"getEntityData":{"returnedType":"object","args":["entity","key"],"argsTypes":["LocalHandle","string"]},"setWorldData":{"returnedType":"bool","args":["key","data"],"argsTypes":["string","object"]},"resetWorldData":{"returnedType":"void","args":["key"],"argsTypes":["string"]},"hasWorldData":{"returnedType":"bool","args":["key"],"argsTypes":["string"]},"getWorldData":{"returnedType":"object","args":["key"],"argsTypes":["string"]},"getGamePlayer":{"returnedType":"int","args":[],"argsTypes":[]},"getLocalPlayer":{"returnedType":"LocalHandle","args":[],"argsTypes":[]},"getEntityPosition":{"returnedType":"Vector3","args":["entity"],"argsTypes":["LocalHandle"]},"getEntityRotation":{"returnedType":"Vector3","args":["entity"],"argsTypes":["LocalHandle"]},"getEntityVelocity":{"returnedType":"Vector3","args":["entity"],"argsTypes":["LocalHandle"]},"getVehicleHealth":{"returnedType":"float","args":["entity"],"argsTypes":["LocalHandle"]},"getVehicleRPM":{"returnedType":"float","args":["entity"],"argsTypes":["LocalHandle"]},"isPlayerInAnyVehicle":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerOnFire":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerParachuting":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerInFreefall":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerAiming":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerShooting":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerReloading":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerInCover":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerOnLadder":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"getPlayerAimingPoint":{"returnedType":"Vector3","args":["player"],"argsTypes":["LocalHandle"]},"isPlayerDead":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"doesEntityExist":{"returnedType":"bool","args":["entity"],"argsTypes":["LocalHandle"]},"setEntityInvincible":{"returnedType":"void","args":["entity","invincible"],"argsTypes":["LocalHandle","bool"]},"getEntityInvincible":{"returnedType":"bool","args":["entity"],"argsTypes":["LocalHandle"]},"getLocalPlayerInvincible":{"returnedType":"bool","args":[],"argsTypes":[]},"createParticleEffectOnPosition":{"returnedType":"void","args":["ptfxLibrary","ptfxName","position","rotation","scale"],"argsTypes":["string","string","Vector3","Vector3","float"]},"createParticleEffectOnEntity":{"returnedType":"void","args":["ptfxLibrary","ptfxName","entity","offset","rotation","scale","boneIndex"],"argsTypes":["string","string","LocalHandle","Vector3","Vector3","float","int"]},"createExplosion":{"returnedType":"void","args":["explosionType","position","damageScale"],"argsTypes":["int","Vector3","float"]},"createOwnedExplosion":{"returnedType":"void","args":["owner","explosionType","position","damageScale"],"argsTypes":["LocalHandle","int","Vector3","float"]},"createProjectile":{"returnedType":"void","args":["weapon","start","target","damage","speed","dimension"],"argsTypes":["int","Vector3","Vector3","int","float","int"]},"createOwnedProjectile":{"returnedType":"void","args":["owner","weapon","start","target","damage","speed","dimension"],"argsTypes":["LocalHandle","int","Vector3","Vector3","int","float","int"]},"setVehicleLivery":{"returnedType":"void","args":["vehicle","livery"],"argsTypes":["LocalHandle","int"]},"getVehicleLivery":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleLocked":{"returnedType":"void","args":["vehicle","locked"],"argsTypes":["LocalHandle","bool"]},"getVehicleLocked":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleTrailer":{"returnedType":"LocalHandle","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleTraileredBy":{"returnedType":"LocalHandle","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleSirenState":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"isVehicleTyrePopped":{"returnedType":"bool","args":["vehicle","tyre"],"argsTypes":["LocalHandle","int"]},"popVehicleTyre":{"returnedType":"void","args":["vehicle","tyre","pop"],"argsTypes":["LocalHandle","int","bool"]},"isVehicleDoorBroken":{"returnedType":"bool","args":["vehicle","door"],"argsTypes":["LocalHandle","int"]},"setVehicleDoorState":{"returnedType":"void","args":["vehicle","door","open"],"argsTypes":["LocalHandle","int","bool"]},"getVehicleDoorState":{"returnedType":"bool","args":["vehicle","door"],"argsTypes":["LocalHandle","int"]},"breakVehicleTyre":{"returnedType":"void","args":["vehicle","door","breakDoor"],"argsTypes":["LocalHandle","int","bool"]},"isVehicleWindowBroken":{"returnedType":"bool","args":["vehicle","window"],"argsTypes":["LocalHandle","int"]},"breakVehicleWindow":{"returnedType":"void","args":["vehicle","window","breakWindow"],"argsTypes":["LocalHandle","int","bool"]},"setVehicleExtra":{"returnedType":"void","args":["vehicle","slot","enabled"],"argsTypes":["LocalHandle","int","bool"]},"getVehicleExtra":{"returnedType":"bool","args":["vehicle","slot"],"argsTypes":["LocalHandle","int"]},"setVehicleNumberPlate":{"returnedType":"void","args":["vehicle","plate"],"argsTypes":["LocalHandle","string"]},"getVehicleNumberPlate":{"returnedType":"string","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleEngineStatus":{"returnedType":"void","args":["vehicle","turnedOn"],"argsTypes":["LocalHandle","bool"]},"getEngineStatus":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleSpecialLightStatus":{"returnedType":"void","args":["vehicle","status"],"argsTypes":["LocalHandle","bool"]},"getVehicleSpecialLightStatus":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"setEntityCollissionless":{"returnedType":"void","args":["entity","status"],"argsTypes":["LocalHandle","bool"]},"getEntityCollisionless":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleMod":{"returnedType":"void","args":["vehicle","slot","modType"],"argsTypes":["LocalHandle","int","int"]},"getVehicleMod":{"returnedType":"int","args":["vehicle","slot"],"argsTypes":["LocalHandle","int"]},"removeVehicleMod":{"returnedType":"void","args":["vehicle","slot"],"argsTypes":["LocalHandle","int"]},"setVehicleBulletproofTyres":{"returnedType":"void","args":["vehicle","bulletproof"],"argsTypes":["LocalHandle","bool"]},"getVehicleBulletproofTyres":{"returnedType":"bool","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleNumberPlateStyle":{"returnedType":"void","args":["vehicle","style"],"argsTypes":["LocalHandle","int"]},"getVehicleNumberPlateStyle":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehiclePearlescentColor":{"returnedType":"void","args":["vehicle","color"],"argsTypes":["LocalHandle","int"]},"getVehiclePearlescentColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleWheelColor":{"returnedType":"void","args":["vehicle","color"],"argsTypes":["LocalHandle","int"]},"getVehicleWheelColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleWheelType":{"returnedType":"void","args":["vehicle","type"],"argsTypes":["LocalHandle","int"]},"getVehicleWheelType":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleModColor1":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"setVehicleModColor2":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"setVehicleTyreSmokeColor":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"setVehicleWindowTint":{"returnedType":"void","args":["vehicle","type"],"argsTypes":["LocalHandle","int"]},"getVehicleWindowTint":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleEnginePowerMultiplier":{"returnedType":"void","args":["vehicle","mult"],"argsTypes":["LocalHandle","float"]},"getVehicleEnginePowerMultiplier":{"returnedType":"float","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleEngineTorqueMultiplier":{"returnedType":"void","args":["vehicle","mult"],"argsTypes":["LocalHandle","float"]},"getVehicleEngineTorqueMultiplier":{"returnedType":"float","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleNeonState":{"returnedType":"void","args":["vehicle","slot","turnedOn"],"argsTypes":["LocalHandle","int","bool"]},"getVehicleNeonState":{"returnedType":"bool","args":["vehicle","slot"],"argsTypes":["LocalHandle","int"]},"setVehicleNeonColor":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"setVehicleDashboardColor":{"returnedType":"void","args":["vehicle","type"],"argsTypes":["LocalHandle","int"]},"getVehicleDashboardColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setVehicleTrimColor":{"returnedType":"void","args":["vehicle","type"],"argsTypes":["LocalHandle","int"]},"getVehicleTrimColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleDisplayName":{"returnedType":"string","args":["model"],"argsTypes":["int"]},"getVehicleMaxSpeed":{"returnedType":"float","args":["model"],"argsTypes":["int"]},"getVehicleMaxBraking":{"returnedType":"float","args":["model"],"argsTypes":["int"]},"getVehicleMaxTraction":{"returnedType":"float","args":["model"],"argsTypes":["int"]},"getVehicleMaxAcceleration":{"returnedType":"float","args":["model"],"argsTypes":["int"]},"getVehicleMaxOccupants":{"returnedType":"float","args":["model"],"argsTypes":["int"]},"getVehicleClass":{"returnedType":"int","args":["model"],"argsTypes":["int"]},"detonatePlayerStickies":{"returnedType":"void","args":[],"argsTypes":[]},"setPlayerNametag":{"returnedType":"void","args":["player","text"],"argsTypes":["LocalHandle","string"]},"resetPlayerNametag":{"returnedType":"void","args":["player"],"argsTypes":["LocalHandle"]},"setPlayerNametagVisible":{"returnedType":"void","args":["player","show"],"argsTypes":["LocalHandle","bool"]},"getPlayerNametagVisible":{"returnedType":"bool","args":["player"],"argsTypes":["LocalHandle"]},"setPlayerNametagColor":{"returnedType":"void","args":["player","r","g","b"],"argsTypes":["LocalHandle","byte","byte","byte"]},"resetPlayerNametagColor":{"returnedType":"void","args":["player"],"argsTypes":["LocalHandle"]},"setPlayerSkin":{"returnedType":"void","args":["model"],"argsTypes":["int"]},"setPlayerDefaultClothes":{"returnedType":"void","args":[],"argsTypes":[]},"setPlayerTeam":{"returnedType":"void","args":["team"],"argsTypes":["int"]},"getPlayerTeam":{"returnedType":"int","args":[],"argsTypes":[]},"playPlayerScenario":{"returnedType":"void","args":["name"],"argsTypes":["string"]},"playPlayerAnimation":{"returnedType":"void","args":["animDict","animName","flag","duration"],"argsTypes":["string","string","int","int"]},"stopPlayerAnimation":{"returnedType":"void","args":[],"argsTypes":[]},"setVehiclePrimaryColor":{"returnedType":"void","args":["vehicle","color"],"argsTypes":["LocalHandle","int"]},"setVehicleSecondaryColor":{"returnedType":"void","args":["vehicle","color"],"argsTypes":["LocalHandle","int"]},"setVehicleCustomPrimaryColor":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"setVehicleCustomSecondaryColor":{"returnedType":"void","args":["vehicle","r","g","b"],"argsTypes":["LocalHandle","int","int","int"]},"getVehicleCustomPrimaryColor":{"returnedType":"Color","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleCustomSecondaryColor":{"returnedType":"Color","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehiclePrimaryColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"getVehicleSecondaryColor":{"returnedType":"int","args":["vehicle"],"argsTypes":["LocalHandle"]},"setPlayerClothes":{"returnedType":"void","args":["player","slot","drawable","texture"],"argsTypes":["LocalHandle","int","int","int"]},"setPlayerAccessory":{"returnedType":"void","args":["player","slot","drawable","texture"],"argsTypes":["LocalHandle","int","int","int"]},"clearPlayerAccessory":{"returnedType":"void","args":["player","slot"],"argsTypes":["LocalHandle","int"]},"vehicleNameToModel":{"returnedType":"int","args":["modelName"],"argsTypes":["string"]},"pedNameToModel":{"returnedType":"int","args":["modelName"],"argsTypes":["string"]},"weaponNameToModel":{"returnedType":"int","args":["modelName"],"argsTypes":["string"]},"loadInterior":{"returnedType":"void","args":["pos"],"argsTypes":["Vector3"]},"clearPlayerTasks":{"returnedType":"void","args":[],"argsTypes":[]},"setEntityPositionFrozen":{"returnedType":"void","args":["entity","frozen"],"argsTypes":["LocalHandle","bool"]},"setEntityVelocity":{"returnedType":"void","args":["entity","velocity"],"argsTypes":["LocalHandle","Vector3"]},"getPlayerVehicleSeat":{"returnedType":"int","args":["player"],"argsTypes":["LocalHandle"]},"setPlayerWeaponTint":{"returnedType":"void","args":["weapon","tint"],"argsTypes":["int","int"]},"getPlayerWeaponTint":{"returnedType":"int","args":["weapon"],"argsTypes":["int"]},"givePlayerWeaponComponent":{"returnedType":"void","args":["weapon","component"],"argsTypes":["int","int"]},"removePlayerWeaponComponent":{"returnedType":"void","args":["weapon","component"],"argsTypes":["int","int"]},"hasPlayerWeaponComponent":{"returnedType":"bool","args":["weapon","component"],"argsTypes":["int","int"]},"getAllWeaponComponents":{"returnedType":"WeaponComponent[]","args":["weapon"],"argsTypes":["WeaponHash"]},"getPlayerCurrentWeapon":{"returnedType":"int","args":[],"argsTypes":[]},"disconnect":{"returnedType":"void","args":["reason"],"argsTypes":["string"]},"setEntityPosition":{"returnedType":"void","args":["ent","pos"],"argsTypes":["LocalHandle","Vector3"]},"setEntityRotation":{"returnedType":"void","args":["ent","rot"],"argsTypes":["LocalHandle","Vector3"]},"setPlayerIntoVehicle":{"returnedType":"void","args":["vehicle","seat"],"argsTypes":["LocalHandle","int"]},"setPlayerHealth":{"returnedType":"void","args":["health"],"argsTypes":["int"]},"getPlayerHealth":{"returnedType":"int","args":["player"],"argsTypes":["LocalHandle"]},"setTextLabelText":{"returnedType":"void","args":["label","text"],"argsTypes":["LocalHandle","string"]},"setTextLabelColor":{"returnedType":"void","args":["textLabel","alpha","r","g","b"],"argsTypes":["LocalHandle","int","int","int","int"]},"getTextLabelColor":{"returnedType":"Color","args":["textLabel"],"argsTypes":["LocalHandle"]},"setTextLabelSeethrough":{"returnedType":"void","args":["handle","seethrough"],"argsTypes":["LocalHandle","bool"]},"getTextLabelSeethrough":{"returnedType":"bool","args":["handle"],"argsTypes":["LocalHandle"]},"getOffsetInWorldCoords":{"returnedType":"Vector3","args":["entity","offset"],"argsTypes":["LocalHandle","Vector3"]},"getOffsetFromWorldCoords":{"returnedType":"Vector3","args":["entity","pos"],"argsTypes":["LocalHandle","Vector3"]},"drawLine":{"returnedType":"void","args":["start","end","a","r","g","b"],"argsTypes":["Vector3","Vector3","int","int","int","int"]},"playSoundFrontEnd":{"returnedType":"void","args":["audioLib","audioName"],"argsTypes":["string","string"]},"showShard":{"returnedType":"void","args":["text","timeout"],"argsTypes":["string","int"]},"loadConfig":{"returnedType":"XmlGroup","args":["config"],"argsTypes":["string"]},"fromJson":{"returnedType":"object","args":["json"],"argsTypes":["string"]},"toJson":{"returnedType":"string","args":["data"],"argsTypes":["object"]},"getScreenResolutionMantainRatio":{"returnedType":"SizeF","args":[],"argsTypes":[]},"sendChatMessage":{"returnedType":"void","args":["text"],"argsTypes":["string"]},"getScreenResolution":{"returnedType":"Size","args":[],"argsTypes":[]},"sendNotification":{"returnedType":"void","args":["text"],"argsTypes":["string"]},"displaySubtitle":{"returnedType":"void","args":["text","duration"],"argsTypes":["string","double"]},"formatTime":{"returnedType":"string","args":["ms","format"],"argsTypes":["double","string"]},"setPlayerInvincible":{"returnedType":"void","args":["invinc"],"argsTypes":["bool"]},"setPlayerWantedLevel":{"returnedType":"void","args":["wantedLevel"],"argsTypes":["int"]},"getPlayerWantedLevel":{"returnedType":"int","args":[],"argsTypes":[]},"getPlayerInvincible":{"returnedType":"bool","args":[],"argsTypes":[]},"setPlayerArmor":{"returnedType":"void","args":["armor"],"argsTypes":["int"]},"getPlayerArmor":{"returnedType":"int","args":["player"],"argsTypes":["LocalHandle"]},"getStreamedPlayers":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedVehicles":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedObjects":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedPickups":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedPeds":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedMarkers":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getStreamedTextLabels":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllPlayers":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllVehicles":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllObjects":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllPickups":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllPeds":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllMarkers":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getAllTextLabels":{"returnedType":"LocalHandle[]","args":[],"argsTypes":[]},"getPlayerVehicle":{"returnedType":"LocalHandle","args":["player"],"argsTypes":["LocalHandle"]},"explodeVehicle":{"returnedType":"void","args":["vehicle"],"argsTypes":["LocalHandle"]},"getPlayerByName":{"returnedType":"LocalHandle","args":["name"],"argsTypes":["string"]},"getPlayerName":{"returnedType":"string","args":["player"],"argsTypes":["LocalHandle"]},"forceSendAimData":{"returnedType":"void","args":["force"],"argsTypes":["bool"]},"isAimDataForced":{"returnedType":"bool","args":[],"argsTypes":[]},"getPlayerAimCoords":{"returnedType":"Vector3","args":["player"],"argsTypes":["LocalHandle"]},"getPlayerPing":{"returnedType":"int","args":["player"],"argsTypes":["LocalHandle"]},"createVehicle":{"returnedType":"LocalHandle","args":["model","pos","heading"],"argsTypes":["int","Vector3","float"]},"createPed":{"returnedType":"LocalHandle","args":["model","pos","heading"],"argsTypes":["int","Vector3","float"]},"createBlip":{"returnedType":"LocalHandle","args":["pos"],"argsTypes":["Vector3"]},"setBlipPosition":{"returnedType":"void","args":["blip","pos"],"argsTypes":["LocalHandle","Vector3"]},"getBlipPosition":{"returnedType":"Vector3","args":["blip"],"argsTypes":["LocalHandle"]},"getWaypointPosition":{"returnedType":"Vector3","args":[],"argsTypes":[]},"isWaypointSet":{"returnedType":"bool","args":[],"argsTypes":[]},"setWaypoint":{"returnedType":"void","args":["x","y"],"argsTypes":["double","double"]},"removeWaypoint":{"returnedType":"void","args":[],"argsTypes":[]},"setBlipColor":{"returnedType":"void","args":["blip","color"],"argsTypes":["LocalHandle","int"]},"getBlipColor":{"returnedType":"int","args":["blip"],"argsTypes":["LocalHandle"]},"setBlipSprite":{"returnedType":"void","args":["blip","sprite"],"argsTypes":["LocalHandle","int"]},"getBlipSprite":{"returnedType":"int","args":["blip"],"argsTypes":["LocalHandle"]},"setBlipName":{"returnedType":"void","args":["blip","name"],"argsTypes":["LocalHandle","string"]},"getBlipName":{"returnedType":"string","args":["blip"],"argsTypes":["LocalHandle"]},"setBlipTransparency":{"returnedType":"void","args":["blip","alpha"],"argsTypes":["LocalHandle","int"]},"getBlipTransparency":{"returnedType":"int","args":["blip"],"argsTypes":["LocalHandle"]},"setBlipShortRange":{"returnedType":"void","args":["blip","shortRange"],"argsTypes":["LocalHandle","bool"]},"getBlipShortRange":{"returnedType":"bool","args":["blip"],"argsTypes":["LocalHandle"]},"showBlipRoute":{"returnedType":"void","args":["blip","show"],"argsTypes":["LocalHandle","bool"]},"setBlipScale":{"returnedType":"void","args":["blip","scale"],"argsTypes":["LocalHandle","float"]},"getBlipScale":{"returnedType":"float","args":["blip"],"argsTypes":["LocalHandle"]},"setChatVisible":{"returnedType":"void","args":["display"],"argsTypes":["bool"]},"getChatVisible":{"returnedType":"bool","args":[],"argsTypes":[]},"getAveragePacketSize":{"returnedType":"float","args":[],"argsTypes":[]},"getBytesSentPerSecond":{"returnedType":"float","args":[],"argsTypes":[]},"getBytesReceivedPerSecond":{"returnedType":"float","args":[],"argsTypes":[]},"requestControlOfPlayer":{"returnedType":"void","args":["player"],"argsTypes":["LocalHandle"]},"stopControlOfPlayer":{"returnedType":"void","args":["player"],"argsTypes":["LocalHandle"]},"setHudVisible":{"returnedType":"void","args":["visible"],"argsTypes":["bool"]},"isSpectating":{"returnedType":"bool","args":[],"argsTypes":[]},"getHudVisible":{"returnedType":"bool","args":[],"argsTypes":[]},"createMarker":{"returnedType":"LocalHandle","args":["markerType","pos","dir","rot","scale","r","g","b","alpha"],"argsTypes":["int","Vector3","Vector3","Vector3","Vector3","int","int","int","int"]},"setMarkerType":{"returnedType":"void","args":["marker","type"],"argsTypes":["LocalHandle","int"]},"getMarkerType":{"returnedType":"int","args":["marker"],"argsTypes":["LocalHandle"]},"setMarkerColor":{"returnedType":"void","args":["marker","alpha","r","g","b"],"argsTypes":["LocalHandle","int","int","int","int"]},"getMarkerColor":{"returnedType":"Color","args":["marker"],"argsTypes":["LocalHandle"]},"setMarkerScale":{"returnedType":"void","args":["marker","scale"],"argsTypes":["LocalHandle","Vector3"]},"getMarkerScale":{"returnedType":"Vector3","args":["marker"],"argsTypes":["LocalHandle"]},"setMarkerDirection":{"returnedType":"void","args":["marker","dir"],"argsTypes":["LocalHandle","Vector3"]},"getMarkerDirection":{"returnedType":"Vector3","args":["marker"],"argsTypes":["LocalHandle"]},"deleteEntity":{"returnedType":"void","args":["handle"],"argsTypes":["LocalHandle"]},"attachEntity":{"returnedType":"void","args":["ent1","ent2","bone","positionOffset","rotationOffset"],"argsTypes":["LocalHandle","LocalHandle","string","Vector3","Vector3"]},"detachEntity":{"returnedType":"void","args":["ent"],"argsTypes":["LocalHandle"]},"isEntityAttachedToAnything":{"returnedType":"bool","args":["ent"],"argsTypes":["LocalHandle"]},"isEntityAttachedToEntity":{"returnedType":"bool","args":["from","to"],"argsTypes":["LocalHandle","LocalHandle"]},"createTextLabel":{"returnedType":"LocalHandle","args":["text","pos","range","size"],"argsTypes":["string","Vector3","float","float"]},"lerpVector":{"returnedType":"Vector3","args":["start","end","currentTime","duration"],"argsTypes":["Vector3","Vector3","double","double"]},"lerpFloat":{"returnedType":"double","args":["start","end","currentTime","duration"],"argsTypes":["double","double","double","double"]},"isInRangeOf":{"returnedType":"bool","args":["entity","destination","range"],"argsTypes":["Vector3","Vector3","double"]},"dxDrawTexture":{"returnedType":"void","args":["path","pos","size","id"],"argsTypes":["string","Point","Size","int"]},"drawGameTexture":{"returnedType":"void","args":["dict","txtName","x","y","width","height","heading","r","g","b","alpha"],"argsTypes":["string","string","double","double","double","double","double","int","int","int","int"]},"drawRectangle":{"returnedType":"void","args":["xPos","yPos","wSize","hSize","r","g","b","alpha"],"argsTypes":["double","double","double","double","int","int","int","int"]},"drawText":{"returnedType":"void","args":["caption","xPos","yPos","scale","r","g","b","alpha","font","justify","shadow","outline","wordWrap"],"argsTypes":["string","double","double","double","int","int","int","int","int","int","bool","bool","int"]},"addTextElement":{"returnedType":"UIResText","args":["caption","x","y","scale","r","g","b","a","font","alignment"],"argsTypes":["string","double","double","double","int","int","int","int","int","int"]},"getGameTime":{"returnedType":"int","args":[],"argsTypes":[]},"getGlobalTime":{"returnedType":"int","args":[],"argsTypes":[]},"angleBetween":{"returnedType":"double","args":["from","to"],"argsTypes":["Vector3","Vector3"]},"isPed":{"returnedType":"bool","args":["ent"],"argsTypes":["LocalHandle"]},"isVehicle":{"returnedType":"bool","args":["ent"],"argsTypes":["LocalHandle"]},"isProp":{"returnedType":"bool","args":["ent"],"argsTypes":["LocalHandle"]},"toFloat":{"returnedType":"float","args":["d"],"argsTypes":["double"]},"f":{"returnedType":"ScriptContext.fArg","args":["value"],"argsTypes":["double"]},"sleep":{"returnedType":"void","args":["ms"],"argsTypes":["int"]},"startAudio":{"returnedType":"void","args":["path","looped"],"argsTypes":["string","bool"]},"pauseAudio":{"returnedType":"void","args":[],"argsTypes":[]},"resumeAudio":{"returnedType":"void","args":[],"argsTypes":[]},"setAudioTime":{"returnedType":"void","args":["seconds"],"argsTypes":["double"]},"getAudioTime":{"returnedType":"double","args":[],"argsTypes":[]},"isAudioPlaying":{"returnedType":"bool","args":[],"argsTypes":[]},"setGameVolume":{"returnedType":"void","args":["vol"],"argsTypes":["double"]},"isAudioInitialized":{"returnedType":"bool","args":[],"argsTypes":[]},"stopAudio":{"returnedType":"void","args":[],"argsTypes":[]},"triggerServerEvent":{"returnedType":"void","args":["eventName","arguments"],"argsTypes":["string","object[]"]},"toString":{"returnedType":"string","args":["obj"],"argsTypes":["object"]},"getBoneName":{"returnedType":"string","args":["bone"],"argsTypes":["int"]},"getWeaponName":{"returnedType":"string","args":["weapon"],"argsTypes":["int"]},"getVehicleModelName":{"returnedType":"string","args":["model"],"argsTypes":["int"]},"createMenu":{"returnedType":"UIMenu","args":["subtitle","x","y","anchor"],"argsTypes":["string","double","double","int"]},"createMenuItem":{"returnedType":"UIMenuItem","args":["label","description"],"argsTypes":["string","string"]},"createColoredItem":{"returnedType":"UIMenuColoredItem","args":["label","description","hexColor","hexHighlightColor"],"argsTypes":["string","string","string","string"]},"createCheckboxItem":{"returnedType":"UIMenuCheckboxItem","args":["label","description","isChecked"],"argsTypes":["string","string","bool"]},"createListItem":{"returnedType":"UIMenuListItem","args":["label","description","items","index"],"argsTypes":["string","string","List<string>","int"]},"getMenuPool":{"returnedType":"MenuPool","args":[],"argsTypes":[]},"drawMenu":{"returnedType":"void","args":["menu"],"argsTypes":["UIMenu"]},"setMenuBannerSprite":{"returnedType":"void","args":["menu","spritedict","spritename"],"argsTypes":["UIMenu","string","string"]},"setMenuBannerTexture":{"returnedType":"void","args":["menu","path"],"argsTypes":["UIMenu","string"]},"setMenuBannerRectangle":{"returnedType":"void","args":["menu","alpha","red","green","blue"],"argsTypes":["UIMenu","int","int","int","int"]},"setMenuTitle":{"returnedType":"void","args":["menu","title"],"argsTypes":["UIMenu","string"]},"setMenuSubtitle":{"returnedType":"void","args":["menu","text"],"argsTypes":["UIMenu","string"]},"getUserInput":{"returnedType":"string","args":["defaultText","maxlen"],"argsTypes":["string","int"]},"isControlJustPressed":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"isControlPressed":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"isDisabledControlJustReleased":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"isDisabledControlJustPressed":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"isDisabledControlPressed":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"isControlJustReleased":{"returnedType":"bool","args":["control"],"argsTypes":["int"]},"disableControlThisFrame":{"returnedType":"void","args":["control"],"argsTypes":["int"]},"enableControlThisFrame":{"returnedType":"void","args":["control"],"argsTypes":["int"]},"disableAllControlsThisFrame":{"returnedType":"void","args":[],"argsTypes":[]},"getControlNormal":{"returnedType":"float","args":["control"],"argsTypes":["int"]},"getDisabledControlNormal":{"returnedType":"float","args":["control"],"argsTypes":["int"]},"setControlNormal":{"returnedType":"void","args":["control","value"],"argsTypes":["int","float"]},"isChatOpen":{"returnedType":"bool","args":[],"argsTypes":[]},"loadAnimationDict":{"returnedType":"void","args":["dict"],"argsTypes":["string"]},"loadModel":{"returnedType":"void","args":["model"],"argsTypes":["int"]},"requestScaleform":{"returnedType":"Scaleform","args":["scaleformName"],"argsTypes":["string"]},"renderScaleform":{"returnedType":"void","args":["sc","x","y","w","h"],"argsTypes":["Scaleform","double","double","double","double"]},"setEntityTransparency":{"returnedType":"void","args":["entity","alpha"],"argsTypes":["LocalHandle","int"]},"getEntityType":{"returnedType":"int","args":["entity"],"argsTypes":["LocalHandle"]},"getEntityTransparency":{"returnedType":"int","args":["entity"],"argsTypes":["LocalHandle"]},"setEntityDimension":{"returnedType":"void","args":["entity","dimension"],"argsTypes":["LocalHandle","int"]},"getEntityDimension":{"returnedType":"int","args":["entity"],"argsTypes":["LocalHandle"]},"getEntityModel":{"returnedType":"int","args":["entity"],"argsTypes":["LocalHandle"]},"givePlayerWeapon":{"returnedType":"void","args":["weapon","ammo","equipNow","ammoLoaded"],"argsTypes":["int","int","bool","bool"]},"removeAllPlayerWeapons":{"returnedType":"void","args":[],"argsTypes":[]},"doesPlayerHaveWeapon":{"returnedType":"bool","args":["weapon"],"argsTypes":["int"]},"removePlayerWeapon":{"returnedType":"void","args":["weapon"],"argsTypes":["int"]},"setWeather":{"returnedType":"void","args":["weather"],"argsTypes":["string"]},"getWeather":{"returnedType":"string","args":[],"argsTypes":[]},"resetWeather":{"returnedType":"void","args":[],"argsTypes":[]},"setTime":{"returnedType":"void","args":["hours","minutes"],"argsTypes":["double","double"]},"getTime":{"returnedType":"TimeSpan","args":[],"argsTypes":[]},"resetTime":{"returnedType":"void","args":[],"argsTypes":[]}}
 
API.Events = {}
 
API.EventTypes = {"ServerEventTrigger":{"returnedType":"delegate","args":["eventName","arguments"],"argsTypes":["string","object[]"]},"ChatEvent":{"returnedType":"delegate","args":["msg"],"argsTypes":["string"]},"StreamEvent":{"returnedType":"delegate","args":["item","entityType"],"argsTypes":["LocalHandle","int"]},"DataChangedEvent":{"returnedType":"delegate","args":["entity","key","oldValue"],"argsTypes":["LocalHandle","string","object"]},"CustomDataReceived":{"returnedType":"delegate","args":["data"],"argsTypes":["string"]},"EmptyEvent":{"returnedType":"delegate","args":[],"argsTypes":[]},"EntityEvent":{"returnedType":"delegate","args":["entity"],"argsTypes":["LocalHandle"]},"PlayerKilledEvent":{"returnedType":"delegate","args":["killer","weapon"],"argsTypes":["LocalHandle","int"]},"IntChangeEvent":{"returnedType":"delegate","args":["oldValue"],"argsTypes":["int"]},"BoolChangeEvent":{"returnedType":"delegate","args":["oldValue"],"argsTypes":["bool"]},"PlayerDamageEvent":{"returnedType":"delegate","args":["attacker","weaponUsed","boneHit"],"argsTypes":["LocalHandle","int","int"]},"PlayerMeleeDamageEvent":{"returnedType":"delegate","args":["attacker","weaponUsed"],"argsTypes":["LocalHandle","int"]},"WeaponShootEvent":{"returnedType":"delegate","args":["weaponUsed","aimCoords"],"argsTypes":["int","Vector3"]}}
*/
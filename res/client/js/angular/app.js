﻿var oversight = angular.module('oversight', ['ngAnimate', 'ngSanitize']);

angular.element(document).ready(function () {
    var appElement = document.querySelector('[ng-app=oversight]');
    var appScope = angular.element(appElement).scope();

    var inventoryElement = document.querySelector("[ng-controller='inventoryController']");
    var inventoryScope = angular.element(inventoryElement).scope();

    setVar = function (name, value) {
        console.log(name, value);
        appScope.$apply(function() {
            appScope[name] = value;
        });
    };

    onLogInResult = function (result) {
        if (!result)
            appScope.$apply(function () {
                appScope.logInResult = false;
        });
    };

    registerResult = function (result, errorMessage) {
        appScope.$apply(function () {
            if (result == false)
                appScope.regData.errorMessage = errorMessage;
        });
    };

    showMessage = function (msg) {
        appScope.$apply(function () {
            appScope.showMessage(msg);
        });
    };

    changeChatType = function (type) {
        appScope.$apply(function () {
            appScope.changeChatType(type);
        });
    };

    changeChatChannel = function (channel) {
        appScope.$apply(function () {
            appScope.changeChatChannel(channel);
        });
    };

    loadInventoryItems = function (items) {
        appScope.$apply(function () {
            inventoryScope.setInventoryItems(items);
        });
    };

    toggleInventory = function () {
        appScope.$apply(function () {
            inventoryScope.toggleInventory();
        });
    };

    addObjectToNearbyStoringObjects = function (entity) {
        appScope.$apply(function () {
            // inventoryScope.
        })
    }
});

oversight.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            // to address @blesh's comment, set attribute value to 'false'
            // on blur event:
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
}]);

oversight.directive('shortCut', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            console.log(attrs.shortCut);
            scope.$apply(function () {
                scope.$eval(attrs.shortCut);
            });

            event.preventDefault();
        });
    };
});

oversight.filter("trust", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);
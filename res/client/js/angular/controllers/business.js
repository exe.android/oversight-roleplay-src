oversight.controller('businessController', function ($scope) {
    $scope.lumberJackSalary = 30;

    $scope.hireToWork = function (work)
    {
        resourceCall("playerHiringToWork", work);
    };

    $scope.closeWindow = function ()
    {
        resourceCall("closeWindow");
    }
});
﻿oversight.controller('loginController', function ($scope) {
    $scope.formData = {
        login: 'Insight',
        password: 123123
    };

    $scope.regData = {
        login: null,
        password: null,
        repeatedPassword: null,
        email: null,
        name: null,
        surname: null,
        errorMessage: null
    };

    $scope.step = 'login';

    $scope.logInResult = {};

    $scope.processLogIn = function ()
    {
        resourceCall("CefHelper.browserCallback", "processLogIn", $scope.formData.login, $scope.formData.password);
    };

    $scope.processRegistration = function ()
    {
        $scope.regData.errorMessage = null;

        if ($scope.regData.password != $scope.regData.repeatedPassword)
        {
            $scope.regData.errorMessage = "Введенные пароли не совпадают";
            return;
        }

        resourceCall("processRegistration",
            $scope.regData.login,
            $scope.regData.password,
            $scope.regData.email,
            $scope.regData.name,
            $scope.regData.surname);
    }
});
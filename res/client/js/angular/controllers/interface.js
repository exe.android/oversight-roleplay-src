﻿oversight.controller('interfaceController', function ($scope, $window, $rootScope) {
    $scope.currentInterface = 'player';

    $scope.username = null;
    $scope.hunger = 30;
    $scope.thirst = 30;

    // STATES: off, ignition, after_ignition, starting, on
    $scope.currentVehicleEngineState = "off";
    $scope.currentVehicleModelName = "Kuruma";
    $scope.currentVehicleModelClass = "Sports";
    $scope.currentVehicleSpeed = 0;
    $scope.currentVehicleMaxSpeed = 0;
    $scope.currentVehicleRPM = 0;
    $scope.currentVehicleFuelPercent = 8;
    $scope.currentVehicleMileage = 0;

    $scope.getMileage = function (number) {
        var mileage = $scope.currentVehicleMileage / 1000;
        mileage = mileage.toFixed(1);
        var mileageString = mileage.toString();
        while (mileageString.length < 8)
            mileageString = "0" + mileageString;
        mileageString = mileageString.replace(".", ",");
        return mileageString.substr(number, 1);
    };

    $scope.getCurrentVehicleSpeedPercent = function () {
        var speed = ($scope.currentVehicleSpeed / $scope.currentVehicleMaxSpeed * 100);
        return (speed <= 100) ? speed : 100;
    };

    // CUSTOMIZE

    $scope.customizeStep = 1;

    $scope.currentCustomizeType = null;
    $scope.customizeTypes = [
        {
            name: 'Лицо 1',
            image: 'face.png',
            data: 'GTAO_SHAPE_FIRST_ID'
        },
        {
            name: 'Лицо 2',
            image: 'face.png',
            data: 'GTAO_SHAPE_SECOND_ID'
        },
        {
            name: 'Цвет кожи',
            image: 'skin-color.png',
            data: 'GTAO_SKIN_FIRST_ID'
        },
        {
            name: 'Волосы',
            image: 'hairs.png',
            data: 'GTAO_HAIR'
        },
        {
            name: 'Цвет волос',
            image: 'palette.png',
            data: 'GTAO_HAIR_COLOR'
        },
        {
            name: 'Оттенок волос',
            image: 'grade.png',
            data: 'GTAO_HAIR_HIGHLIGHT_COLOR'
        },
        {
            name: 'Цвет глаз',
            image: 'eyes.png',
            data: 'GTAO_EYE_COLOR'
        }
    ];

    $scope.faceShapesNames = ['Размер носа', 'Высота носа', 'Длина носа', 'Углубление носа', 'Кончик носа', 'Кривизна носа', 'Высота бровей', 'Длина бровей',
        'Мешочные впадины', 'Щеки', 'Скулы', 'Разрез глаз', 'Размер губ', 'Челюсти середина', 'Челюсти зад', 'Размер подбородка', 'Длина подбородка', 'Ширина подбородка', 'Углубление подбородка', 'Размер шеи'];

    $scope.faceShapes = [];

    $scope.faceShapesNames.map(function (shape) {
        $scope.faceShapes.push({name: shape, value: 0});
    });

    $scope.pedComponents = [
        {
            name: 'Одежда (низ)',
            data: 'GTAO_PED_COMPONENT_TSHIRT',
            image: 'tshirt.png'
        },
        {
            name: 'Цвет одежды (низ)',
            data: 'GTAO_PED_COMPONENT_TSHIRT_VARIATION',
            image: 'color.png'
        },
        {
            name: 'Одежда (верх)',
            data: 'GTAO_PED_COMPONENT_CLOTHES',
            image: 'jacket.png'
        },
        {
            name: 'Цвет одежды',
            data: 'GTAO_PED_COMPONENT_CLOTHES_VARIATION',
            image: 'color.png'
        },
        {
            name: 'Торс',
            data: 'GTAO_PED_COMPONENT_TORSO',
            image: 'torso.png'
        },
        {
            name: 'Штаны',
            data: 'GTAO_PED_COMPONENT_LEG',
            image: 'jeans.png'
        },
        {
            name: 'Цвет штанов',
            data: 'GTAO_PED_COMPONENT_LEG_VARIATION',
            image: 'color.png'
        },
        // {
        //     name: 'Обувь',
        //     data: 'GTAO_PED_COMPONENT_FOOT',
        //     image: ''
        // },
        // {
        //     name: 'Вариация обуви',
        //     data: 'GTAO_PED_COMPONENT_FOOT_VARIATION',
        //     image: ''
        // }
    ];

    $scope.getShapeValue = function (index) {
        return $scope.faceShapes[index];
    };

    $scope.faceShapesChanged = function (index, value) {
        resourceCall("faceShapesChanged", index, value);
    };

    $scope.setCurrentCustomizeType = function (type) {
        $scope.currentCustomizeType = type;
        resourceCall("setCurrentFaceCustomizeSlot", type);
    };

    $scope.changeStep = function (step) {
        $scope.customizeStep = step;
        resourceCall("pedCustomizer.changeStep", step);
    };

    $scope.savePedCustomize = function () {
        resourceCall("pedCustomizer.savePed", 1);
    };

    $scope.ctrlDown = false;
    $scope.altDown = false;

    $scope.ctrlKey = 17;
    $scope.altKey = 18;

    angular.element($window).bind("keyup", function($event) {
        if ($event.keyCode == $scope.ctrlKey)
            $scope.ctrlDown = false;
        if ($event.keyCode == $scope.altKey)
            $scope.altDown = false;
        $scope.$apply();
    });

    angular.element($window).bind("keydown", function($event) {
        if ($event.keyCode == $scope.ctrlKey)
            $scope.ctrlDown = true;
        if ($event.keyCode == $scope.altKey)
            $scope.altDown = true;

        var secondKey = $event.keyCode - 48;
        if ($event.keyCode != $scope.ctrlKey && $scope.ctrlDown) {
            resourceCall("Chat.changeChatType", secondKey);
        }

        if ($event.keyCode != $scope.ctrlKey && $scope.altDown) {
            resourceCall("Chat.changeChatChannel", secondKey);
        }
        $scope.$apply();
    });
});
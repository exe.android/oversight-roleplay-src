oversight.controller('rentalHouseController', function ($scope) {
    $scope.houseName = "";
    $scope.playersLimit = 1;
    $scope.playersRentingCount = 0;
    $scope.rentPrice = 0;
    $scope.houseId = 0;
    $scope.comfortLevel = 0;

    $scope.currentPlayerRentingThisHouse = 0;

    $scope.getNumber = function (arr)
    {
        return new Array(arr);
    };

    $scope.closeWindow = function ()
    {
        resourceCall("CefHelper.browserCallback", "closeWindow");
    };

    $scope.rentHouse = function ()
    {
        resourceCall("CefHelper.browserCallback", "rentHouse");
    }
});
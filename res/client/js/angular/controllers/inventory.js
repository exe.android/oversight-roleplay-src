/**
 * Created by Insight on 28.02.2017.
 */
oversight.controller('inventoryController', function ($scope, $window, $rootScope) {
    const PAGE_GRID_ITEMS = 32;

    $rootScope.inventoryOpened = false;

    $scope.attributesIcons = {
        GiveEnergy: "energy",
        RemoveThirst: "water"
    };

    $scope.inventoryItems = [];

    $scope.setInventoryItems = function (items) {
        items = JSON.parse(items);
        $scope.inventoryItems = items;

        // $scope.inventoryItems.push({
        //     Id: 1,
        //     Amount: 33000,
        //     Template: {
        //         Icon: "dollar",
        //         DisplayName: "Доллар",
        //         Description: "Грязные зеленые бумажки",
        //         Weight: 0.01,
        //         SelfUsable: false,
        //         Wasteable: true,
        //         Exchangable: true,
        //         Stackable: true
        //     }
        // });
        //
        // for (i = 0; i < 20; i ++)
        // $scope.inventoryItems.push({
        //     Id: i,
        //     Template: {
        //         Icon: "pepsi_can",
        //         DisplayName: "Pepsi, 0.3 л.",
        //         Description: "Бодрящий газированный напиток. Отлично уталяет жажду и восполняет энергию",
        //         Weight: 0.2,
        //         SelfUsable: true,
        //         Wasteable: true,
        //         Exchangable: true,
        //         Interactive: true,
        //         Attributes: [
        //             {
        //                 Name: "GiveEnergy",
        //                 Value: 30,
        //                 Description: "Дает энергии"
        //             },
        //             {
        //                 Name: "RemoveThirst",
        //                 Value: 15,
        //                 Description: "Утоляет жажду"
        //             }
        //         ]
        //     }
        // });

        for (i = $scope.inventoryItems.length; i < PAGE_GRID_ITEMS; i++)
        {
            $scope.inventoryItems.push({
                dummy: true
            });
        }
    };

    $scope.toggleInventory = function () {
        $rootScope.inventoryOpened = !$rootScope.inventoryOpened;
        resourceCall("LocalPlayer.changeCursorState", $rootScope.inventoryOpened);
    };

    $scope.getInventoryItems = function () {
        return $scope.inventoryItems;
    };

    $scope.getAttributeIcon = function (attribute) {
        for (name in $scope.attributesIcons)
        {
            if (name == attribute.Name)
                return $scope.attributesIcons[name];
        }
    };

    $scope.getItemAttributes = function (item) {
        if (typeof (item.Template) != "undefined" && typeof (item.Template.Attributes) != "undefined")
        {
            var attributes =  item.Template.Attributes.map(function (attribute) {
                var icon = $scope.getAttributeIcon(attribute);
                attribute.Icon = icon;
                return attribute;
            });
            return attributes;
        }
        else
            return [];
    };

    $scope.getActiveItem = function () {
        var item = $scope.inventoryItems.filter(function (item) {
            return item.active;
        })[0];

        if (typeof (item) != "undefined")
        {
            return item;
        }
        else
        {
            return null;
        }
    };

    $scope.setItemActive = function (item) {
        $scope.inventoryItems = $scope.inventoryItems.map(function (inventoryItem) {
            if (inventoryItem.dummy)
                return inventoryItem;

            if (inventoryItem.active && inventoryItem.Id != item.Id)
            {
                inventoryItem.active = false;
            }

            if (inventoryItem.hovered && item.Id != inventoryItem.Id)
            {
                inventoryItem.hovered = false;
            }

            if (item.Id == inventoryItem.Id)
            {
                if (item.Template.Interactive) {
                    inventoryItem.active = !inventoryItem.active;
                }
            }
            return inventoryItem;
        });
    };

    $scope.useItemSelf = function (item) {
        resourceCall("Inventory.useItemSelf", item.Id);
    };

    $scope.addObjectToNearbyStoringObjects = function (entity) {

    };

    $scope.setInventoryItems("[]");
});

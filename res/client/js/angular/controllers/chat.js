oversight.controller('chatController', function ($scope, $location, $anchorScroll, $window, $sce) {
    $scope.chatMessage = "";
    $scope.currentChatType = 1;
    $scope.currentChatChannel = 1;

    $scope.chatIsVisible = false;
    $scope.chatMessages = [];

    $scope.chatTypes = [
        {
            type: 1,
            name: "Обычный",
            shortcut: "ctrl + 1",
            code: "common"
        },
        {
            type: 2,
            name: "/b",
            shortcut: "ctrl + 2",
            code: "ooc"
        },
        {
            type: 3,
            name: "/me",
            shortcut: "ctrl + 3",
            code: "me"
        },
        {
            type: 4,
            name: "/do",
            shortcut: "ctrl + 4",
            code: "doing"
        },
        {
            type: 5,
            name: "/s",
            shortcut: "ctrl + 5",
            code: "shout"
        }
    ];

    $scope.chatChannels = [
        {
            type: 1,
            name: "Улица",
            shortcut: "alt + 1"
        },
        {
            type: 2,
            name: "Рация",
            shortcut: "alt + 2"
        },
        {
            type: 3,
            name: "Телефон",
            shortcut: "alt + 3"
        }
    ];

    // К каким каналам игрок имеет доступ
    $scope.playerChatChannelsAccess = [0,1,2];

    // Возвращает список активных каналов чата пользователя, основываясь на его правах доступа к каналу, а так же активности
    // игровых элементов (к примеру, активный разговор по телефону)
    $scope.getPlayerActiveChatChannels = function () {
        var activeChannels = $scope.chatChannels.filter(function (channel) {
            return $scope.playerChatChannelsAccess.indexOf(channel.type) != -1;
        });
        return activeChannels;
    };

    $scope.getPlayerActiveChatTypes = function () {
        var activeTypes = $scope.chatTypes.filter(function (type) {
            // Сделать проверку на текущий канал и прочие ограничения
            return true;
        });
        return activeTypes;
    };

    $scope.sendMessage = function () {
        resourceCall("Chat.sendMessage", $scope.chatMessage);
        $scope.chatMessage = null;
    };

    $scope.chatMessageModel = function () {
        this.message = null;
    };

    $scope.showMessage = function (message) {
        var chatMessage = new $scope.chatMessageModel();
        chatMessage.message = message;

        $scope.chatMessages.push(chatMessage);
        $location.hash("message_" + parseInt($scope.chatMessages.length - 1));
        $anchorScroll();
    };

    $scope.changeChatChannel = function (channel) {
        var channels = $scope.getPlayerActiveChatChannels();
        channels = channels.map(function (type) {
            return type.type;
        });

        if (channels.indexOf(channel) != -1) {
            $scope.chatMessage = $scope.chatMessage.substr(0, $scope.chatMessage.length - 1);
            $scope.currentChatChannel = channel;
        }
    };

    $scope.changeChatType = function (type) {
        var types = $scope.getPlayerActiveChatTypes();
        types = types.map(function (type) {
            return type.type;
        });

        if (types.indexOf(type) != -1) {
            $scope.chatMessage = $scope.chatMessage.substr(0, $scope.chatMessage.length - 1);
            $scope.currentChatType = type;
        }

        var typeCode = $scope.getPlayerActiveChatTypes().filter(function (iterateType) {
            return iterateType.type == type;
        })[0].code;

        resourceCall("Chat.triggerChangeChatType", typeCode)
    };
});
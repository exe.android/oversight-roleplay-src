﻿const CTRL_KEY = 13;
const ALT_KEY = 14;

class Chat {
    private static controller: GTANetwork.GUI.Browser;
    private static _chat: GTANetwork.Javascript.JavascriptChat;
    private static lastPressedKey: number;

    public static init = () => {
        Chat.controller = Scope.cef.getChatControl();
        Chat._chat = API.registerChatOverride();
        API.setCefBrowserHeadless(Chat.controller, false);
        API.loadPageCefBrowser(Chat.controller, "res/html/chat.html");

        Chat._chat.onFocusChange.connect(Chat.onFocusChange);
        Chat._chat.onAddMessageRequest.connect(Chat.showMessage);
    }

    private static showMessage = (msg, hasColor, r, g, b) => {
        Chat.controller.call("showMessage", msg);
    }

    public static sendMessage = (msg: string) => {
        Chat._chat.sendMessage(msg);
    }

    public static onFocusChange = (isShow: boolean) => {
        Chat.controller.call("setVar", "chatIsVisible", isShow);
        API.showCursor(isShow);
    }

    public static changeChatType = (type: number) => {
        Chat.controller.call("changeChatType", type);
    }

    public static changeChatChannel = (channel: number) => {
        Chat.controller.call("changeChatChannel", channel);
    }

    public static triggerChangeChatType = (type: string) => {
        API.triggerServerEvent("playerChangingChatType", type);
    }
}
﻿"use strict";

enum InterfaceType {
    Player,
    Vehicle,
    PedCustomize,
    Window
}

enum VehicleSeat {
    Driver = -1
}

enum VarType {
    Int = 0,
    UInt = 1,
    Long = 2,
    ULong = 3,
    String = 4,
    Vector3 = 5,
    Vector2 = 6,
    Float = 7,
    Bool = 8,
    Handle = 9
};


class Interface {
    private currentInterface: InterfaceType;
    private cef: CefHelper;
    private isUserNameSetted: boolean;

    constructor() {
        this.cef = Scope.cef;
        API.onUpdate.connect(this.drawFrame);
    }

    public getCurrentInterface = () => {
        return this.currentInterface;
    }

    public toggleUserInterface = (newInterface: InterfaceType) => {
        this.currentInterface = newInterface;
        this.setBrowserValueDelayed("currentInterface", InterfaceType[newInterface].toLowerCase());

        switch (newInterface) {
            case InterfaceType.Player:
                this.showCursor(false);
                this.cef.loadPage("interface.html");
                break;

            case InterfaceType.Vehicle:
                this.showCursor(false);
                // this.syncVehicleState();
                break;

            case InterfaceType.PedCustomize:
                //pedCustomizer.show()
                break;

            case InterfaceType.Window:
                break;
        }
    }

    // Пока так, но нужно исправить
    public toggleUserInterfaceByName = (interfaceName: string) => {
        if (interfaceName == "player")
            this.toggleUserInterface(InterfaceType.Player);
        if (interfaceName == "vehicle")
            this.toggleUserInterface(InterfaceType.Vehicle);
        if (interfaceName == "pedсustomize")
            this.toggleUserInterface(InterfaceType.PedCustomize);
        if (interfaceName == "window")
            this.toggleUserInterface(InterfaceType.Window);
    }

    private drawFrame = () => {
        let localPlayer = LocalPlayer.getPlayer();

        if (this.currentInterface != InterfaceType.Player && this.currentInterface != InterfaceType.Vehicle)
            return;

        /* if (this.currentInterface == 'player') {
            this.setName();
        } */

        if (this.currentInterface === InterfaceType.Player && !this.isUserNameSetted) {
            this.setBrowserValueDelayed("username", LocalPlayer.getName());
            this.isUserNameSetted = true;
        }

        if (this.currentInterface === InterfaceType.Vehicle) {
            let vehicle = API.getPlayerVehicle(localPlayer);
            let seat = API.getPlayerVehicleSeat(localPlayer);

            if (seat != VehicleSeat.Driver && this.currentInterface != InterfaceType.Player)
                return this.toggleUserInterface(InterfaceType.Player);

            let modelId = API.getEntitySyncedData(vehicle, "MODEL_ID");
            let maxSpeed = 200;

            if (typeof modelId == "number") {
                let modelName = API.getVehicleModelName(modelId);
                maxSpeed = API.getVehicleMaxSpeed(modelId) * 5.6;

                this.setBrowserValue("currentVehicleModelName", modelName);
            }
            else {
                this.setBrowserValue("currentVehicleModelName", "NO_DATA");
            }

            let className = API.getEntitySyncedData(vehicle, "CLASS_NAME");
            if (typeof className == "string") {
                this.setBrowserValue("currentVehicleModelClass", className);
            }
            else {
                this.setBrowserValue("currentVehicleModelClass", "NO_DATA");
            }

            let fuelPercent = API.getEntitySyncedData(vehicle, "FUEL_PERCENT");
            if (typeof fuelPercent == "number") {
                this.setBrowserValue("currentVehicleFuelPercent", fuelPercent);
            }

            let mileage = API.getEntitySyncedData(vehicle, "MILEAGE");
            if (typeof mileage == "number") {
                this.setBrowserValue("currentVehicleMileage", mileage);
            }

            let speed = this.getVehicleSpeed(vehicle);
            let rpm = API.getVehicleRPM(vehicle) * 10;

            this.setBrowserValue("currentVehicleSpeed", Math.floor(speed));
            this.setBrowserValue("currentVehicleMaxSpeed", maxSpeed);
            this.setBrowserValue("currentVehicleRPM", rpm);
        }
    }

    private syncCurrentVehicleData = () => {
        let vehicle = API.getPlayerVehicle(LocalPlayer.getPlayer());

        let engineState = API.getEntitySyncedData(vehicle, "ENGINE_STATE");
        let fuelPercent = API.getEntitySyncedData(vehicle, "FUEL_PERCENT");
        let mileage = API.getEntitySyncedData(vehicle, "MILEAGE");

        // UI.setVehicleEngineStatus(engineState);
        this.setBrowserValue("currentVehicleFuelPercent", fuelPercent);
        this.setBrowserValue("mileage", mileage);
    }

    private getVehicleSpeed = (vehicle: GTANetwork.Util.LocalHandle) => {
        let velocity = API.getEntityVelocity(vehicle);
        let speed = Math.sqrt(
            velocity.X * velocity.X +
            velocity.Y * velocity.Y +
            velocity.Z * velocity.Z
        );
        return speed * 3.6;
    }

    private showCursor = (value: boolean) => {
        API.showCursor(value);
    }

    private setBrowserValue = (key: string, value: any) => {
        Scope.cef.setBrowserValue(key, value);
    }

    private setBrowserValueDelayed = (key: string, value: any) => {
        Scope.cef.setBrowserValueWithDelay(key, value);
    }

    public showLoginForm = () => {
        let rot = new Vector3();
        let pos = new Vector3(-1812.974, 4600.104, 65.62144);
        let targetPos = new Vector3(2611.133, 1793.092, 373.806);
        let loginCamera = API.createCamera(pos, rot);
        let loginEndMovePathcamera = API.createCamera(targetPos, rot);

        this.cef.loadPage("login.html");
        this.cef.show();
        this.showCursor(true);

        API.setActiveCamera(loginCamera);
        API.pointCameraAtPosition(loginCamera, new Vector3());
        API.interpolateCameras(loginCamera, loginEndMovePathcamera, 5000000, false, false);
        API.setHudVisible(false);
    }

    public showWelcomeShard = () => {
        API.showShard("Добро пожаловать на Oversight Roleplay", 4000);
    }
}
﻿// Переписать на camelCase
enum EngineState {
    off,
    ignition,
    after_ignition,
    starting,
    on
}

class LocalVehicle {
    private static getVehicle = () => {
        return API.getPlayerVehicle(LocalPlayer.getPlayer());
    }

    public static getEngineStatus = () => {
        let state = API.getEntitySyncedData(LocalVehicle.getVehicle(), "ENGINE_STATE");
        if (state == "off")
            return EngineState.off;
        if (state == "ignition")
            return EngineState.ignition;
        if (state == "after_ignition")
            return EngineState.after_ignition;
        if (state == "starting")
            return EngineState.starting;
        if (state == "on")
            return EngineState.on;
    }

    private static changeEngineState = (value: EngineState) => {
        let currentEngineStatus = LocalVehicle.getEngineStatus();

        if (value === EngineState.ignition) {
            // Если двигатель не в статусе ОТКЛЮЧЕН, то отбой
            if (currentEngineStatus && currentEngineStatus != EngineState.off)
                return;

            // Включаем зажигание
            LocalVehicle.updateInterface(EngineState.ignition);

            // Ждём 3 секунды
            new DelayedEvent(LocalVehicle.afterIgnition, 3000);

        } else {
            LocalVehicle.updateInterface(value);
        }
    }

    private static afterIgnition = () => {
        // Если игрок вышел из авто, прекращаем скрипт, ставим двигатель в OFF
        if (!API.getPlayerVehicle(LocalPlayer.getPlayer())) {
            LocalVehicle.updateInterface(EngineState.off);
            return;
        }

        // Время зажигания прошло, можно заводить. Лампочки остаются гореть
        LocalVehicle.updateInterface(EngineState.after_ignition);
    }

    private static startingEngine = () => {
        if (LocalVehicle.getEngineStatus() != EngineState.starting)
            return;
        // API.startAudio("res/sound/ignition.mp3", false);
        LocalVehicle.changeEngineState(EngineState.on);
        API.triggerServerEvent("vehicleEngineStateChanged", true);
    }

    private static updateInterface = (state: EngineState) => {
        let engineStateString: string = EngineState[state].toLowerCase();

        Scope.cef.setBrowserValue("currentVehicleEngineState", engineStateString);
        API.setEntitySyncedData(LocalVehicle.getVehicle(), "ENGINE_STATE", engineStateString);
    }

    public static onVehicleIgnitionButtonPressed = (type: KeyNewState, key: System.Windows.Forms.KeyEventArgs) => {
        let vehicle = LocalVehicle.getVehicle();
        if (!vehicle)
            return;

        if (API.getPlayerVehicleSeat(LocalPlayer.getPlayer()) != VehicleSeat.Driver)
            return;

        let currentEngineState = LocalVehicle.getEngineStatus();

        if (key.KeyCode === Keys.B) {
            // Двигатель выключен, включаем зажигание
            if (currentEngineState == EngineState.off|| !currentEngineState)
                LocalVehicle.changeEngineState(EngineState.ignition);

            // Зажигание прошло, начинаем заводить
            if (currentEngineState == EngineState.after_ignition && type === KeyNewState.Down) {
                LocalVehicle.changeEngineState(EngineState.starting);
                let event = new DelayedEvent(LocalVehicle.startingEngine, 1200);
                event.setName("startingEngine");
            }
        }

        // Зажигание прервалось, игрок отпустил кнопку
        if (currentEngineState == EngineState.starting && type === KeyNewState.Up) {
            LocalVehicle.changeEngineState(EngineState.after_ignition);
            Scope.eventHandler.removeEventByName("startingEngine");
        }

        // Глушим двигатель нажатием на N
        if (key.KeyCode === Keys.N) {
            if (currentEngineState === EngineState.on && type === KeyNewState.Down) {
                LocalVehicle.changeEngineState(EngineState.off);
                API.triggerServerEvent("vehicleEngineStateChanged", false);
            }
        }
    }
}
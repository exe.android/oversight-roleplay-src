﻿class Inventory {
    public static playerToggleInventory = () => {
        Inventory.syncInventoryItems();
        Scope.cef.call("toggleInventory");
    }

    public static syncInventoryItems = () => {
        API.triggerServerEvent("playerOpeningInventory");
    }

    public static useItemSelf = (itemId: number) => {
        API.triggerServerEvent("playerUsingItemSelf", itemId);
    }

    public static addObjectToNearbyStoringObjects = (entity: GTANetwork.Util.LocalHandle) => {
        Scope.cef.call("addObjectToNearbyStoringObjects", entity);
    }
}
﻿/// <reference path="../../../../types-gtanetwork/index.d.ts" />

class LocalPlayer {
    private static player: GTANetwork.Util.LocalHandle;
    private static playerName: string;

    public static init = () => {
        LocalPlayer.player = API.getLocalPlayer();
    }

    public static getSyncedData = (key: string) => {       
        return API.getEntitySyncedData(LocalPlayer.player, key);
    }

    public static getName = () => {
        if (!LocalPlayer.playerName)
            LocalPlayer.playerName = API.getPlayerName(API.getLocalPlayer());
        return LocalPlayer.playerName;
    }

    public static getPlayer = () => {
        return API.getLocalPlayer();
    }

    public static spawnNormally = () => {
        API.setGameplayCameraActive();
    }

    public static changeCursorState = (newState: boolean) => {
        API.showCursor(newState);
    }
}
﻿class BaseWindow {
    constructor() {
    }

    protected loadUrl(pageUrl: string) {
        Scope.interface.toggleUserInterface(InterfaceType.Window);
        Scope.cef.loadPage(pageUrl);
        API.showCursor(true);
    }

    public close() {
        Scope.interface.toggleUserInterface(InterfaceType.Player);
    }
}
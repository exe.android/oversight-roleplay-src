﻿class RentalHouseWindow extends BaseWindow {
    constructor() {
        super();

        RentalHouse.loadDataToBrowser();
    }

    public show = () => {
        let pageUrl: string = "rentalhouse/econom.html";
        super.loadUrl(pageUrl);
    }
}
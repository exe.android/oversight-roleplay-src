﻿class Scope {
    public static interface: Interface;
    public static cef: CefHelper;
    public static eventHandler: EventHandler;
    public static windowsFactory: WindowsFactory;

    // Вызывается в onResourceStart
    public static init = () => {
        Scope.cef = new CefHelper(true);
        Scope.interface = new Interface();
        Scope.windowsFactory = new WindowsFactory();

        LocalPlayer.init();
        Chat.init();

        let eventHandler = new EventHandler();
        eventHandler.start();
        Scope.eventHandler = eventHandler;

        let keyHandler = new KeyHandler();
        keyHandler.start();

        Scope.interface.showLoginForm();
    }
}
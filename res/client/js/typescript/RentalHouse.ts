﻿class RentalHouse {
    constructor() {
        
    }

    public static loadDataToBrowser = () => {
        let localPlayer = LocalPlayer.getPlayer();
        Scope.cef.setBrowserValueWithDelay("houseName", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_NAME"));
        Scope.cef.setBrowserValueWithDelay("playersLimit", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_LIMIT"));
        Scope.cef.setBrowserValueWithDelay("playersRentingCount", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_COUNT"));
        Scope.cef.setBrowserValueWithDelay("rentPrice", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENT_PRICE"));
        Scope.cef.setBrowserValueWithDelay("currentPlayerRentingThisHouse", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_IS_LOCAL_PLAYER_RENTING_THIS_HOUSE"));
        Scope.cef.setBrowserValueWithDelay("houseId", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_ID"));
        Scope.cef.setBrowserValueWithDelay("comfortLevel", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_COMFORT_LEVEL")); 
    }

    public static bindEvents = () => {
        CefHelper.eventHandlers["rentHouse"] = () => {
            API.triggerServerEvent("playerRentingHouse");
        }
    }
}
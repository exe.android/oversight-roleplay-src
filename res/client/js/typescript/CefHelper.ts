﻿/// <reference path="../../../../types-gtanetwork/index.d.ts" />

const TickTime = 1000;

class CefHelper {
    public static eventHandlers: { [key: string]: (args: any[]) => void } = {};
    public get IsOpen() { return this._isOpen; }

    private _browserControl: GTANetwork.GUI.Browser;
    private _browserChatControl: GTANetwork.GUI.Browser;
    private _pagePath: string;
    private _isCreated = false;
    private _isOpen = false;
    private _local = true;
    private _eventQueue = new Array<(browser: GTANetwork.GUI.Browser) => void>();
    private _creationTime = 0;
    private _lastCheckTime = 0;

    constructor(local: boolean = true) {
        this._local = local;

        API.onResourceStop.connect(this.onResourceStop);
        API.onUpdate.connect(this.onUpdate);
        this.create();

        CefHelper.eventHandlers["closeWindow"] = () => {
            Scope.interface.toggleUserInterface(InterfaceType.Player);
        }
    }

    private onResourceStop = () => {
        this.destroy();
    };

    public getChatControl = () => {
        return this._browserChatControl;
    }

    private onUpdate = () => {
        if (!this._isOpen) return;

        if (this._eventQueue.length > 0 && this._creationTime > 0 && API.getGlobalTime() - this._lastCheckTime >= TickTime) {
            for (let event of this._eventQueue) {
                event(this._browserControl);
            }
            this._eventQueue = new Array<(browser: GTANetwork.GUI.Browser) => void>();
            //this._eventQueue.shift()(this._browserControl);
            this._lastCheckTime = API.getGlobalTime();
        }

        let currentInterface = Scope.interface.getCurrentInterface();
        if (currentInterface != InterfaceType.Player && currentInterface != InterfaceType.Vehicle) {
            // API.disableAllControlsThisFrame();
        }
    };

    private create = () => {
        if (this._isCreated) return;

        var resolution = API.getScreenResolution();

        this._browserControl = API.createCefBrowser(resolution.Width, resolution.Height, this._local);
        API.waitUntilCefBrowserInit(this._browserControl);
        API.setCefBrowserPosition(this._browserControl, 0, 0);

        this._browserChatControl = API.createCefBrowser(resolution.Width, resolution.Height, this._local);
        API.waitUntilCefBrowserInit(this._browserChatControl);
        API.setCefBrowserPosition(this._browserChatControl, 0, 0);

        this._creationTime = API.getGlobalTime();
        this._isCreated = true;
    };

    private destroy = () => {
        if (!this._isCreated || typeof (this._browserControl) == "undefined" || this._browserControl == null) return;

        this.hide();

        API.destroyCefBrowser(this._browserControl);
        this._browserControl.Dispose();
        this._browserControl = null;
        this._isCreated = false;
    }

    public show = () => {
        if (this._isOpen) return;

        this.create();

        this._isOpen = true;

        API.setCefBrowserHeadless(this._browserControl, false);
        API.showCursor(true);
        // API.setCanOpenChat(false);
    };

    public hide = () => {
        if (!this._isOpen) return;

        this._isOpen = false;

        API.setCanOpenChat(true);
        API.showCursor(false);
        API.setCefBrowserHeadless(this._browserControl, true);
    };

    public loadPage = (page: string) => {
        if (!page.length)
            return;
        API.loadPageCefBrowser(this._browserControl, "res/html/" + page);
        this._lastCheckTime = API.getGlobalTime();
    }

    public callDelayed = (caller: (browser: GTANetwork.GUI.Browser) => void) => {
        this._eventQueue.push(caller);
    };

    public call = (methodName: string, ...args: any[]) => {
        this._browserControl.call(methodName);
    }

    public setBrowserValue = (key: string, value: any) => {
        this._browserControl.call("setVar", key, value);
    }

    public setBrowserValueWithDelay = (key: string, value: any) => {
        this.callDelayed(b => {b.call("setVar", key, value)});
    }

    public static browserCallback = (eventName: string, ...args: any[]) => {
        var handler = CefHelper.eventHandlers[eventName];
        if (typeof (handler) == undefined || handler == null) {
            API.sendChatMessage("~r~No handler for browser event: " + eventName);
            return;
        }
        handler(args);
    }
}
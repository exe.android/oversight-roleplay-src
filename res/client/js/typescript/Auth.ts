﻿class Auth {
    public static bindEvents = () => {
        CefHelper.eventHandlers["processLogIn"] = (args: any[]) => {
            API.sendChatMessage("LOGIN");
            API.triggerServerEvent("processLogIn", args[0], args[1]);
        }
    }
}
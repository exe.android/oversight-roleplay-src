﻿class DelayedEvent {
    private _method: () => void;
    private _timeToDelay: number;
    private _timeWhenExecute: number;
    private _name: string = "";

    constructor(method: () => void, timeForDelay: number) {
        this._method = method;
        this._timeWhenExecute = API.getGlobalTime() + timeForDelay;

        Scope.eventHandler.addEvent(this);
    }

    public getName = () => {
        return this._name;
    }

    public setName = (name: string) => {
        this._name = name;
    }

    public getTimeWhenExecute = () => {
        return this._timeWhenExecute;
    }

    public call = () => {
        this._method();
    }
}
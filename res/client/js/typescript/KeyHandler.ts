﻿enum KeyNewState {
    Up,
    Down
}

class KeyHandler {
    public start() {
        API.onKeyDown.connect(this.onKeyDown);
        API.onKeyUp.connect(this.onKeyUp);
    }

    private onKeyDown = (sender: any, e: System.Windows.Forms.KeyEventArgs) => {
        switch (e.KeyCode) {
            case Keys.I:
                Inventory.playerToggleInventory();
                break;
            case Keys.E:
                API.triggerServerEvent("playerPressedKey", "E");
                break;
            case Keys.Y:
                API.triggerServerEvent("playerPressedKey", "Y");
                break;
            case Keys.Q:
                API.triggerServerEvent("playerPressedKey", "Q");
                break;
            case Keys.H:
                API.triggerServerEvent("playerPressedKey", "H");
                break;
            case Keys.B:
                LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Down, e);
                break;
            case Keys.N:
                LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Down, e);
                break;
        }
    }

    private onKeyUp = (sender: any, e: System.Windows.Forms.KeyEventArgs) => {
        switch (e.KeyCode) {
            case Keys.B:
                LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Up, e);
                break;
        }
    }
}
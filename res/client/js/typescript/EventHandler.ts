﻿/// <reference path="../../../../types-gtanetwork/index.d.ts" />

class EventHandler {
    private started: boolean;
    private _eventQueue = new Array<DelayedEvent>();
    private _lastCheckTime = 0;

    public start = () => {
        if (this.started)
            return;

        API.onServerEventTrigger.connect(this.onServerEventTrigger);
        API.onPlayerEnterVehicle.connect(this.onPlayerEnterVehicle);
        API.onUpdate.connect(this.onUpdate);
        API.onEntityStreamIn.connect(this.onEntityStreamIn);

        RentalHouse.bindEvents();
        Auth.bindEvents();

        this.started = true;
    }

    /*
        Выполняет ивенты из пулла ивентов, в случае выполнения удаляет из списка
    */
    private onUpdate = () => {
        this._eventQueue = this._eventQueue.filter((event) => {
            if (API.getGlobalTime() >= event.getTimeWhenExecute()) {
                event.call();
                // Ивент выполнился, удаляем из списка
                return false;
            }
            // Всё еще держим в списке
            return true;
        });
    }

    private onEntityStreamIn = (ent, entType) => {
        if (entType == 6 || entType == 8) {// Player or ped
            PedCustomizer.setPedCharacter(ent);
        }
    }

    public removeEventByName = (name: string) => {
        this._eventQueue = this._eventQueue.filter((event) => {
            return name != event.getName();
        });
    }

    public addEvent = (event: DelayedEvent) => {
        this._eventQueue.push(event);
    }

    private onPlayerEnterVehicle = (vehicle: GTANetwork.Util.LocalHandle) => {
        Scope.interface.toggleUserInterface(InterfaceType.Vehicle);
    }

    private onServerEventTrigger = (eventName: string, args: System.Array<any>) => {
        switch (eventName) {
            case "logInResult":
                if (args[0] === true) {
                    Scope.interface.showWelcomeShard();
                    LocalPlayer.spawnNormally();
                    API.setHudVisible(true);
                }
                break;
            case "registerResult":
                Scope.cef.callDelayed(b => { b.call("registerResult", args[0], args[1]) });
                break;

            case "spawnNormally":
                Scope.interface.toggleUserInterface(InterfaceType.Player);
                API.setGameplayCameraActive();
                break;

            case "startCustomizing":
                Scope.interface.toggleUserInterface(InterfaceType.PedCustomize);
                let pos = new Vector3(-1091.452, 4369.446, 13.68538);
                let rot = new Vector3(42.38255, 0, 35.4815);
                let currentCamera = API.createCamera(pos, rot);
                API.setActiveCamera(currentCamera);
                API.pointCameraAtPosition(currentCamera, new Vector3(-1088.719, 4370.354, 13.052));
                break;

            case "UPDATE_CHARACTER":
                PedCustomizer.setPedCharacter(LocalPlayer.getPlayer());
                break;

            case "showWindow":
                let windowType = args[0].toString();
                let window = Scope.windowsFactory.getWindow(windowType);
                window.show();
                break;

            case "toggleInterface":
                let interfaceName = args[0].toString().toLowerCase();
                Scope.interface.toggleUserInterfaceByName(interfaceName);
                break;

            case "loadInventoryItems":
                Scope.cef.callDelayed(b => {
                    b.call("loadInventoryItems", args[0]);
                });
                break;
        }
    }
}
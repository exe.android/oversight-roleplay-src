﻿class PedCustomizer {
    public static setPedCharacter(ent) {
        if (API.isPed(ent) && API.getEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA") === true && (API.getEntityModel(ent) == 1885233650 || API.getEntityModel(ent) == -1667301416)) {
            // COMPONENTS
            let componentTshirt = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT");
            let componentTshirtVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 8, componentTshirt, componentTshirtVariation);

            let componentClothes = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES");
            let componentClothesVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 11, componentClothes, componentClothesVariation);

            let componentTorso = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TORSO");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 3, componentTorso, 0);

            let componentLeg = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG");
            let componentLegVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 4, componentLeg, componentLegVariation);

            let componentFoot = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT");
            let componentFootVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 6, componentLeg, componentLegVariation);

            // FACE
            let shapeFirstId = API.getEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID");
            let shapeSecondId = API.getEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID");

            let skinFirstId = API.getEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID");
            let skinSecondId = API.getEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID");

            let shapeMix = API.f(API.getEntitySyncedData(ent, "GTAO_SHAPE_MIX"));
            let skinMix = API.f(API.getEntitySyncedData(ent, "GTAO_SKIN_MIX"));

            API.callNative("SET_PED_HEAD_BLEND_DATA", ent, shapeFirstId, shapeSecondId, 0, skinFirstId, skinSecondId, 0, shapeMix, skinMix, 0, false);

            // HAIR
            let hair = API.getEntitySyncedData(ent, "GTAO_HAIR");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 2, hair, 1);

            // HAIR COLOR
            let hairColor = API.getEntitySyncedData(ent, "GTAO_HAIR_COLOR");
            let highlightColor = API.getEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR");

            API.callNative("_SET_PED_HAIR_COLOR", ent, hairColor, highlightColor);

            // EYE COLOR

            let eyeColor = API.getEntitySyncedData(ent, "GTAO_EYE_COLOR");

            API.callNative("_SET_PED_EYE_COLOR", ent, eyeColor);

            // EYEBROWS, MAKEUP, LIPSTICK
            let eyebrowsStyle = API.getEntitySyncedData(ent, "GTAO_EYEBROWS");
            let eyebrowsColor = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR");
            let eyebrowsColor2 = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2");

            API.callNative("SET_PED_HEAD_OVERLAY", ent, 2, eyebrowsStyle, API.f(1));

            API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 2, 1, eyebrowsColor, eyebrowsColor2);

            if (API.hasEntitySyncedData(ent, "GTAO_LIPSTICK")) {
                let lipstick = API.getEntitySyncedData(ent, "GTAO_LIPSTICK");
                let lipstickColor = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR");
                let lipstickColor2 = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2");

                API.callNative("SET_PED_HEAD_OVERLAY", ent, 8, lipstick, API.f(1));
                API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 8, 2, lipstickColor, lipstickColor2);
            }

            if (API.hasEntitySyncedData(ent, "GTAO_MAKEUP")) {
                let makeup = API.getEntitySyncedData(ent, "GTAO_MAKEUP");
                let makeupColor = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR");
                let makeupColor2 = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2");

                API.callNative("SET_PED_HEAD_OVERLAY", ent, 4, makeup, API.f(1));
                // API.callNative("SET_PED_HEAD_OVERLAY", ent, 8, lipstick, API.f(1));
                API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 4, 0, makeupColor, makeupColor2);
            }

            // FACE FEATURES (e.g. nose length, chin shape, etc)

            let faceFeatureList = API.getEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST");

            for (let i = 0; i < 21; i++) {
                API.callNative("_SET_PED_FACE_FEATURE", ent, i, API.f(faceFeatureList[i]));
            }
        }
    }
}
﻿/*
 VARIABLES
 */

// let mainBrowser = null;
// let lastUpdate = 0;
// let lastVehicle = null;

// let pressingKey = null;
// let res = API.getScreenResolution();
// let localPlayer = null;
// let playerLogged = false;
//
// let currentCamera = null;
//
// let varType = {
//     Int: 0,
//     UInt: 1,
//     Long: 2,
//     ULong: 3,
//     String: 4,
//     Vector3: 5,
//     Vector2: 6,
//     Float: 7,
//     Bool: 8,
//     Handle: 9,
// };

// let rot = new Vector3();
// let pos = new Vector3(-1812.974 , 4600.104, 65.62144);
// let targetPos = new Vector3(2611.133, 1793.092, 373.806);
// let loginCamera = API.createCamera(pos, rot);
// let loginEndMovePathcamera = API.createCamera(targetPos, rot);

/*
 CALLBACKS
 */

// API.onResourceStart.connect(function () {
//     localPlayer = API.getLocalPlayer();
//     UI.showLoginForm();
//     //playerLogged = true;
//     //UI.init();
//     //UI.toggleUserInterface("faceCustomize");
// });

API.onResourceStop.connect(function () {
    UI.closeBrowser();
});

API.onPlayerExitVehicle.connect(function (vehicle) {
    UI.toggleUserInterface('player');
});

API.onPlayerEnterVehicle.connect(function (vehicle) {
    lastVehicle = vehicle;
    UI.toggleUserInterface('vehicle');
});

API.onKeyUp.connect(function (sender, args) {
    if (args.KeyCode == Keys.B) {
        onVehicleIgnitionButtonPressed('up', args.KeyCode);
    }
});

API.onKeyDown.connect(function (sender, args) {
    if (args.KeyCode == Keys.B || args.KeyCode == Keys.N) {
        onVehicleIgnitionButtonPressed('down', args.KeyCode);
    }

    if (args.KeyCode == Keys.Q) {
        if (pedCustomizer.currentStep == 1)
            pedCustomizer.changeCurrentSlot('minus');
        if (pedCustomizer.currentStep == 2)
            pedCustomizer.changeComponent('minus');
    }

    if (args.KeyCode == Keys.P) {
        let position = API.getGameplayCamPos();
        let rotation = API.getGameplayCamRot();
        API.triggerServerEvent("saveCamera", position, rotation);
    }

    if (args.KeyCode == Keys.E) {
        if (UI.currentInterface != 'faceCustomize') {
            API.showCursor(!API.isCursorShown());
        }

        if (UI.currentInterface == "faceCustomize") {
            if (pedCustomizer.currentStep == 1)
                pedCustomizer.changeCurrentSlot('plus');
            if (pedCustomizer.currentStep == 2)
                pedCustomizer.changeComponent('plus');
        }

        API.triggerServerEvent("playerPressedKey", "E");
    }

    if (args.KeyCode == Keys.L) {
        // UI.toggleUserInterface("faceCustomize");
        syncCurrentRentalHouseData();
    }

    if (args.KeyCode == Keys.Y) {
        API.triggerServerEvent("playerPressedKey", "Y");
    }

    if (args.KeyCode == Keys.Q) {
        API.triggerServerEvent("playerPressedKey", "Q");
    }

    if (args.KeyCode == Keys.H) {
        API.triggerServerEvent("playerPressedKey", "H");
    }
});

API.onServerEventTrigger.connect(function (eventName, args) {
    if (eventName == "logInResult") {
        if (args[0] == true) {
            API.showShard("Добро пожаловать на Oversight Roleplay", 4000);
            API.setHudVisible(true);
            //API.showCursor(false);
            //UI.init();
            playerLogged = true;
            // UI.toggleUserInterface("faceCustomize");
        }
        mainBrowser.call("onLogInResult", args[0]);
    }

    if (eventName == "registerResult") {
        mainBrowser.call("registerResult", args[0], args[1]);
    }

    if (eventName == 'spawnNormally') {
        UI.init();
        API.setGameplayCameraActive();
    }

    // if (eventName == 'startCustomizing') {
    //     UI.toggleUserInterface("faceCustomize");
    //     let pos = new Vector3(-1091.452, 4369.446, 13.68538);
    //     let rot = new Vector3(42.38255, 0, 35.4815);
    //     currentCamera = API.createCamera(pos, rot);
    //     API.setActiveCamera(currentCamera);
    //     API.pointCameraAtPosition(currentCamera, new Vector3(-1088.719, 4370.354, 13.052));
    // }
    //
    // if (eventName == "UPDATE_CHARACTER") {
    //     setPedCharacter(localPlayer);
    // }
    //
    // if (eventName == "showWindow") {
    //     let windowType = args[0].toString();
    //     let windowName = args[1].toString();
    //     let urlString = windowType + "/" + windowName + ".html";
    //     UI.toggleUserInterface('window');
    //     UI.loadPage(urlString);
    //
    //     if (windowType == 'rentalhouse') {
    //         syncCurrentRentalHouseData();
    //     }
    //
    //     API.showCursor(true);
    // }
    //
    // if (eventName == "toggleInterface") {
    //     let interfaceName = args[0].toString().toLowerCase();
    //     UI.toggleUserInterface(interfaceName);
    // }
});

// function syncCurrentRentalHouseData()
// {
//     API.sleep(300);
//     UI.setVar("houseName", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_NAME"));
//     UI.setVar("playersLimit", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_LIMIT"));
//     UI.setVar("playersRentingCount", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_COUNT"));
//     UI.setVar("rentPrice", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENT_PRICE"));
//     UI.setVar("currentPlayerRentingThisHouse", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_IS_LOCAL_PLAYER_RENTING_THIS_HOUSE"));
//     UI.setVar("houseId", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_ID"));
//     UI.setVar("comfortLevel", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_COMFORT_LEVEL"));
//     API.sendChatMessage("RENTPRICE: " + API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENT_PRICE"));
// }
//
// API.onUpdate.connect(function () {
//     if (mainBrowser != null) {
//         localPlayer = API.getLocalPlayer();
//         lastUpdate = API.getGlobalTime();
//         UI.drawFrame();
//     }
//     // Игрок жмет левую кнопку мышки
//     if (API.isControlJustPressed(24)) {
//         API.stopPlayerAnimation();
//         API.triggerServerEvent("playerBeginAttack");
//     }
// });

API.onResourceStart.connect(function () {
    let players = API.getStreamedPlayers();
    lastAttackControlCheck = API.getGlobalTime();
    for (let i = players.Length - 1; i >= 0; i--) {
        setPedCharacter(players[i]);
    }
});

API.onEntityStreamIn.connect(function (ent, entType) {
    if (entType == 6 || entType == 8) {// Player or ped
        setPedCharacter(ent);
    }
});

/*
 CLASSES
 */

// FaceCustomizer

let oFaceCustomizer = function () {
    this.currentSlot = null;
    this.currentStep = 1;

    this.changeStep = function (step) {
        if (step < 1 || step > 2)
            return false;
        this.currentStep = step;
    };

    this.savePed = function () {
        UI.toggleUserInterface('player');
        API.triggerServerEvent("savePedComponents");
    };

    // type: plus | minus
    // Вторая страница кастомизации
    this.changeComponent = function (type) {
        let slot = this.currentSlot;
        let limit = 0;

        if (slot == "GTAO_PED_COMPONENT_TSHIRT")
            limit = API.returnNative("GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS", varType.Int, localPlayer, 8);
        if (slot == "GTAO_PED_COMPONENT_TSHIRT_VARIATION")
            limit = API.returnNative("GET_NUMBER_OF_PED_TEXTURE_VARIATIONS", varType.Int, localPlayer, 8, 0);

        if (slot == "GTAO_PED_COMPONENT_CLOTHES")
            limit = API.returnNative("GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS", varType.Int, localPlayer, 11);
        if (slot == "GTAO_PED_COMPONENT_CLOTHES_VARIATION")
            limit = API.returnNative("GET_NUMBER_OF_PED_TEXTURE_VARIATIONS", varType.Int, localPlayer, 11, 0);

        if (slot == "GTAO_PED_COMPONENT_TORSO")
            limit = API.returnNative("GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS", varType.Int, localPlayer, 3);

        if (slot == "GTAO_PED_COMPONENT_LEG")
            limit = API.returnNative("GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS", varType.Int, localPlayer, 4);
        if (slot == "GTAO_PED_COMPONENT_LEG_VARIATION")
            limit = API.returnNative("GET_NUMBER_OF_PED_TEXTURE_VARIATIONS", varType.Int, localPlayer, 4, 0);

        if (slot == "GTAO_PED_COMPONENT_FOOT")
            limit = API.returnNative("GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS", varType.Int, localPlayer, 6);
        if (slot == "GTAO_PED_COMPONENT_FOOT_VARIATION")
            limit = API.returnNative("GET_NUMBER_OF_PED_TEXTURE_VARIATIONS", varType.Int, localPlayer, 6, 0);

        let newValue = 0;

        let currentComponentValue = API.getEntitySyncedData(localPlayer, this.currentSlot);

        if (type == 'minus') {
            if (currentComponentValue <= 0)
                return;
            newValue = --currentComponentValue;
        }
        if (type == 'plus') {
            newValue = ++currentComponentValue;
            if (newValue > limit) {
                return;
            }
        }

        API.setEntitySyncedData(localPlayer, this.currentSlot, newValue);
        setPedCharacter(localPlayer);
    };

    // Первая страница кастомизации
    this.changeCurrentSlot = function (type) {
        if (!this.currentSlot)
            return;
        let currentSlotData = parseInt(API.getEntitySyncedData(localPlayer, this.currentSlot));
        let newValue = 0;

        if (type == 'minus') {
            if (currentSlotData <= 0)
                return;
            newValue = --currentSlotData;
        }

        if (type == 'plus') {
            newValue = ++currentSlotData;
        }

        API.setEntitySyncedData(localPlayer, this.currentSlot, newValue);
        API.sendChatMessage("HAIR: " + API.getEntitySyncedData(localPlayer, "GTAO_HAIR"));
        setPedCharacter(localPlayer);
    };

    this.show = function () {
        UI.loadPage("facecustomize.html");
        this.currentStep = 1;
        API.showCursor(true);
    }
};

let pedCustomizer = new oFaceCustomizer();

// User Interface
//
// let oUserInterface = function () {
//     this.currentInterface = null;
//
//     this.create = function () {
//         mainBrowser = API.createCefBrowser(res.Width, res.Height);
//         API.waitUntilCefBrowserInit(mainBrowser);
//         API.setCefBrowserPosition(mainBrowser, 0, 0);
//     };

    // this.init = function () {
    //     this.loadPage("interface.html");
    //     this.toggleUserInterface('player');
    //     this.setName();
    // };

    // this.toggleUserInterface = function (value) {
    //     if (value == 'player') {
    //         API.showCursor(false);
    //         this.loadPage("interface.html");
    //         this.currentInterface = 'player';
    //         this.setName();
    //     }
    //     if (value == 'vehicle') {
    //         API.showCursor(false);
    //         //this.loadPage("interface.html");
    //         this.currentInterface = 'vehicle';
    //         this.setVar("currentInterface", "vehicle");
    //         this.syncVehicleState();
    //     }
    //     if (value == 'faceCustomize') {
    //         this.currentInterface = 'faceCustomize';
    //         pedCustomizer.show();
    //     }
    //     if (value == 'window') {
    //         this.currentInterface = 'window';
    //     }
    // };

    //
    // this.drawFrame = function () {
    //     if (!playerLogged)
    //         return;
    //     localPlayer = API.getLocalPlayer();
    //
    //     if (this.currentInterface != 'player' && this.currentInterface != 'vehicle')
    //         return;
    //
    //     if (this.currentInterface == 'player') {
    //         this.setName();
    //     }
    //
    //     if (this.currentInterface == 'vehicle') {
    //         let vehicle = API.getPlayerVehicle(localPlayer);
    //         let seat = API.getPlayerVehicleSeat(localPlayer);
    //
    //         if (seat != -1)
    //             return this.toggleUserInterface('player');
    //
    //         let modelId = API.getEntitySyncedData(vehicle, "MODEL_ID");
    //         if (typeof modelId == "number") {
    //             let modelName = API.getVehicleModelName(modelId);
    //             max_speed = API.getVehicleMaxSpeed(modelId) * 3.6;
    //
    //             this.setVar("currentVehicleModelName", modelName);
    //         }
    //         else {
    //             this.setVar("currentVehicleModelName", "NO_DATA");
    //             max_speed = 200;
    //         }
    //
    //         let className = API.getEntitySyncedData(vehicle, "CLASS_NAME");
    //         if (typeof className == "string") {
    //             this.setVar("currentVehicleModelClass", className);
    //         }
    //         else {
    //             this.setVar("currentVehicleModelClass", "NO_DATA");
    //         }
    //
    //         let fuelPercent = API.getEntitySyncedData(lastVehicle, "FUEL_PERCENT");
    //         if (typeof fuelPercent == "number") {
    //             UI.setFuelBarToPercent(fuelPercent);
    //         }
    //
    //         let mileage = API.getEntitySyncedData(lastVehicle, "MILEAGE");
    //         if (typeof mileage == "number") {
    //             UI.setMileage(mileage);
    //         }
    //
    //         let speed = getVehicleSpeed(vehicle);
    //         let rpm = API.getVehicleRPM(vehicle) * 10;
    //
    //         this.setVar("currentVehicleSpeed", Math.floor(speed));
    //         this.setVar("currentVehicleMaxSpeed", max_speed);
    //         this.setVar("currentVehicleRPM", rpm);
    //     }
    // };

    // this.setVehicleEngineStatus = function (value) {
    //     let currentEngineStatus = API.getEntitySyncedData(lastVehicle, "ENGINE_STATE");
    //     if (value == "ignition") {
    //         // Если двигатель не отключен, то отбой
    //         if (currentEngineStatus && currentEngineStatus != "off")
    //             return;
    //
    //         // Включаем зажигание
    //         setVehicleEngineState(lastVehicle, value);
    //
    //         // Ждём 3 секунды
    //         API.sleep(3000);
    //
    //         // Если игрок вышел из авто, прекращаем скрипт, ставим двигатель в OFF
    //         if (!API.getPlayerVehicle(localPlayer)) {
    //             setVehicleEngineState(lastVehicle, "off");
    //             return;
    //         }
    //
    //         // Время зажигания прошло, можно заводить. Лампочки остаются гореть
    //         setVehicleEngineState(lastVehicle, "after_ignition");
    //     }
    //
    //     if (value == "after_ignition") {
    //         setVehicleEngineState(lastVehicle, "after_ignition");
    //     }
    //
    //     if (value == "starting") {
    //         // Начинаем заводить машину
    //         setVehicleEngineState(lastVehicle, "starting");
    //     }
    //
    //     if (value == "on") {
    //         setVehicleEngineState(lastVehicle, "on");
    //     }
    //
    //     if (value == "off") {
    //         // Отключаем двигатель
    //         setVehicleEngineState(lastVehicle, "off");
    //     }
    // };

    // this.setFuelBarToPercent = function (percent) {
    //     this.setVar("currentVehicleFuelPercent", percent);
    // };

    // this.setMileage = function (mileage) {
    //     this.setVar("currentVehicleMileage", mileage);
    // };

    // this.setName = function () {
    //     this.setVar("username", API.getPlayerName(localPlayer));
    // };
    //
    // this.openBrowser = function () {
    //     if (mainBrowser != null) {
    //         API.setCefBrowserHeadless(mainBrowser, false);
    //     }
    // };
    //
    // this.closeBrowser = function () {
    //     if (mainBrowser != null) {
    //         API.setCefBrowserHeadless(mainBrowser, true);
    //     }
    // };

    // this.loadPage = function (url) {
    //     if (mainBrowser == null)
    //         this.openBrowser();
    //     API.waitUntilCefBrowserInit(mainBrowser);
    //     url = "res/html/" + url;
    //     API.loadPageCefBrowser(mainBrowser, url);
    // };
    //
    // this.setVar = function (name, value) {
    //     mainBrowser.call("setVar", name, value);
    // };

    // this.showLoginForm = function () {
    //     this.loadPage("login.html");
    //     API.showCursor(true);
    //
    //     API.setActiveCamera(loginCamera);
    //     API.pointCameraAtPosition(loginCamera, new Vector3());
    //     API.interpolateCameras(loginCamera, loginEndMovePathcamera, 5000000, false, false);
    //     API.setHudVisible(false);
    // };

    // this.syncVehicleState = function () {
    //     let engineState = API.getEntitySyncedData(lastVehicle, "ENGINE_STATE");
    //     let fuelPercent = API.getEntitySyncedData(lastVehicle, "FUEL_PERCENT");
    //     let mileage = API.getEntitySyncedData(lastVehicle, "MILEAGE");
    //
    //     UI.setVehicleEngineStatus(engineState);
    //     UI.setFuelBarToPercent(fuelPercent);
    //     UI.setMileage(mileage);
    // };
};

let UI = new oUserInterface();
UI.create();

/*
 HANDLE FUNCTIONS
 */

function setCurrentFaceCustomizeSlot(slot) {
    pedCustomizer.currentSlot = slot;
}

// function processLogIn(login, password) {
//     API.triggerServerEvent("processLogIn", login, password);
// }

function processRegistration(login, password, email, name, surname) {
    API.triggerServerEvent("processRegistration", login, password, email, name, surname);
}

// function getVehicleSpeed(vehicle) {
//     let velocity = API.getEntityVelocity(vehicle);
//     let speed = Math.sqrt(
//         velocity.X * velocity.X +
//         velocity.Y * velocity.Y +
//         velocity.Z * velocity.Z
//     );
//     return speed * 3.6;
// }

// function setVehicleEngineState(vehicle, state) {
//     UI.setVar("currentVehicleEngineState", state);
//     API.setEntitySyncedData(vehicle, "ENGINE_STATE", state);
// }

// Повторно получаем статус двигателя, проверяем, не отпустил ли игрок кнопку
// function* startingEngine() {
//     UI.setVehicleEngineStatus("starting");
//     yield 1200;
//     if (API.getEntitySyncedData(lastVehicle, "ENGINE_STATE") != "starting")
//         return;
//     API.startAudio("res/sound/ignition.mp3", false);
//     UI.setVehicleEngineStatus("on");
//     API.triggerServerEvent("vehicleEngineStateChanged", true);
// }

// // Нажал кнопку запуска двигателя (type = UP || DOWN)
// function onVehicleIgnitionButtonPressed(type, key) {
//     let vehicle = API.getPlayerVehicle(localPlayer);
//     if (!vehicle)
//         return;
//
//     if (API.getPlayerVehicleSeat(localPlayer) == -1) {
//         let currentEngineState = API.getEntitySyncedData(vehicle, "ENGINE_STATE");
//         if (key == Keys.B) {
// //        API.sendChatMessage("STATE " + currentEngineState, " TYPE " + type);
//
//             // Двигатель выключен, включаем зажигание
//             if (currentEngineState == "off" || !currentEngineState)
//                 UI.setVehicleEngineStatus("ignition");
//
//             // Зажигание прошло, начинаем заводить
//             if (currentEngineState == "after_ignition" && type == 'down') {
//                 API.startCoroutine(startingEngine);
//             }
//
//             // Зажигание прервалось, игрок отпустил кнопку
//             if (currentEngineState == "starting" && type == 'up') {
//                 UI.setVehicleEngineStatus("after_ignition");
//             }
//         }
//
//         // Глушим двигатель нажатием на N
//         if (key == Keys.N) {
//             if (currentEngineState == "on" && type == 'down') {
//                 UI.setVehicleEngineStatus("off");
//                 API.triggerServerEvent("vehicleEngineStateChanged", false);
//             }
//         }
//
//     }
// }

function playerHiringToWork(work) {
    API.triggerServerEvent("playerHiringToWork", "lumberJack")
}

function faceShapesChanged(index, value) {
    let currentFaceFeatureList = API.getEntitySyncedData(localPlayer, "GTAO_FACE_FEATURES_LIST");
    currentFaceFeatureList[index] = value;
    API.setEntitySyncedData(localPlayer, "GTAO_FACE_FEATURES_LIST", currentFaceFeatureList);
    setPedCharacter(localPlayer);
}

// function closeWindow() {
//     UI.toggleUserInterface('player');
// }

// function setPedCharacter(ent) {
//     if (API.isPed(ent) &&
//         API.getEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA") === true &&
//         (API.getEntityModel(ent) == 1885233650 || // FreemodeMale
//         API.getEntityModel(ent) == -1667301416)) // FreemodeFemale+
//     {
//
//         // COMPONENTS
//         let componentTshirt = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT");
//         let componentTshirtVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT_VARIATION");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 8, componentTshirt, componentTshirtVariation);
//
//         let componentClothes = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES");
//         let componentClothesVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES_VARIATION");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 11, componentClothes, componentClothesVariation);
//
//         let componentTorso = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TORSO");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 3, componentTorso, 0);
//
//         let componentLeg = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG");
//         let componentLegVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG_VARIATION");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 4, componentLeg, componentLegVariation);
//
//         let componentFoot = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT");
//         let componentFootVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT_VARIATION");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 6, componentLeg, componentLegVariation);
//
//         // FACE
//         let shapeFirstId = API.getEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID");
//         let shapeSecondId = API.getEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID");
//
//         let skinFirstId = API.getEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID");
//         let skinSecondId = API.getEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID");
//
//         let shapeMix = API.f(API.getEntitySyncedData(ent, "GTAO_SHAPE_MIX"));
//         let skinMix = API.f(API.getEntitySyncedData(ent, "GTAO_SKIN_MIX"));
//
//         API.callNative("SET_PED_HEAD_BLEND_DATA", ent, shapeFirstId, shapeSecondId, 0, skinFirstId, skinSecondId, 0, shapeMix, skinMix, 0, false);
//
//         // HAIR
//         let hair = API.getEntitySyncedData(ent, "GTAO_HAIR");
//         API.callNative("SET_PED_COMPONENT_VARIATION", ent, 2, hair, 1);
//
//         // HAIR COLOR
//         let hairColor = API.getEntitySyncedData(ent, "GTAO_HAIR_COLOR");
//         let highlightColor = API.getEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR");
//
//         API.callNative("_SET_PED_HAIR_COLOR", ent, hairColor, highlightColor);
//
//         // EYE COLOR
//
//         let eyeColor = API.getEntitySyncedData(ent, "GTAO_EYE_COLOR");
//
//         API.callNative("_SET_PED_EYE_COLOR", ent, eyeColor);
//
//         // EYEBROWS, MAKEUP, LIPSTICK
//         let eyebrowsStyle = API.getEntitySyncedData(ent, "GTAO_EYEBROWS");
//         let eyebrowsColor = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR");
//         let eyebrowsColor2 = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2");
//
//         API.callNative("SET_PED_HEAD_OVERLAY", ent, 2, eyebrowsStyle, API.f(1));
//
//         API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 2, 1, eyebrowsColor, eyebrowsColor2);
//
//         if (API.hasEntitySyncedData(ent, "GTAO_LIPSTICK")) {
//             let lipstick = API.getEntitySyncedData(ent, "GTAO_LIPSTICK");
//             let lipstickColor = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR");
//             let lipstickColor2 = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2");
//
//             API.callNative("SET_PED_HEAD_OVERLAY", ent, 8, lipstick, API.f(1));
//             API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 8, 2, lipstickColor, lipstickColor2);
//         }
//
//         if (API.hasEntitySyncedData(ent, "GTAO_MAKEUP")) {
//             let makeup = API.getEntitySyncedData(ent, "GTAO_MAKEUP");
//             let makeupColor = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR");
//             let makeupColor2 = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2");
//
//             API.callNative("SET_PED_HEAD_OVERLAY", ent, 4, makeup, API.f(1));
//             API.callNative("SET_PED_HEAD_OVERLAY", ent, 8, lipstick, API.f(1));
//             API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 4, 0, makeupColor, makeupColor2);
//         }
//
//         // FACE FEATURES (e.g. nose length, chin shape, etc)
//
//         let faceFeatureList = API.getEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST");
//
//         for (let i = 0; i < 21; i++) {
//             API.callNative("_SET_PED_FACE_FEATURE", ent, i, API.f(faceFeatureList[i]));
//         }
//
//     }
// }
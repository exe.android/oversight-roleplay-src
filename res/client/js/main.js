class Auth {
}
Auth.bindEvents = () => {
    CefHelper.eventHandlers["processLogIn"] = (args) => {
        API.sendChatMessage("LOGIN");
        API.triggerServerEvent("processLogIn", args[0], args[1]);
    };
};
const TickTime = 1000;
class CefHelper {
    constructor(local = true) {
        this._isCreated = false;
        this._isOpen = false;
        this._local = true;
        this._eventQueue = new Array();
        this._creationTime = 0;
        this._lastCheckTime = 0;
        this.onResourceStop = () => {
            this.destroy();
        };
        this.getChatControl = () => {
            return this._browserChatControl;
        };
        this.onUpdate = () => {
            if (!this._isOpen)
                return;
            if (this._eventQueue.length > 0 && this._creationTime > 0 && API.getGlobalTime() - this._lastCheckTime >= TickTime) {
                for (let event of this._eventQueue) {
                    event(this._browserControl);
                }
                this._eventQueue = new Array();
                this._lastCheckTime = API.getGlobalTime();
            }
            let currentInterface = Scope.interface.getCurrentInterface();
            if (currentInterface != InterfaceType.Player && currentInterface != InterfaceType.Vehicle) {
            }
        };
        this.create = () => {
            if (this._isCreated)
                return;
            var resolution = API.getScreenResolution();
            this._browserControl = API.createCefBrowser(resolution.Width, resolution.Height, this._local);
            API.waitUntilCefBrowserInit(this._browserControl);
            API.setCefBrowserPosition(this._browserControl, 0, 0);
            this._browserChatControl = API.createCefBrowser(resolution.Width, resolution.Height, this._local);
            API.waitUntilCefBrowserInit(this._browserChatControl);
            API.setCefBrowserPosition(this._browserChatControl, 0, 0);
            this._creationTime = API.getGlobalTime();
            this._isCreated = true;
        };
        this.destroy = () => {
            if (!this._isCreated || typeof (this._browserControl) == "undefined" || this._browserControl == null)
                return;
            this.hide();
            API.destroyCefBrowser(this._browserControl);
            this._browserControl.Dispose();
            this._browserControl = null;
            this._isCreated = false;
        };
        this.show = () => {
            if (this._isOpen)
                return;
            this.create();
            this._isOpen = true;
            API.setCefBrowserHeadless(this._browserControl, false);
            API.showCursor(true);
        };
        this.hide = () => {
            if (!this._isOpen)
                return;
            this._isOpen = false;
            API.setCanOpenChat(true);
            API.showCursor(false);
            API.setCefBrowserHeadless(this._browserControl, true);
        };
        this.loadPage = (page) => {
            if (!page.length)
                return;
            API.loadPageCefBrowser(this._browserControl, "res/html/" + page);
            this._lastCheckTime = API.getGlobalTime();
        };
        this.callDelayed = (caller) => {
            this._eventQueue.push(caller);
        };
        this.call = (methodName, ...args) => {
            this._browserControl.call(methodName);
        };
        this.setBrowserValue = (key, value) => {
            this._browserControl.call("setVar", key, value);
        };
        this.setBrowserValueWithDelay = (key, value) => {
            this.callDelayed(b => { b.call("setVar", key, value); });
        };
        this._local = local;
        API.onResourceStop.connect(this.onResourceStop);
        API.onUpdate.connect(this.onUpdate);
        this.create();
        CefHelper.eventHandlers["closeWindow"] = () => {
            Scope.interface.toggleUserInterface(InterfaceType.Player);
        };
    }
    get IsOpen() { return this._isOpen; }
}
CefHelper.eventHandlers = {};
CefHelper.browserCallback = (eventName, ...args) => {
    var handler = CefHelper.eventHandlers[eventName];
    if (typeof (handler) == undefined || handler == null) {
        API.sendChatMessage("~r~No handler for browser event: " + eventName);
        return;
    }
    handler(args);
};
const CTRL_KEY = 13;
const ALT_KEY = 14;
class Chat {
}
Chat.init = () => {
    Chat.controller = Scope.cef.getChatControl();
    Chat._chat = API.registerChatOverride();
    API.setCefBrowserHeadless(Chat.controller, false);
    API.loadPageCefBrowser(Chat.controller, "res/html/chat.html");
    Chat._chat.onFocusChange.connect(Chat.onFocusChange);
    Chat._chat.onAddMessageRequest.connect(Chat.showMessage);
};
Chat.showMessage = (msg, hasColor, r, g, b) => {
    Chat.controller.call("showMessage", msg);
};
Chat.sendMessage = (msg) => {
    Chat._chat.sendMessage(msg);
};
Chat.onFocusChange = (isShow) => {
    Chat.controller.call("setVar", "chatIsVisible", isShow);
    API.showCursor(isShow);
};
Chat.changeChatType = (type) => {
    Chat.controller.call("changeChatType", type);
};
Chat.changeChatChannel = (channel) => {
    Chat.controller.call("changeChatChannel", channel);
};
Chat.triggerChangeChatType = (type) => {
    API.triggerServerEvent("playerChangingChatType", type);
};
class DelayedEvent {
    constructor(method, timeForDelay) {
        this._name = "";
        this.getName = () => {
            return this._name;
        };
        this.setName = (name) => {
            this._name = name;
        };
        this.getTimeWhenExecute = () => {
            return this._timeWhenExecute;
        };
        this.call = () => {
            this._method();
        };
        this._method = method;
        this._timeWhenExecute = API.getGlobalTime() + timeForDelay;
        Scope.eventHandler.addEvent(this);
    }
}
class EventHandler {
    constructor() {
        this._eventQueue = new Array();
        this._lastCheckTime = 0;
        this.start = () => {
            if (this.started)
                return;
            API.onServerEventTrigger.connect(this.onServerEventTrigger);
            API.onPlayerEnterVehicle.connect(this.onPlayerEnterVehicle);
            API.onUpdate.connect(this.onUpdate);
            API.onEntityStreamIn.connect(this.onEntityStreamIn);
            RentalHouse.bindEvents();
            Auth.bindEvents();
            this.started = true;
        };
        this.onUpdate = () => {
            this._eventQueue = this._eventQueue.filter((event) => {
                if (API.getGlobalTime() >= event.getTimeWhenExecute()) {
                    event.call();
                    return false;
                }
                return true;
            });
        };
        this.onEntityStreamIn = (ent, entType) => {
            if (entType == 6 || entType == 8) {
                PedCustomizer.setPedCharacter(ent);
            }
        };
        this.removeEventByName = (name) => {
            this._eventQueue = this._eventQueue.filter((event) => {
                return name != event.getName();
            });
        };
        this.addEvent = (event) => {
            this._eventQueue.push(event);
        };
        this.onPlayerEnterVehicle = (vehicle) => {
            Scope.interface.toggleUserInterface(InterfaceType.Vehicle);
        };
        this.onServerEventTrigger = (eventName, args) => {
            switch (eventName) {
                case "logInResult":
                    if (args[0] === true) {
                        Scope.interface.showWelcomeShard();
                        LocalPlayer.spawnNormally();
                        API.setHudVisible(true);
                    }
                    break;
                case "registerResult":
                    Scope.cef.callDelayed(b => { b.call("registerResult", args[0], args[1]); });
                    break;
                case "spawnNormally":
                    Scope.interface.toggleUserInterface(InterfaceType.Player);
                    API.setGameplayCameraActive();
                    break;
                case "startCustomizing":
                    Scope.interface.toggleUserInterface(InterfaceType.PedCustomize);
                    let pos = new Vector3(-1091.452, 4369.446, 13.68538);
                    let rot = new Vector3(42.38255, 0, 35.4815);
                    let currentCamera = API.createCamera(pos, rot);
                    API.setActiveCamera(currentCamera);
                    API.pointCameraAtPosition(currentCamera, new Vector3(-1088.719, 4370.354, 13.052));
                    break;
                case "UPDATE_CHARACTER":
                    PedCustomizer.setPedCharacter(LocalPlayer.getPlayer());
                    break;
                case "showWindow":
                    let windowType = args[0].toString();
                    let window = Scope.windowsFactory.getWindow(windowType);
                    window.show();
                    break;
                case "toggleInterface":
                    let interfaceName = args[0].toString().toLowerCase();
                    Scope.interface.toggleUserInterfaceByName(interfaceName);
                    break;
                case "loadInventoryItems":
                    Scope.cef.callDelayed(b => {
                        b.call("loadInventoryItems", args[0]);
                    });
                    break;
            }
        };
    }
}
"use strict";
var InterfaceType;
(function (InterfaceType) {
    InterfaceType[InterfaceType["Player"] = 0] = "Player";
    InterfaceType[InterfaceType["Vehicle"] = 1] = "Vehicle";
    InterfaceType[InterfaceType["PedCustomize"] = 2] = "PedCustomize";
    InterfaceType[InterfaceType["Window"] = 3] = "Window";
})(InterfaceType || (InterfaceType = {}));
var VehicleSeat;
(function (VehicleSeat) {
    VehicleSeat[VehicleSeat["Driver"] = -1] = "Driver";
})(VehicleSeat || (VehicleSeat = {}));
var VarType;
(function (VarType) {
    VarType[VarType["Int"] = 0] = "Int";
    VarType[VarType["UInt"] = 1] = "UInt";
    VarType[VarType["Long"] = 2] = "Long";
    VarType[VarType["ULong"] = 3] = "ULong";
    VarType[VarType["String"] = 4] = "String";
    VarType[VarType["Vector3"] = 5] = "Vector3";
    VarType[VarType["Vector2"] = 6] = "Vector2";
    VarType[VarType["Float"] = 7] = "Float";
    VarType[VarType["Bool"] = 8] = "Bool";
    VarType[VarType["Handle"] = 9] = "Handle";
})(VarType || (VarType = {}));
;
class Interface {
    constructor() {
        this.getCurrentInterface = () => {
            return this.currentInterface;
        };
        this.toggleUserInterface = (newInterface) => {
            this.currentInterface = newInterface;
            this.setBrowserValueDelayed("currentInterface", InterfaceType[newInterface].toLowerCase());
            switch (newInterface) {
                case InterfaceType.Player:
                    this.showCursor(false);
                    this.cef.loadPage("interface.html");
                    break;
                case InterfaceType.Vehicle:
                    this.showCursor(false);
                    break;
                case InterfaceType.PedCustomize:
                    break;
                case InterfaceType.Window:
                    break;
            }
        };
        this.toggleUserInterfaceByName = (interfaceName) => {
            if (interfaceName == "player")
                this.toggleUserInterface(InterfaceType.Player);
            if (interfaceName == "vehicle")
                this.toggleUserInterface(InterfaceType.Vehicle);
            if (interfaceName == "pedсustomize")
                this.toggleUserInterface(InterfaceType.PedCustomize);
            if (interfaceName == "window")
                this.toggleUserInterface(InterfaceType.Window);
        };
        this.drawFrame = () => {
            let localPlayer = LocalPlayer.getPlayer();
            if (this.currentInterface != InterfaceType.Player && this.currentInterface != InterfaceType.Vehicle)
                return;
            if (this.currentInterface === InterfaceType.Player && !this.isUserNameSetted) {
                this.setBrowserValueDelayed("username", LocalPlayer.getName());
                this.isUserNameSetted = true;
            }
            if (this.currentInterface === InterfaceType.Vehicle) {
                let vehicle = API.getPlayerVehicle(localPlayer);
                let seat = API.getPlayerVehicleSeat(localPlayer);
                if (seat != VehicleSeat.Driver && this.currentInterface != InterfaceType.Player)
                    return this.toggleUserInterface(InterfaceType.Player);
                let modelId = API.getEntitySyncedData(vehicle, "MODEL_ID");
                let maxSpeed = 200;
                if (typeof modelId == "number") {
                    let modelName = API.getVehicleModelName(modelId);
                    maxSpeed = API.getVehicleMaxSpeed(modelId) * 5.6;
                    this.setBrowserValue("currentVehicleModelName", modelName);
                }
                else {
                    this.setBrowserValue("currentVehicleModelName", "NO_DATA");
                }
                let className = API.getEntitySyncedData(vehicle, "CLASS_NAME");
                if (typeof className == "string") {
                    this.setBrowserValue("currentVehicleModelClass", className);
                }
                else {
                    this.setBrowserValue("currentVehicleModelClass", "NO_DATA");
                }
                let fuelPercent = API.getEntitySyncedData(vehicle, "FUEL_PERCENT");
                if (typeof fuelPercent == "number") {
                    this.setBrowserValue("currentVehicleFuelPercent", fuelPercent);
                }
                let mileage = API.getEntitySyncedData(vehicle, "MILEAGE");
                if (typeof mileage == "number") {
                    this.setBrowserValue("currentVehicleMileage", mileage);
                }
                let speed = this.getVehicleSpeed(vehicle);
                let rpm = API.getVehicleRPM(vehicle) * 10;
                this.setBrowserValue("currentVehicleSpeed", Math.floor(speed));
                this.setBrowserValue("currentVehicleMaxSpeed", maxSpeed);
                this.setBrowserValue("currentVehicleRPM", rpm);
            }
        };
        this.syncCurrentVehicleData = () => {
            let vehicle = API.getPlayerVehicle(LocalPlayer.getPlayer());
            let engineState = API.getEntitySyncedData(vehicle, "ENGINE_STATE");
            let fuelPercent = API.getEntitySyncedData(vehicle, "FUEL_PERCENT");
            let mileage = API.getEntitySyncedData(vehicle, "MILEAGE");
            this.setBrowserValue("currentVehicleFuelPercent", fuelPercent);
            this.setBrowserValue("mileage", mileage);
        };
        this.getVehicleSpeed = (vehicle) => {
            let velocity = API.getEntityVelocity(vehicle);
            let speed = Math.sqrt(velocity.X * velocity.X +
                velocity.Y * velocity.Y +
                velocity.Z * velocity.Z);
            return speed * 3.6;
        };
        this.showCursor = (value) => {
            API.showCursor(value);
        };
        this.setBrowserValue = (key, value) => {
            Scope.cef.setBrowserValue(key, value);
        };
        this.setBrowserValueDelayed = (key, value) => {
            Scope.cef.setBrowserValueWithDelay(key, value);
        };
        this.showLoginForm = () => {
            let rot = new Vector3();
            let pos = new Vector3(-1812.974, 4600.104, 65.62144);
            let targetPos = new Vector3(2611.133, 1793.092, 373.806);
            let loginCamera = API.createCamera(pos, rot);
            let loginEndMovePathcamera = API.createCamera(targetPos, rot);
            this.cef.loadPage("login.html");
            this.cef.show();
            this.showCursor(true);
            API.setActiveCamera(loginCamera);
            API.pointCameraAtPosition(loginCamera, new Vector3());
            API.interpolateCameras(loginCamera, loginEndMovePathcamera, 5000000, false, false);
            API.setHudVisible(false);
        };
        this.showWelcomeShard = () => {
            API.showShard("Добро пожаловать на Oversight Roleplay", 4000);
        };
        this.cef = Scope.cef;
        API.onUpdate.connect(this.drawFrame);
    }
}
class Inventory {
}
Inventory.playerToggleInventory = () => {
    Inventory.syncInventoryItems();
    Scope.cef.call("toggleInventory");
};
Inventory.syncInventoryItems = () => {
    API.triggerServerEvent("playerOpeningInventory");
};
Inventory.useItemSelf = (itemId) => {
    API.triggerServerEvent("playerUsingItemSelf", itemId);
};
Inventory.addObjectToNearbyStoringObjects = (entity) => {
    Scope.cef.call("addObjectToNearbyStoringObjects", entity);
};
var KeyNewState;
(function (KeyNewState) {
    KeyNewState[KeyNewState["Up"] = 0] = "Up";
    KeyNewState[KeyNewState["Down"] = 1] = "Down";
})(KeyNewState || (KeyNewState = {}));
class KeyHandler {
    constructor() {
        this.onKeyDown = (sender, e) => {
            switch (e.KeyCode) {
                case Keys.I:
                    Inventory.playerToggleInventory();
                    break;
                case Keys.E:
                    API.triggerServerEvent("playerPressedKey", "E");
                    break;
                case Keys.Y:
                    API.triggerServerEvent("playerPressedKey", "Y");
                    break;
                case Keys.Q:
                    API.triggerServerEvent("playerPressedKey", "Q");
                    break;
                case Keys.H:
                    API.triggerServerEvent("playerPressedKey", "H");
                    break;
                case Keys.B:
                    LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Down, e);
                    break;
                case Keys.N:
                    LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Down, e);
                    break;
            }
        };
        this.onKeyUp = (sender, e) => {
            switch (e.KeyCode) {
                case Keys.B:
                    LocalVehicle.onVehicleIgnitionButtonPressed(KeyNewState.Up, e);
                    break;
            }
        };
    }
    start() {
        API.onKeyDown.connect(this.onKeyDown);
        API.onKeyUp.connect(this.onKeyUp);
    }
}
class LocalPlayer {
}
LocalPlayer.init = () => {
    LocalPlayer.player = API.getLocalPlayer();
};
LocalPlayer.getSyncedData = (key) => {
    return API.getEntitySyncedData(LocalPlayer.player, key);
};
LocalPlayer.getName = () => {
    if (!LocalPlayer.playerName)
        LocalPlayer.playerName = API.getPlayerName(API.getLocalPlayer());
    return LocalPlayer.playerName;
};
LocalPlayer.getPlayer = () => {
    return API.getLocalPlayer();
};
LocalPlayer.spawnNormally = () => {
    API.setGameplayCameraActive();
};
LocalPlayer.changeCursorState = (newState) => {
    API.showCursor(newState);
};
var EngineState;
(function (EngineState) {
    EngineState[EngineState["off"] = 0] = "off";
    EngineState[EngineState["ignition"] = 1] = "ignition";
    EngineState[EngineState["after_ignition"] = 2] = "after_ignition";
    EngineState[EngineState["starting"] = 3] = "starting";
    EngineState[EngineState["on"] = 4] = "on";
})(EngineState || (EngineState = {}));
class LocalVehicle {
}
LocalVehicle.getVehicle = () => {
    return API.getPlayerVehicle(LocalPlayer.getPlayer());
};
LocalVehicle.getEngineStatus = () => {
    let state = API.getEntitySyncedData(LocalVehicle.getVehicle(), "ENGINE_STATE");
    if (state == "off")
        return EngineState.off;
    if (state == "ignition")
        return EngineState.ignition;
    if (state == "after_ignition")
        return EngineState.after_ignition;
    if (state == "starting")
        return EngineState.starting;
    if (state == "on")
        return EngineState.on;
};
LocalVehicle.changeEngineState = (value) => {
    let currentEngineStatus = LocalVehicle.getEngineStatus();
    if (value === EngineState.ignition) {
        if (currentEngineStatus && currentEngineStatus != EngineState.off)
            return;
        LocalVehicle.updateInterface(EngineState.ignition);
        new DelayedEvent(LocalVehicle.afterIgnition, 3000);
    }
    else {
        LocalVehicle.updateInterface(value);
    }
};
LocalVehicle.afterIgnition = () => {
    if (!API.getPlayerVehicle(LocalPlayer.getPlayer())) {
        LocalVehicle.updateInterface(EngineState.off);
        return;
    }
    LocalVehicle.updateInterface(EngineState.after_ignition);
};
LocalVehicle.startingEngine = () => {
    if (LocalVehicle.getEngineStatus() != EngineState.starting)
        return;
    LocalVehicle.changeEngineState(EngineState.on);
    API.triggerServerEvent("vehicleEngineStateChanged", true);
};
LocalVehicle.updateInterface = (state) => {
    let engineStateString = EngineState[state].toLowerCase();
    Scope.cef.setBrowserValue("currentVehicleEngineState", engineStateString);
    API.setEntitySyncedData(LocalVehicle.getVehicle(), "ENGINE_STATE", engineStateString);
};
LocalVehicle.onVehicleIgnitionButtonPressed = (type, key) => {
    let vehicle = LocalVehicle.getVehicle();
    if (!vehicle)
        return;
    if (API.getPlayerVehicleSeat(LocalPlayer.getPlayer()) != VehicleSeat.Driver)
        return;
    let currentEngineState = LocalVehicle.getEngineStatus();
    if (key.KeyCode === Keys.B) {
        if (currentEngineState == EngineState.off || !currentEngineState)
            LocalVehicle.changeEngineState(EngineState.ignition);
        if (currentEngineState == EngineState.after_ignition && type === KeyNewState.Down) {
            LocalVehicle.changeEngineState(EngineState.starting);
            let event = new DelayedEvent(LocalVehicle.startingEngine, 1200);
            event.setName("startingEngine");
        }
    }
    if (currentEngineState == EngineState.starting && type === KeyNewState.Up) {
        LocalVehicle.changeEngineState(EngineState.after_ignition);
        Scope.eventHandler.removeEventByName("startingEngine");
    }
    if (key.KeyCode === Keys.N) {
        if (currentEngineState === EngineState.on && type === KeyNewState.Down) {
            LocalVehicle.changeEngineState(EngineState.off);
            API.triggerServerEvent("vehicleEngineStateChanged", false);
        }
    }
};
API.onResourceStart.connect(() => {
    Scope.init();
});
class PedCustomizer {
    static setPedCharacter(ent) {
        if (API.isPed(ent) && API.getEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA") === true && (API.getEntityModel(ent) == 1885233650 || API.getEntityModel(ent) == -1667301416)) {
            let componentTshirt = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT");
            let componentTshirtVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 8, componentTshirt, componentTshirtVariation);
            let componentClothes = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES");
            let componentClothesVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 11, componentClothes, componentClothesVariation);
            let componentTorso = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_TORSO");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 3, componentTorso, 0);
            let componentLeg = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG");
            let componentLegVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 4, componentLeg, componentLegVariation);
            let componentFoot = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT");
            let componentFootVariation = API.getEntitySyncedData(ent, "GTAO_PED_COMPONENT_FOOT_VARIATION");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 6, componentLeg, componentLegVariation);
            let shapeFirstId = API.getEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID");
            let shapeSecondId = API.getEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID");
            let skinFirstId = API.getEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID");
            let skinSecondId = API.getEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID");
            let shapeMix = API.f(API.getEntitySyncedData(ent, "GTAO_SHAPE_MIX"));
            let skinMix = API.f(API.getEntitySyncedData(ent, "GTAO_SKIN_MIX"));
            API.callNative("SET_PED_HEAD_BLEND_DATA", ent, shapeFirstId, shapeSecondId, 0, skinFirstId, skinSecondId, 0, shapeMix, skinMix, 0, false);
            let hair = API.getEntitySyncedData(ent, "GTAO_HAIR");
            API.callNative("SET_PED_COMPONENT_VARIATION", ent, 2, hair, 1);
            let hairColor = API.getEntitySyncedData(ent, "GTAO_HAIR_COLOR");
            let highlightColor = API.getEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR");
            API.callNative("_SET_PED_HAIR_COLOR", ent, hairColor, highlightColor);
            let eyeColor = API.getEntitySyncedData(ent, "GTAO_EYE_COLOR");
            API.callNative("_SET_PED_EYE_COLOR", ent, eyeColor);
            let eyebrowsStyle = API.getEntitySyncedData(ent, "GTAO_EYEBROWS");
            let eyebrowsColor = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR");
            let eyebrowsColor2 = API.getEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2");
            API.callNative("SET_PED_HEAD_OVERLAY", ent, 2, eyebrowsStyle, API.f(1));
            API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 2, 1, eyebrowsColor, eyebrowsColor2);
            if (API.hasEntitySyncedData(ent, "GTAO_LIPSTICK")) {
                let lipstick = API.getEntitySyncedData(ent, "GTAO_LIPSTICK");
                let lipstickColor = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR");
                let lipstickColor2 = API.getEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2");
                API.callNative("SET_PED_HEAD_OVERLAY", ent, 8, lipstick, API.f(1));
                API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 8, 2, lipstickColor, lipstickColor2);
            }
            if (API.hasEntitySyncedData(ent, "GTAO_MAKEUP")) {
                let makeup = API.getEntitySyncedData(ent, "GTAO_MAKEUP");
                let makeupColor = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR");
                let makeupColor2 = API.getEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2");
                API.callNative("SET_PED_HEAD_OVERLAY", ent, 4, makeup, API.f(1));
                API.callNative("_SET_PED_HEAD_OVERLAY_COLOR", ent, 4, 0, makeupColor, makeupColor2);
            }
            let faceFeatureList = API.getEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST");
            for (let i = 0; i < 21; i++) {
                API.callNative("_SET_PED_FACE_FEATURE", ent, i, API.f(faceFeatureList[i]));
            }
        }
    }
}
class Peds {
}
class RentalHouse {
    constructor() {
    }
}
RentalHouse.loadDataToBrowser = () => {
    let localPlayer = LocalPlayer.getPlayer();
    Scope.cef.setBrowserValueWithDelay("houseName", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_NAME"));
    Scope.cef.setBrowserValueWithDelay("playersLimit", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_LIMIT"));
    Scope.cef.setBrowserValueWithDelay("playersRentingCount", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENTING_PLAYERS_COUNT"));
    Scope.cef.setBrowserValueWithDelay("rentPrice", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_RENT_PRICE"));
    Scope.cef.setBrowserValueWithDelay("currentPlayerRentingThisHouse", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_IS_LOCAL_PLAYER_RENTING_THIS_HOUSE"));
    Scope.cef.setBrowserValueWithDelay("houseId", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_ID"));
    Scope.cef.setBrowserValueWithDelay("comfortLevel", API.getEntitySyncedData(localPlayer, "RENTAL_HOUSE_COMFORT_LEVEL"));
};
RentalHouse.bindEvents = () => {
    CefHelper.eventHandlers["rentHouse"] = () => {
        API.triggerServerEvent("playerRentingHouse");
    };
};
class Scope {
}
Scope.init = () => {
    Scope.cef = new CefHelper(true);
    Scope.interface = new Interface();
    Scope.windowsFactory = new WindowsFactory();
    LocalPlayer.init();
    Chat.init();
    let eventHandler = new EventHandler();
    eventHandler.start();
    Scope.eventHandler = eventHandler;
    let keyHandler = new KeyHandler();
    keyHandler.start();
    Scope.interface.showLoginForm();
};
class Utils {
}
class BaseWindow {
    constructor() {
    }
    loadUrl(pageUrl) {
        Scope.interface.toggleUserInterface(InterfaceType.Window);
        Scope.cef.loadPage(pageUrl);
        API.showCursor(true);
    }
    close() {
        Scope.interface.toggleUserInterface(InterfaceType.Player);
    }
}
class RentalHouseWindow extends BaseWindow {
    constructor() {
        super();
        this.show = () => {
            let pageUrl = "rentalhouse/econom.html";
            super.loadUrl(pageUrl);
        };
        RentalHouse.loadDataToBrowser();
    }
}
class WindowsFactory {
    constructor() {
        this.getWindow = (windowName) => {
            switch (windowName) {
                case "rentalhouse":
                    return new RentalHouseWindow();
            }
        };
    }
}
//# sourceMappingURL=main.js.map
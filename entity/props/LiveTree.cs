﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using GTANetworkServer;
using oversight.system;
using oversight.entityGroup;
using Object = GTANetworkServer.Object;

namespace oversight.entity.props
{
    class LiveTree : CommonProp
    {
        private ETreeState _currentState = ETreeState.Normally;
        // Время последнего изменения состояния
        private DateTime _stateChangeTime;
        // Здоровье дерева
        private int _health = 100;
        private Player LastDamagedPlayer;

        // Состояние дерева
        public enum ETreeState
        {
            // Просто растёт
            Normally = 0,
            // Упало дерево
            Destroyed = 1,
            // Обрублены сучки, лежит бревно
            Log = 2,
            // Удалено
            Removed = -1
        }

        public LiveTree(GTANetworkServer.Object obj, string group) : base(obj, group)
        {
            _stateChangeTime = DateTime.Now;
            CreateMarker(3);
        }

        public ETreeState GetCurrentState()
        {
            return _currentState;
        }

        /// <summary>
        /// Возрождает дерево
        /// </summary>
        public void Spawn()
        {
            GTANetworkServer.Object obj = Main.Api.createObject(Hash, Position, Rotation);
            ChangeObject(obj);
            _health = 100;
            _stateChangeTime = DateTime.Now;
            _currentState = ETreeState.Normally;
        }

        /// <summary>
        /// Ломает дерево, удаляет объект, ставит объект сломанного дерева
        /// </summary>
        private void ChangeState(ETreeState newState)
        {
            if (_currentState == ETreeState.Removed)
                return;

            Vector3 newPosition = Position;
            Vector3 newRotation = Rotation;
            _health = 100;
            _currentState = newState;
            _stateChangeTime = DateTime.Now;

            if (newState == ETreeState.Destroyed)
            {
                // Дерево падает с рандомным наклоном
                newRotation.Z = new Random().Next(0, 360);
                newPosition.Z += 0.4f;

                ChangeModel(Data.eProp.hei_prop_hei_tree_fallen_02);
                ChangePosition(newPosition);
            }

            if (newState == ETreeState.Log)
            {
                Log log = new Log(Main.Api.createObject((int)Data.eProp.prop_log_02, newPosition, newRotation));
                log.ChangeShapeRadius(5);

                try
                {
                    // Дерево упало в рядом находящуюся кучу, кладем его в неё
                    LogPile nearestLogPile =
                        (LogPile) Props.GetAllProps()
                            .Where(prop => prop is LogPile && Utils.RangeOfTwoVectors(Position, prop.Position) < 6)
                            .OrderByDescending(prop => Utils.RangeOfTwoVectors(Position, prop.Position))
                            .First();

                    if (nearestLogPile != null)
                    {
                        nearestLogPile.PutLog(log, LastDamagedPlayer);
                        log.LogPile = nearestLogPile;
                    }
                    Remove();
                }
                catch (InvalidOperationException)
                {
                    // Рядом нет куч, создаем новую
                    Object logPileObject = Main.Api.createObject((int) Data.eProp.prop_sign_road_09e, Position, new Vector3());
                    LogPile logPile = new LogPile(logPileObject);
                    log.LogPile = logPile;
                    logPile.SetCreator(LastDamagedPlayer);
                    logPile.PutLog(log, LastDamagedPlayer);
                    Remove();
                }
            }
        }

        /// <summary>
        /// Превращает дерево в следующее положение
        /// </summary>
        private void ChangeToNextState()
        {
            switch (GetCurrentState())
            {
                case ETreeState.Normally:
                    ChangeState(ETreeState.Destroyed);
                    break;
                case ETreeState.Destroyed:
                    ChangeState(ETreeState.Log);
                    break;
            }
        }

        public void Damage(int damageAmount, Player fromPlayer)
        {
            int newHealth = _health - damageAmount;
            LastDamagedPlayer = fromPlayer;

            if (_currentState == ETreeState.Removed)
                return;

            if (newHealth <= 0)
            {
                ChangeToNextState();
            }
            else
                _health = newHealth;
        }
    }
}

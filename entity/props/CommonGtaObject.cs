﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;

namespace oversight.entity.props
{
    class CommonGtaObject
    {
        public int Hash;
        public Vector3 Position;

        public CommonGtaObject(int hash, Vector3 position)
        {
            Hash = hash;
            Position = position;
        }
    }
}

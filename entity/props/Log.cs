﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using oversight.entityGroup;
using oversight.system;
using Object = GTANetworkServer.Object;

namespace oversight.entity.props
{
    class Log: CommonProp
    {
        public LogPile LogPile;

        public Log(Object obj) : base(obj, "log")
        {
        }

        public void PlayerTryingAttachLogToVehicle(Player player)
        {
            if (Shape == null)
                return;

            if (!Shape.containsEntity(player.Handler) || Group != "log")
                return;

            NetHandle currentPlayerVehicle = Main.Api.getPlayerVehicle(player.Handler);
            int currentPlayerVehicleModel = Main.Api.getEntityModel(currentPlayerVehicle);

            if (currentPlayerVehicleModel != (int)VehicleHash.Forklift)
            {
                Main.Api.sendNotificationToPlayer(player.Handler, "Вы должны находиться в погрузчике");
                return;
            }

            OsVehicle vehicle = Vehicles.Get(currentPlayerVehicle);
            if (vehicle == null)
                return;

            try
            {
                var count = vehicle.AttachedProps.Count(prop => prop is Log);
                if (count > 0)
                {
                    Main.Api.sendNotificationToPlayer(player.Handler, "Нельзя погрузить более 1 бревна");
                    return;
                }

                vehicle.AttachedProps.Add(this);
                Main.Api.sendNotificationToPlayer(player.Handler, "Нажмите [~b~Y~s~], чтобы скинуть бревно");

                // Цепляем бревно к перевозчику, удаляем его shape
                Main.Api.attachEntityToEntity(Obj, currentPlayerVehicle, "SKEL_ROOT", new Vector3(0, 1.8, 0), new Vector3(0, 0, 100));
                RemoveShape();

                // Если это последнее бревно в куче, то удаляем кучу
                count = LogPile.Logs.Count;
                Main.Api.sendChatMessageToAll("LOGS COUNT: " + count);
                if (count == 1)
                {
                    LogPile.Remove();
                    LogPile = null;
                }
                else
                {
                    LogPile.RemoveLog(this);
                }
            }
            catch (ArgumentNullException)
            {

            }

            catch (NullReferenceException)
            {
                Main.Api.sendChatMessageToAll("NULL");
            }
        }

        public void DettachingLogFromVehicle(Player player)
        {
            try
            {
                NetHandle currentPlayerVehicle = Main.Api.getPlayerVehicle(player.Handler);
                OsVehicle vehicle = Vehicles.Get(currentPlayerVehicle);

                if (vehicle == null)
                    return;

                int count =
                    Props.GetAllProps()
                        .Count(prop => prop is LogPile && Utils.RangeOfTwoVectors(Position, prop.Position) < 6);

                Main.Api.sendChatMessageToAll("COUNT: " + count);
                if (count >= 2)
                {
                    Main.Api.sendNotificationToPlayer(player.Handler,
                        "Вы не можете положить бревно сюда, т.к. рядом более 1 кучи");
                    return;
                }

                // Поблизости есть куча, кидаем бревно в неё
                if (count ==  1)
                {
                    try
                    {
                        LogPile nearestPile =
                            (LogPile) Props.GetAllProps()
                                .Where(prop => prop is LogPile && Utils.RangeOfTwoVectors(Position, prop.Position) < 6)
                                .OrderByDescending(prop => Utils.RangeOfTwoVectors(Position, prop.Position))
                                .First();
                        
                        Props.GetAllProps().Remove(LogPile);
                        LogPile = nearestPile;
                        nearestPile.PutLog(this, player);
                        //Main.Api.detachEntity(Obj);
                        //vehicle.AttachedProps.Remove(this);
                        //ReCreateShape();
                    }
                    catch (ArgumentNullException)
                    {
                        
                    }
                }

                // Поблизости нет куч, создаем новую.
                if (count == 0)
                {
                    Object logPileObject = Main.Api.createObject((int)Data.eProp.prop_sign_road_09e, Position, new Vector3());
                    LogPile logPile = new LogPile(logPileObject);
                    LogPile = logPile;
                    logPile.SetCreator(player);
                    logPile.PutLog(this, player);
                    Main.Api.detachEntity(Obj);
                    vehicle.AttachedProps.Remove(this);
                    ReCreateShape();
                }
            }
            catch (InvalidOperationException)
            {

            }
            catch (ArgumentNullException)
            {

            }
        }
    }
}

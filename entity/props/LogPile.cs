﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.system;
using Object = GTANetworkServer.Object;

namespace oversight.entity.props
{
    class LogPile : CommonProp
    {
        private Player _Creator;

        public Player Creator => _Creator;
        private List<Log> _Logs = new List<Log>();
        private OsVehicle _InVehicle;

        public enum eLogPileDisplayState
        {
            showing, hidden
        }

        public enum eLogPileState
        {
            collecting, collected
        }

        private eLogPileDisplayState _CurrentDisplayState = eLogPileDisplayState.showing;
        private eLogPileState _CurrentState = eLogPileState.collecting;

        const int MAX_LOGS_IN_PILE = 2;

        public LogPile(Object sign) : base(sign, "logPile")
        {
            CreateMarker(6);
            Main.Api.setEntityCollisionless(Obj, true);
        }

        public OsVehicle InVehicle => _InVehicle;

        public List<Log> Logs => _Logs;

        public eLogPileState CurrentState => _CurrentState;

        public void ReCreateMarker()
        {
            CreateMarker(6);
            RemoveMarker();
        }

        /// <summary>
        /// Установить создателя
        /// </summary>
        /// <param name="player"></param>
        public void SetCreator(Player player)
        {
            _Creator = player;
        }

        /// <summary>
        /// Положить бревно в кучу
        /// </summary>
        /// <param name="log"></param>
        /// <param name="byPlayer"></param>
        public void PutLog(Log log, Player byPlayer)
        {
            int logsCount = _Logs.Count;

            if (_Logs.Contains(log))
                return;

            _Logs.Add(log);

            int newValue = logsCount + 1;
            if (newValue >= MAX_LOGS_IN_PILE)
            {
                // Превращаем лежащие рядом деревья в цельную модель, если набралось достаточное количество
                Main.Api.sendNotificationToPlayer(byPlayer.Handler, "Вы собрали кучу, можно грузить на трейлер");
                ChangeModel(Data.eProp.prop_logpile_03);
                RemoveMarker();
                foreach (Log prop in _Logs)
                {
                    prop.Remove();
                }
                _CurrentState = eLogPileState.collected;
                CreateShape(4);
            }
            else
            {
                // Кладём добавленное в кучу бревно рядом
                Main.Api.sendNotificationToPlayer(byPlayer.Handler, string.Format("Бревен в куче: \n [{0}/~g~{1}~s~]", newValue, MAX_LOGS_IN_PILE));
            }
        }

        public new void ChangePosition(Vector3 newPosition)
        {
            Main.Api.setEntityPosition(Obj, newPosition);
            ReCreateShape();
            ReCreateMarker();
        }

        public void RemoveLog(Log log)
        {
            if (!_Logs.Contains(log))
                return;
            _Logs.Remove(log);
        }

        public new void Remove() 
        {
            RemoveShape();
            RemoveMarker();
            // Props.GetAllProps().Remove(this);
            base.Remove();
        }
    }
}

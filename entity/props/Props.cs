﻿using GTANetworkServer;
using GTANetworkShared;
using Microsoft.CSharp.RuntimeBinder;
using MySql.Data.MySqlClient;
using oversight.entityGroup;
using oversight.system;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.ClientServices.Providers;
using System.Windows.Forms;
using static oversight.entity.props.LiveTree;
using static oversight.system.Data;

namespace oversight.entity.props
{
    class Props
    {
        private static List<CommonProp> _props = new List<CommonProp>();
        private static List<CommonGtaObject> ObjectsToDelete = new List<CommonGtaObject>();

        public static void Add(dynamic prop)
        {
            try
            {
                _props.Add(prop);
            } catch (RuntimeBinderException e)
            {
                Main.Api.consoleOutput("[SEVERE] " + e.Message.ToString());
            }
        }

        public static List<CommonProp> GetAllProps()
        {
            return _props;
        }

        public static CommonProp Get(GTANetworkServer.Object obj)
        {
            return _props.Find(p => p.Obj.handle.Value == obj.handle.Value);
        }

        public static void LoadProps()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM `mapping`", DbConnection.GetInstance().Connection);
            command.Prepare();

            System.Data.Common.DbDataReader reader = command.ExecuteReader();

            int i = 0;
            int b = 0;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> data = DbConnection.GetRowData(reader);
                    string group = (string) data.Get("group");
                    try
                    {
                        Type t = Type.GetType("oversight.entity.props." + group);

                        int hash = (int)data.Get("hash");
                        double posX = (double)data.Get("pos_x");
                        double posY = (double)data.Get("pos_y");
                        double posZ = (double)data.Get("pos_z");
                        double rotX = (double)data.Get("rot_x");
                        double rotY = (double)data.Get("rot_y");
                        double rotZ = (double)data.Get("rot_z");

                        GTANetworkServer.Object obj = Main.Api.createObject(hash, new Vector3(posX, posY, posZ), new Vector3(rotX, rotY, rotZ));

                        if (group != "common")
                        {
                            if (t != null)
                            {
                                var prop = Activator.CreateInstance(t, obj, group);
                            }
                        }
                        else 
                        {
                            CommonProp prop = new CommonProp(obj, group);
                        }
                        i++;
                    } catch (ArgumentNullException)
                    {
                        b++;
                    }
                }
                reader.Close();
            }

            if (b > 0)
                Main.Api.consoleOutput($"[SERVER] {b} props WAS NOT loaded due to group error");

            Main.Api.consoleOutput($"[SERVER] {i} props loaded");
        }

        public static void onEntityEnterColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.entered);
        }

        public static void onEntityExitColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.exit);
        }

        // Проверяем, вошел ли игрок в радиус взаимодействия с объектом
        private static void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, Data.eMoveType type)
        {
            if (Main.Api.getEntityType(entity) != EntityType.Player)
                return;

            Client user = Main.Api.getPlayerFromHandle(entity);
            if (user == null)
                return;

            Player player = Players.Get(user);
            // Игрок отошел от дерева
            if (type == Data.eMoveType.exit && player.TmpData.NearestLiveTree != null)
            {
                player.TmpData.NearestLiveTree = null;
            }

            if (type == Data.eMoveType.exit && player.TmpData.NearestCommonProp != null)
            {
                player.TmpData.NearestCommonProp = null;
            }

            CommonProp prop = _props.Find(p => p.Shape?.containsEntity(entity) ?? false);
            if (prop == null)
                return;

            player.TmpData.NearestCommonProp = prop;
            // Игрок подошел к дереву
            if (type == eMoveType.entered && prop is LiveTree)
            {
                LiveTree tree = (LiveTree) prop;
                player.TmpData.NearestLiveTree = tree;

                if (player.CurrentWork == eWorks.LumberJack && tree.GetCurrentState() != ETreeState.Removed)
                {
                    Main.Api.sendNotificationToPlayer(user, "Нажмите [~b~Q~s~], чтобы рубить дерево");
                }
            }

            // Игрок подошел к куче бревен
            else if (type == eMoveType.entered && prop is LogPile)
            {
                LogPile pile = (LogPile) prop;
                if (pile.CurrentState != LogPile.eLogPileState.collected)
                {
                    Main.Api.sendNotificationToPlayer(user, "Приносите бревна к куче, чтобы собрать её");
                    return;
                }
                Main.Api.sendNotificationToPlayer(user, "Нажмите [~b~H~s~], чтобы поместить бревна на трейлер");
            }

            else if(type == eMoveType.entered && prop is Log)
            {
                if (Main.Api.isEntityAttachedToAnything(prop.Obj))
                    return;

                int model = Main.Api.getEntityModel(Main.Api.getPlayerVehicle(user));
                if (model != (int) VehicleHash.Forklift)
                    return;

                Main.Api.sendNotificationToPlayer(user, "Нажмите [~b~E~s~], чтобы поместить бревно на перевозчик");
            }
        }

        /// <summary>
        /// Проверяет, не ударил ли игрок дерево нажатием на клавишу
        /// </summary>
        /// <param name="player"></param>
        public static void CheckPlayerCuttingTree(Player player)
        {
            DateTime nowTime = DateTime.Now;
            TimeSpan span = nowTime - player.TmpData.LastTreeCuttingCheck;

            // Если игрок стоит у дерева
            if (player.TmpData.NearestLiveTree == null || player.TmpData.NearestLiveTree.GetCurrentState() == ETreeState.Removed)
                return;

            // Время, прошедшее с предыдущего удара
            double duration = span.TotalMilliseconds;

            // Запрещаем бить чаще, чем раз в 1300мс
            if (duration <= 1300)
                return;

            if (Main.Api.getPlayerCurrentWeapon(player.Handler) != WeaponHash.Hatchet)
            {
                Main.Api.sendNotificationToPlayer(player.Handler, "Достаньте топор, чтобы рубить дерево");
                return;
            }

            // Проигрываем анимацию удара
            Main.Api.stopPlayerAnimation(player.Handler);
            Main.Api.playPlayerAnimation(player.Handler, 0, "melee@large_wpn@streamed_core_fps", "car_down_attack");

            // Наносим рандомный урон по дереву от 3 до 6
            int damage = new Random().Next(100, 100);
            player.TmpData.NearestLiveTree.Damage(damage, player);

            // Записываем время удара
            player.TmpData.LastTreeCuttingCheck = DateTime.Now;
        }

        public static void CheckPlayerLoadingLogPileToTrailer(Player player)
        {
            if (player.TmpData.NearestCommonProp != null && player.TmpData.NearestCommonProp is LogPile)
            {
                List<OsVehicle> nearestVehicles = Vehicles.GetNearestVehiclesToEntity(player.Handler, 14);
                try
                {
                    OsVehicle nearestTrailer =
                        nearestVehicles.First(v => Main.Api.getEntityModel(v.Handler) == (int) VehicleHash.TRFlat);
                    Main.Api.sendChatMessageToAll(nearestTrailer.Id.ToString());

                    Main.Api.stopPlayerAnimation(player.Handler);
                    Main.Api.playPlayerAnimation(player.Handler, 0, "mini@repair", "fixing_a_ped");
                    Main.Api.freezePlayer(player.Handler, true);

                    Action action = () => PlayerFinishedLoadingLogPileToTrailer(player);
                    action.DelayFor(11000);
                }
                catch (InvalidOperationException)
                {
                    Main.Api.sendNotificationToPlayer(player.Handler, "Поблизости нет трейлера для загрузки бревен в него");
                }
            }
        }

        /// <summary>
        /// Игрок закончил погрузку кучи бревен в трейлер
        /// </summary>
        /// <param name="player"></param>
        private static void PlayerFinishedLoadingLogPileToTrailer(Player player)
        {
            Main.Api.freezePlayer(player.Handler, false);
            if (player.TmpData.NearestCommonProp != null && player.TmpData.NearestCommonProp is LogPile)
            {
                List<OsVehicle> nearestVehicles = Vehicles.GetNearestVehiclesToEntity(player.Handler, 14);
                try
                {
                    OsVehicle nearestTrailer =
                        nearestVehicles.First(v => Main.Api.getEntityModel(v.Handler) == (int)VehicleHash.TRFlat);
                    OsVehicle nearestVehicle = nearestVehicles.First(v => v.GetModel() == (int)VehicleHash.Packer);

                    nearestTrailer.ChangeModel(VehicleHash.TrailerLogs);
                    nearestVehicle?.AttachTrailer(nearestTrailer);

                    player.TmpData.NearestCommonProp.Remove();
                    player.TmpData.NearestCommonProp = null;
                }
                catch (InvalidOperationException)
                {
                    Main.Api.sendNotificationToPlayer(player.Handler, "Поблизости нет трейлера и прицепа для загрузки бревен в него");
                }
            }
        }

        /// <summary>
        /// Игрок пытается загрузить бревно в погрузчик
        /// </summary>
        /// <param name="player"></param>
        public static void CheckPlayerTryingAttachLogToVehicle(Player player)
        {
            List<CommonProp> logs = GetAllProps().Where(prop => prop is Log).ToList();
            if (logs.Count == 0)
                return;

            foreach (Log log in logs)
            {
                log.PlayerTryingAttachLogToVehicle(player);
            }
        }

        /// <summary>
        /// Игрок пытается отцепить бревно от погрузчика
        /// </summary>
        /// <param name="player"></param>
        public static void CheckPlayerTryingDettachLogFromVehicle(Player player)
        {
            NetHandle currentVehicle = Main.Api.getPlayerVehicle(player.Handler);
            if (currentVehicle == null)
                return;

            try
            {
                Log attachedLog =
                    (Log) GetAllProps().First(prop => prop is Log && Main.Api.isEntityAttachedToAnything(prop.Obj));
                attachedLog.DettachingLogFromVehicle(player);
            }
            catch (InvalidOperationException)
            {
                
            }
        }

        /// <summary>
        /// Поток занимается удалением стандартных объектов GTA 5 для игроков.
        /// </summary>
        public static void StartDeletingCommonGtaObjectsThread()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM `mapping_deleted_objects`", DbConnection.GetInstance().Connection);
            command.Prepare();

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                reader.Close();
                return;
            }

            while (reader.Read())
            {
                var data = DbConnection.GetRowData(reader);
                int objectHash = (int)data.Get("hash");
                Vector3 objectPosition = new Vector3((double) data.Get("pos_x"), (double)data.Get("pos_y"), (double)data.Get("pos_z"));
                CommonGtaObject objectToDelete = new CommonGtaObject(objectHash, objectPosition);
                ObjectsToDelete.Add(objectToDelete);
            }
            reader.Close();
            Main.Api.consoleOutput(string.Format("[SERVER] {0} objects for deleting", ObjectsToDelete.Count));

            new Thread(() =>
            {
                while (true)
                {
                    List<Client> users = Main.Api.getAllPlayers();
                    try
                    {
                        foreach (CommonGtaObject obj in ObjectsToDelete)
                        {
                            // Игроки, близкие к объекту для удаления
                            List<Client> playersNearToObject =
                                users.Where(
                                        p => Utils.RangeOfTwoVectors(Main.Api.getEntityPosition(p), obj.Position) <= 500)
                                    .ToList();

                            // Игроки, для которых объект был удален, но вышедшие из радиуса 500
                            List<Player> playersNeedsToRefreshTheyDeletedObjectsList =
                                Players.GetAllPlayers()
                                    .Where(
                                        p =>
                                            Utils.RangeOfTwoVectors(Main.Api.getEntityPosition(p.Handler), obj.Position) >
                                            500 && p.DeletedCommonGtaObjects.Contains(obj))
                                    .ToList();

                            // Для игрока, который вышел из радиуса 500 нужно будет заново переудалить объект, при вхождении в радиус. Поэтому, удаляем запись.
                            foreach (Player player in playersNeedsToRefreshTheyDeletedObjectsList)
                            {
                                player.DeletedCommonGtaObjects.Remove(obj);
                            }

                            // Удаляем объект для игрока, записываем его список удаленных объектов у игрока, чтобы не посылать команду удаления на следующей итерации
                            foreach (Client user in playersNearToObject)
                            {
                                Player player = Players.Get(user);
                                if (player.DeletedCommonGtaObjects.Contains(obj))
                                    continue;

                                Main.Api.deleteObject(user, obj.Position, obj.Hash);
                                // player.DeletedCommonGtaObjects.Add(obj);
                            }
                        }
                    }
                    catch (ArgumentNullException)
                    {
                    }
                    Thread.Sleep(100);
                }
            }).Start();
        }
    }
}

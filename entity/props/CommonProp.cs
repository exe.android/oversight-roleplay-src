﻿using GTANetworkShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using oversight.entityGroup;
using oversight.system;
using Object = GTANetworkServer.Object;

namespace oversight.entity.props
{
    class CommonProp
    {
        protected Vector3 Pos;
        protected Vector3 Rot;
        protected int _Hash;
        protected int _Dimension;
        protected GTANetworkServer.Object _Obj;
        protected CylinderColShape _Shape;
        protected string _Name;
        protected string _Group;
        protected Marker _Marker;

        public CommonProp(GTANetworkServer.Object obj, string group)
        {
            _Obj = obj;
            Pos = Main.Api.getEntityPosition(obj);
            Rot = Main.Api.getEntityRotation(obj);
            _Hash = Main.Api.getEntityModel(obj);
            _Group = group;

            Props.Add(this);
            CreateShape();
        }

        public GTANetworkServer.Object Obj
        {
            get
            {
                return _Obj;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
        }

        public string Group
        {
            get
            {
                return _Group;
            }
        }

        public CylinderColShape Shape
        {
            get
            {
                return _Shape;
            }
        }

        public int Hash
        {
            get
            {
                return _Hash;
            }
        }

        public int Dimension
        {
            get
            {
                return _Dimension;
            }
        }

        public Vector3 Position
        {
            get
            {
                Vector3 position = Main.Api.getEntityPosition(Obj);
                return position;
            }
        }

        public Vector3 Rotation
        {
            get
            {
                return Rot;
            }
        }

        protected void CreateShape()
        {
            _Shape = Main.Api.createCylinderColShape(Position, 3, 5);
        }

        protected void CreateShape(float radius)
        {
            _Shape = Main.Api.createCylinderColShape(Position, radius, 5);
        }

        protected void RemoveShape()
        {
            Main.Api.deleteColShape(Shape);
            _Shape = null;
        }

        protected void ReCreateShape()
        {
            RemoveShape();
            CreateShape();
        }

        protected void CreateMarker(double radius)
        {
            _Marker = Main.Api.createMarker(1, Position, new Vector3(), new Vector3(), new Vector3(radius, radius, radius), 50, 130, 130, 50);
        }

        protected void RemoveMarker()
        {
            if (_Marker != null)
                Main.Api.deleteEntity(_Marker);
        }

        public void ChangeObject(Object newObject)
        {
            if (Obj != null)
                Obj.delete();
            _Obj = newObject;

            if (newObject != null)
            {
                if (Shape == null)
                    CreateShape();
            }
        }

        public void ChangeModel(Data.eProp hash)
        {
            Vector3 currentPosition = Main.Api.getEntityPosition(Obj);
            Vector3 currentRotation = Main.Api.getEntityRotation(Obj);

            Object obj = Main.Api.createObject((int) hash, currentPosition, currentRotation);
            Remove();
            Props.Add(this);
            _Obj = obj;
        }

        public void ChangeGroup(string newGroup)
        {
            _Group = newGroup;
        }

        public void ChangePosition(Vector3 newPosition)
        {
            Main.Api.setEntityPosition(Obj, newPosition);
            ReCreateShape();
        }

        public void ChangeShapeRadius(int newRadius)
        {
            _Shape.Range = newRadius;
        }

        public void Remove()
        {
            Main.Api.deleteEntity(Obj);
            RemoveShape();
            RemoveMarker();
            Props.GetAllProps().Remove(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.entityGroup;
using static oversight.system.Data;

namespace oversight.entity.business
{
    class Logging: CommonBusiness
    {
        public Logging(int id, string name) : base(id, name)
        {
            
        }

        public override void OnMarkersLoaded()
        {
            CreateBlipOnMarker("logging_enter");
        }

        public override void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, eMoveType type)
        {
            EntityType entityType = Main.Api.getEntityType(entity);
            if (entityType != EntityType.Player)
                return;

            if (type != eMoveType.entered)
                return;

            string shapeName = GetShapeName(shape);
            if (shapeName == null)
                return;

            Client user = Main.Api.getPlayerFromHandle(entity);
            Player player = Players.Get(user);

            // Игрок встает на точку, по которой можно устроиться на работу лесоруба.
            if (shapeName == "logging_enter")
            {
                player.ShowWindow("business", "logging");
                player.TmpData.LastBusinessId = Id;
            }

            player.TmpData.LastColShapeName = shapeName;
        }

        public override void OnPlayerHiringToJob(Player player, string jobName)
        {
            if (player.TmpData.LastBusinessId != Id && jobName != "lumberJack")
                return;

            Main.Api.sendNotificationToPlayer(player.Handler, "Вы устроены ~b~лесорубом");
            player.ToggleInterface(eInterfaceType.Player);
        }
    }
}

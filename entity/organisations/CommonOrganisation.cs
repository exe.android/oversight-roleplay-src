﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using oversight.entityGroup;
using oversight.interfaces;
using oversight.system;

namespace oversight.entity.organisation
{
    delegate void PlayerHiringToJobEvent(Player player, string jobName);

    class CommonOrganisation: Entity
    {
        protected Dictionary<string, CylinderColShape> _Shapes = new Dictionary<string, CylinderColShape>();
        public Dictionary<string, CylinderColShape> Shapes => _Shapes;

        protected Dictionary<string, Marker>_Markers = new Dictionary<string, Marker>();
        public Dictionary<string, Marker> Markers => _Markers;

        protected string _Name;
        public string Name => _Name;

        protected CommonOrganisation(int id, string name)
        {
            _Id = id;
            _Name = name;
            
            Main.Api.onEntityEnterColShape += onEntityEnterColShapeHandler;
            Main.Api.onEntityExitColShape += onEntityExitColShapeHandler;
            Main.OnPlayerHiringToJob += OnPlayerHiringToJob;
            Businesses.OnBusinesessLoaded += LoadBusiness;
        }

        private void LoadBusiness()
        {
            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `bank_cards` WHERE `owner_type` = 'business' AND `owner_id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@id", Id);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                var data = DbConnection.GetRowData(reader);
                int accountId = (int) data.Get("id");
                SetBankAccountId(accountId);
            }
            reader.Close();
        }

        public void onEntityEnterColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.entered);
        }

        public void onEntityExitColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.exit);
        }

        public virtual void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, Data.eMoveType type)
        {
            EntityType entityType = Main.Api.getEntityType(entity);
            if (entityType != EntityType.Player)
                return;

            // Main.Api.sendNotificationToPlayer(Main.Api.getPlayerFromHandle(entity), "WELCOME TO BUSINESS");

            //if (!_Shape.containsEntity(Main.Api.getPlayerFromHandle(entity)))
            //  return;

            //if (type == Data.eMoveType.entered)
            //Main.Api.triggerClientEvent(Main.Api.getPlayerFromHandle(entity), "showLumberMenu");
        }

        protected void CreateBlipOnMarker(string markerName)
        {
            Marker marker = Markers.Get(markerName);
            if (marker == null)
                return;
            NetHandle blip = Main.Api.createBlip(marker.position);
            Main.Api.setBlipSprite(blip, (int)Data.Blip.Dollar);
            Main.Api.setBlipColor(blip, 38);
        }

        public virtual void OnMarkersLoaded()
        {
            
        }

        /// <summary>
        /// Возвращает имя пикапа по его хандлеру
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        public string GetShapeName(ColShape shape)
        {
            try
            {
                return _Shapes.First(s => s.Value.handle == shape.handle).Key;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public virtual void OnPlayerHiringToJob(Player player, string jobName)
        {
            
        }
    }
}

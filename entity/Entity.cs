﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity.bank;

namespace oversight.entity
{
    class Entity
    {
        private int BankAccountId;
        public int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public void SetBankAccountId(int id)
        {
            BankAccountId = id;
        }

        public int GetBankAccountId()
        {
            return BankAccountId;
        }

        public int GetBankMoney()
        {
            if (BankAccountId == null)
                return 0;
            return BankAccount.GetBalance(BankAccountId);
        }
    }
}

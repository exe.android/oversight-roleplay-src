﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using MongoDB.Bson;
using MySql.Data.MySqlClient;
using oversight.entity.bank;
using oversight.entity.houses;
using oversight.system;
using oversight.entity.props;
using oversight.entityGroup;
using oversight.interfaces;
using oversight.inventory;
using oversight.itemstorage;
using static oversight.system.Data;

namespace oversight.entity
{
    struct TmpData
    {
        public LiveTree NearestLiveTree;
        public DateTime LastAttackCheck;
        public DateTime LastTreeCuttingCheck;
        public CommonProp NearestCommonProp;
        public string LastColShapeName;
        public int LastBusinessId;
    }

    class Player: Entity, IStoringObject, IInteractiveEntity
    {
        // Указатель на сетевое управление игрока
        private Client _Handler;

        // Точка спауна
        private Vector3 _SpawnPos;

        // Поскорее избавиться
        public TmpData TmpData;

        // Список удаленных объектов GTA 5 для игрока
        public List<CommonGtaObject> DeletedCommonGtaObjects = new List<CommonGtaObject>();

        // ID банковского счёта игрока
        private int BankAccountId;

        // Энергия игрока
        private int _Energy;

        private eWorks _CurrentWork;

        public Client Handler => _Handler;

        public eWorks CurrentWork => _CurrentWork;

        public Vector3 SpawnPos => _SpawnPos;

        public int Energy => _Energy;

        public Player(Client player)
        {
            _Handler = player;
            TmpData.LastAttackCheck = DateTime.Now;
            TmpData.LastTreeCuttingCheck = DateTime.Now; 

            SetUpPresentation();
        }

        public void SetSpawnPosition(Vector3 position)
        {
            _SpawnPos = position;
        }

        public void SetCurrentWork(eWorks newWork)
        {
            _CurrentWork = newWork;
        }

        public string GetName()
        {
            return Main.Api.getPlayerName(Handler);
        }

        /// <summary>
        /// Спаунит игрока
        /// </summary>
        public void Spawn()
        {
            // Если у игрока не выбрана внешность
            if (!HasCustomize())
            {
                SpawnToCustomize();
                //main.api.freezePlayer(Handler, true);
            }
            // Если выбрана, спавним как обычно
            else
            {
                SpawnNormally();
            }
        }


        /// <summary>
        /// Спаунит игрока в доме
        /// </summary>
        private void SpawnNormally()
        {
            // Если игрок имеет или арендует дом, в котором спаунится, то спауним там.
            House playerHouse = Houses.GetPlayerSpawningHouse(this);
            if (playerHouse != null)
            {
                Vector3 enterHousePosition = playerHouse.EnterPosition;
                Main.Api.setEntityPosition(Handler, enterHousePosition);
            }
            else
            {
                // Если игрок нигде не спаунится, то спауним по координатам из базы
                Main.Api.setEntityPosition(Handler, SpawnPos);
            }

            LoadCharacterCustomize();
            Main.Api.triggerClientEvent(Handler, "spawnNormally");
            Main.Api.freezePlayer(Handler, false);
            Main.Api.setEntityCollisionless(Handler, false);
            Main.Api.setEntityInvincible(Handler, false);
            Main.Api.setEntityTransparency(Handler, 255);
        }

        /// <summary>
        /// Шлёт игрока на кастомизацию персонажа
        /// </summary>
        private void SpawnToCustomize()
        {
            Main.Api.triggerClientEvent(Handler, "startCustomizing");
            PedCustomizer.InitializePedComponents(this);
            PedCustomizer.UpdatePlayerComponents(this);

            Main.Api.setEntityPosition(Handler, new Vector3(-1088.719, 4370.354, 13.052));
            Main.Api.setEntityRotation(Handler, new Vector3(-6.14319, 1.073381, 97.53278));
            Main.Api.setEntityPositionFrozen(Handler, true);
            Main.Api.stopPlayerAnimation(Handler);

            Main.Api.setEntityDimension(Handler, 1000 + Id);
        }

        
        private void LoadCharacterCustomize()
        {
            PedCustomizer.InitializePedComponents(this);
            PedCustomizer.UpdatePlayerComponents(this);
            PedCustomizer.LoadPed(this);
        }

        /// <summary>
        /// Кастомиризовал ли игрок свою внешность
        /// </summary>
        /// <returns></returns>
        private bool HasCustomize()
        {
            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `users_customize` WHERE `player_id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@id", Id);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            bool result = reader.HasRows;
            reader.Close();
            return result;
        }

        /// <summary>
        /// Сохраняет всё связанное с игроком в базу данных
        /// </summary>
        public void SavePlayer()
        {

        }

        /// <summary>
        /// Загружает HTML с заданным типом и именем.
        /// </summary>
        /// <param name="windowType"></param>
        /// <param name="windowName"></param>
        public void ShowWindow(string windowType, string windowName)
        {
            Main.Api.triggerClientEvent(Handler, "showWindow", windowType, windowName);
        }

        /// <summary>
        /// Переключает HUD игрока
        /// </summary>
        /// <param name="interfaceType"></param>
        public void ToggleInterface(eInterfaceType interfaceType)
        {
            Main.Api.triggerClientEvent(Handler, "toggleInterface", Enum.GetName(typeof(eInterfaceType), interfaceType));
        }

        /// <summary>
        /// Устанавливает Synced data
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetSyncedData(string key, dynamic value)
        {
            Main.Api.setEntitySyncedData(Handler, key, value);
        }

        /// <summary>
        /// Отдает Synced data
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public dynamic GetSyncedData(string key)
        {
            return Main.Api.getEntitySyncedData(Handler, key);
        }

        /// <summary>
        /// Отнимает у игрока энергию
        /// </summary>
        /// <param name="value"></param>
        public void RemoveEnergy(int value)
        {
            int newValue = Energy - value;
            SetEnergy(newValue);
        }

        /// <summary>
        /// Прибавляет игроку энергии
        /// </summary>
        /// <param name="value"></param>
        public void AddEnergy(int value)
        {
            int newValue = Energy + value;
            SetEnergy(newValue);
            Main.Api.sendNotificationToPlayer(Handler, string.Format("~g~Персонаж\r~w~Ваш уровень энергии повышен на {0}", value));
        }

        /// <summary>
        /// Приватный метод, устанавливает игроку количество энергии и синхронизирует с клиентом
        /// </summary>
        /// <param name="value"></param>
        private void SetEnergy(int value)
        {
            int newValue = value;
            if (value < 0)
                newValue = 0;
            if (value > 1000)
                newValue = 1000;
            _Energy = newValue;
            SetSyncedData("PLAYER_ENERGY", newValue);
        }

        /// <summary>
        /// Синхронизирует инвентарь на клиенте с хранимой коллекцией предметов игрока
        /// </summary>
        public void SyncPlayerInventory()
        {
            List<Item> playerItems = ItemsCollection.GetItems(eStorageOwnerType.player, Id);
            string json = Main.Api.toJson(playerItems);
            Main.Api.triggerClientEvent(Handler, "loadInventoryItems", json);
        }

        public bool GetIsItemCanBeCarried(Item item)
        {
            return true;
        }

        public int GetCarryingCapacity()
        {
            return 100;
        }

        public void SetUpPresentation()
        {
            SetSyncedData("STORING_OBJECT_TYPE", "player");
            SetSyncedData("STORING_OBJECT_NAME", Main.Api.getPlayerName(Handler));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using GTANetworkServer;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using oversight.entity.bank;
using oversight.entityGroup;
using oversight.exceptions;
using oversight.system;

namespace oversight.entity
{
    class RentalHouse: House
    {
        private List<int> _RentingPlayers = new List<int>();
        public List<int> RentingPlayers => _RentingPlayers;

        private int _RentingPlayersLimit;
        public int RentingPlayersLimit => _RentingPlayersLimit;

        private string _Name;
        public string Name => _Name;

        private int _RentPrice;
        public int RentPrice => _RentPrice;

        public RentalHouse(int houseId, Vector3 enterPosition) : base(houseId, enterPosition)
        {
            SetBlipColor(60);
            Main.OnPlayerRentingHouse += OnPlayerRentingHouse;
        }

        /// <summary>
        /// Игрок арендует здание
        /// </summary>
        /// <param name="player"></param>
        public void OnPlayerRentingHouse(Player player)
        {
            int houseId = (int)player.GetSyncedData("RENTAL_HOUSE_ID");
            if (houseId != this.HouseId)
                return;

            try
            {
                AddNewRentingPlayer(player.Id);
                Main.Api.sendNotificationToPlayer(player.Handler,
                    string.Format("Вы арендовали это жилье за ${0} в час", RentPrice));
                player.ToggleInterface(Data.eInterfaceType.Player);
            }
            catch (RentalHouseException e)
            {
                Main.Api.sendNotificationToPlayer(player.Handler, string.Format("~r~Ошибка:\r{0}", e.Message.ToString()));
            }
        }

        /// <summary>
        /// После того, как все дома подгружены, reader закрыт, можем грузить инфу о здании для аренды.
        /// </summary>
        public override void OnHousesLoaded()
        {
            LoadRentalHouseData();
        }

        /// <summary>
        /// Загружает информацию об арендуемом здании
        /// </summary>
        private void LoadRentalHouseData()
        {
            MySqlCommand command = new MySqlCommand("SELECT hrd.*, IFNULL(GROUP_CONCAT(hrp.player_id SEPARATOR ','), '') renting_players FROM houses_rental_data hrd LEFT JOIN houses_rental_players hrp ON hrd.house_id = hrp.house_id WHERE hrd.house_id = @house_id", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("house_id", HouseId);

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            try
            {
                reader.Read();
                var data = DbConnection.GetRowData(reader);

                string name = (string) data.Get("name");
                _Name = name;

                int playersLimit = (int) data.Get("players_limit");
                _RentingPlayersLimit = playersLimit;

                int rentPrice = (int) data.Get("rent_price");
                _RentPrice = rentPrice;

                string rentingPlayers = (string) data.Get("renting_players");
                if (rentingPlayers != "")
                {
                    const char delimiter = ',';
                    _RentingPlayers = Array.ConvertAll(rentingPlayers.Split(delimiter), int.Parse).ToList();
                }
            }
            catch (System.InvalidCastException e)
            {
                
            }
            
            reader.Close();
        }

        /// <summary>
        /// Игрок встает или уходит с пикапа
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="entity"></param>
        /// <param name="type"></param>
        public override void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, Data.eMoveType type)
        {
            EntityType entityType = Main.Api.getEntityType(entity);
            if (entityType != EntityType.Player)
                return;

            if (!Shape.containsEntity(entity))
                return;

            Client user = Main.Api.getPlayerFromHandle(entity);
            Player player = Players.Get(user);
            UpdateSyncedDataForPlayer(player);
        }

        /// <summary>
        /// Сохраняет информацию о себе в synced data, для того, чтобы можно было отобразить её игроку.
        /// </summary>
        /// <param name="player"></param>
        public override void UpdateSyncedDataForPlayer(Player player)
        {
            player.SetSyncedData("RENTAL_HOUSE_NAME", Name);
            player.SetSyncedData("RENTAL_HOUSE_ID", HouseId);
            player.SetSyncedData("RENTAL_HOUSE_RENTING_PLAYERS_COUNT", RentingPlayers.Count);
            player.SetSyncedData("RENTAL_HOUSE_RENTING_PLAYERS_LIMIT", RentingPlayersLimit);
            player.SetSyncedData("RENTAL_HOUSE_IS_LOCAL_PLAYER_RENTING_THIS_HOUSE", RentingPlayers.Contains(player.Id));
            player.SetSyncedData("RENTAL_HOUSE_RENT_PRICE", RentPrice);
            player.SetSyncedData("RENTAL_HOUSE_COMFORT_LEVEL", ComfortLevel);
            player.ShowWindow("rentalhouse", "econom");
        }

        /// <summary>
        /// Отдает ID банковского счёта владельца
        /// </summary>
        /// <returns></returns>
        public int GetOwnerBankAccountId()
        {
            int ownerAccountId = BankAccount.GetAccountId(OwnerId, OwnerType);
            return ownerAccountId;
        }

        /// <summary>
        /// Имеет ли здание свободные места для аренды
        /// </summary>
        /// <returns></returns>
        private bool IsHouseHasFreeRentPlaces()
        {
            return RentingPlayers.Count < RentingPlayersLimit;
        }

        /// <summary>
        /// Сохраняет поселившегося игрока в базу данных
        /// <exception cref="RentalHouseException">При любой возникшей логической ошибке</exception>
        /// </summary>
        public void AddNewRentingPlayer(int playerId)
        {
            if (Houses.GetPlayerOwningHouses(playerId).Count > 0)
            {
                throw new RentalHouseException("Вы уже владеете жильем!");
            }

            if (Houses.GetPlayerRentingHouse(playerId) != null)
            {
                throw new RentalHouseException("Вы уже снимаете жилье!");
            }

            if (!IsHouseHasFreeRentPlaces())
            {
                throw new RentalHouseException("Нет свободных мест для аренды этого жилья");
            }

            try
            {
                var ownerAccountId = GetOwnerBankAccountId();
                var rentingPlayerAccountId = BankAccount.GetAccountId(playerId, eEntityType.player);
                BankTransaction transaction = new BankTransaction(rentingPlayerAccountId, ownerAccountId, RentPrice,
                    Data.GlobalTax, "Аренда жилья");
                transaction.Execute();
                
                RentingPlayers.Add(playerId);

                MySqlCommand command =
                    new MySqlCommand("INSERT INTO `houses_rental_players` (`id`, `house_id`, `player_id`, `since`) " +
                                     "VALUES (NULL, @house_id, @player_id, NOW())",
                        DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("@house_id", HouseId);
                command.Parameters.AddWithValue("@player_id", playerId);
                command.ExecuteNonQuery();
            }
            catch (NullReferenceException)
            {
            }
            catch (BankTransactionException e)
            {
                throw new RentalHouseException(e.Message);
            }
            catch (BankAccountException e)
            {
                throw new RentalHouseException(e.Message);
            }
            catch (MySqlException)
            {
                
            }
        }

        /// <summary>
        /// Удаляет игрока из списка арендаторов
        /// </summary>
        /// <exception cref="RentalHouseException">Если игрок не арендует помещение</exception>
        /// <param name="playerId"></param>
        public void MoveOutRentingPlayer(int playerId)
        {
            if (!RentingPlayers.Contains(playerId))
            {
                throw new RentalHouseException("Вы не арендуете это помещение");
            }

            RentingPlayers.Remove(playerId);
            MySqlCommand command = new MySqlCommand("DELETE FROM `houses_rental_players` WHERE `house_id` = @house_id AND `player_id` = @player_id", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@house_id", HouseId);
            command.Parameters.AddWithValue("@player_id", playerId);
            command.ExecuteNonQuery();
        }
    }
}

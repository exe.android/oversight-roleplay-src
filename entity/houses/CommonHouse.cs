﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using oversight.system;

namespace oversight.entity.houses
{
    class CommonHouse: House
    {
        private bool _IsPlayerSpawningHere;
        public bool IsPlayerSpawningHere => _IsPlayerSpawningHere;

        public void SetIsPlayerSpawningHere(bool newState)
        {
            _IsPlayerSpawningHere = newState;
        }

        public CommonHouse(int houseId, Vector3 enterPosition) : base(houseId, enterPosition)
        {
        }

        public override void OnHousesLoaded()
        {
            LoadCommonHouseData();
        }

        private void LoadCommonHouseData()
        {
            MySqlCommand command = new MySqlCommand("SELECT * FROM `houses_common_data` WHERE `house_id` = @house_id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@house_id", HouseId);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                var data = DbConnection.GetRowData(reader);
                _IsPlayerSpawningHere = (bool) data.Get("player_spawning_here");
                Main.Api.consoleOutput(IsPlayerSpawningHere.ToString());
            }
            reader.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using GTANetworkShared;
using GTANetworkServer;
using oversight.entityGroup;
using oversight.interfaces;
using oversight.system;

namespace oversight.entity
{
    class House: iShapeHandler
    {
        private int _HouseId;
        private NetHandle _Blip;
        private EOwnState _OwnState;
        private Vector3 _EnterPosition;
        private CylinderColShape _Shape;
        private int _OwnerId;
        private eEntityType _OwnerType;
        private int _ComfortLevel;

        public enum EOwnState
        {
            InOwn = 1,
            Free = 2
        }

        public EOwnState OwnState => _OwnState;

        public int HouseId => _HouseId;

        public Vector3 EnterPosition => _EnterPosition;

        public CylinderColShape Shape => _Shape;

        public int OwnerId => _OwnerId;

        public eEntityType OwnerType => _OwnerType;

        public int ComfortLevel => _ComfortLevel;

        public House(int houseId, Vector3 enterPosition)
        {
            _EnterPosition = enterPosition;
            _HouseId = houseId;
            NetHandle blip = Main.Api.createBlip(EnterPosition);
            SetBlip(blip);

            Main.Api.createMarker(1, EnterPosition, new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(2, 2, 2), 255, 0, 255, 0);
            CylinderColShape shape = Main.Api.createCylinderColShape(EnterPosition, 2, 5);
            SetColShape(shape);

            Main.Api.onEntityEnterColShape += onEntityEnterColShapeHandler;
            Main.Api.onEntityExitColShape += onEntityExitColShapeHandler;
            Houses.OnHousesLoaded += OnHousesLoaded;

            Houses.Add(this);
        }

        public void SetColShape(CylinderColShape shape)
        {
            _Shape = shape;
        }

        public void SetOwnState(EOwnState state)
        {
            _OwnState = state;
            SetBlipColor((int)state);
        }

        public void SetOwnerId(int ownerId)
        {
            _OwnerId = ownerId;
        }

        public void SetOwnerType(eEntityType type)
        {
            _OwnerType = type;
        }

        public void SetComfortLevel(int level)
        {
            _ComfortLevel = level;
        }

        public void SetBlipColor(int color)
        {
            Main.Api.setBlipColor(_Blip, color);
        }

        public void SetBlip(NetHandle blip)
        {
            _Blip = blip;
            Main.Api.setBlipSprite(_Blip, (int)Data.Blip.House);
        }

        public void SetEnterPosition(Vector3 position)
        {
            _EnterPosition = position;
        }

        public void onEntityEnterColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.entered);
        }

        public void onEntityExitColShapeHandler(ColShape shape, NetHandle entity)
        {
            OnEntityEnterOrExitColShapeHandler(shape, entity, Data.eMoveType.exit);
        }

        public virtual void OnEntityEnterOrExitColShapeHandler(ColShape shape, NetHandle entity, Data.eMoveType type)
        {
            EntityType entityType = Main.Api.getEntityType(entity);
            if (entityType != EntityType.Player)
                return;

            if (!Shape.containsEntity(entity))
                return;
            Main.Api.sendNotificationToPlayer(Main.Api.getPlayerFromHandle(entity), "Welcome to common house");
        }

        public virtual void UpdateSyncedDataForPlayer(Player player)
        {
            
        }

        public virtual void OnHousesLoaded()
        {
            
        }

        /// <summary>
        /// Возвращает количество энергии, которое восстановится игроку за 10 минут
        /// </summary>
        /// <returns></returns>
        public int GetTickEnergyValue()
        {
            // Дом с комфортом - 5 восстанавливает энергию полностью за 4 часа
            // Дом с комфортом - 10 восстанавливает энергию полностью за 2 часа
            int houseComfortLevel = ComfortLevel;
            // Если умножать на 8 уровень комфорта, то вышесказанное будет верно, 
            // если учесть во внимание то, что максимум энергии - 1000
            int energyValue = houseComfortLevel * 8;
            return energyValue;
        }
    }
}

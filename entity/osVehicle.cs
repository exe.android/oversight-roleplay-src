﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using GTANetworkServer;
using MySql.Data.MySqlClient;
using oversight.entity.props;
using oversight.entityGroup;
using oversight.system;
using oversight.interfaces;
using oversight.inventory;

namespace oversight.entity
{
    struct SVehicle
    {
        public double FuelDrainIdle;
        public double FuelDrainInmove;
        public int FuelTank;
        public double Mileage;
        public double CurrentFuelAmount;
        public Vector3 LastCheckedPosition;
    }

    class OsVehicle: IStoringObject
    {
        private Vehicle _Handler;
        private int _OwnerId;
        private EOwnerType _OwnerType;
        private string _NumberPlate;
        private int _NumberPlateStyle;
        private int _Id;
        private int _Color1;
        private int _Color2;
        public List<CommonProp> AttachedProps = new List<CommonProp>();
        private VehicleHash _ModelHash;

        public SVehicle Data;

        // Указывает на значение ID в таблице vehicles_data
        private uint _dataId;

        public enum EOwnerType
        {
            Player, Fraction, Business
        }

        public VehicleHash ModelHash => _ModelHash;

        public Vehicle Handler => _Handler;

        public int OwnerId => _OwnerId;

        public EOwnerType OwnerType => _OwnerType;

        public int Color1 => _Color1;
        public int Color2 => _Color2;

        public int Id => _Id;

        public void SetVehicleId(int vehicleId)
        {
            _Id = vehicleId;
        }

        public OsVehicle(Vehicle vehicle)
        {
            _Handler = vehicle;
            SetUpPresentation();
        }

        public void SetOwnerId(int ownerId)
        {
            _OwnerId = ownerId;
        }

        public void SetOwnerType(EOwnerType ownerType)
        {
            _OwnerType = ownerType;
        }

        public void SetModelHash(VehicleHash hash)
        {
            _ModelHash = hash;
        }

        public void SetDataId(uint dataId)
        {
            _dataId = dataId;
        }

        public void SetNumberPlate(string plate, int style, bool save)
        {
            _NumberPlate = plate;
            _NumberPlateStyle = style;

            Main.Api.setVehicleNumberPlate(Handler.handle, plate);
            Main.Api.setVehicleNumberPlateStyle(Handler.handle, style);
        }

        public void SavePosition(Vector3 pos, Vector3 heading)
        {
            MySqlCommand command = new MySqlCommand("UPDATE `vehicles_data` SET `spawnpos_x` = @X, `spawnpos_y` = @Y, `spawnpos_z` = @Z, `rotation_x` = @hX, `rotation_y` = @hY, `rotation_z` = @hZ WHERE `id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("X", pos.X);
            command.Parameters.AddWithValue("Y", pos.Y);
            command.Parameters.AddWithValue("Z", pos.Z);

            command.Parameters.AddWithValue("hX", heading.X);
            command.Parameters.AddWithValue("hY", heading.Y);
            command.Parameters.AddWithValue("hZ", heading.Z);

            Main.Api.consoleOutput($"vehicleid {Id}");
            command.Parameters.AddWithValue("id", Id);

            command.ExecuteNonQuery();
        }

        public void ChangeModel(VehicleHash newVehicleHash)
        {
            Vector3 position = Main.Api.getEntityPosition(Handler);
            Vector3 rotation = Main.Api.getEntityRotation(Handler);

            Main.Api.deleteEntity(Handler);
            _Handler = Main.Api.createVehicle(newVehicleHash, position, rotation, Color1, Color2, 0);
        }

        /// <summary>
        /// Прикрепляет трейлер к грузовику
        /// </summary>
        /// <param name="vehicle"></param>
        public void AttachTrailer(OsVehicle vehicle)
        {
            //bool isTrailer = Vehicles.Trailers.Contains((VehicleHash) Main.Api.getEntityModel(vehicle.Entity));
            //if (!isTrailer)
                //return;

            Main.Api.sendNativeToAllPlayers(Hash.ATTACH_VEHICLE_TO_TRAILER, Handler, vehicle.Handler);
        }

        public int GetModel()
        {
            int model = Main.Api.getEntityModel(Handler);
            return model;
        }

        public string GetModelName()
        {
            return Main.Api.getVehicleDisplayName(ModelHash);
        }

        public void SetSyncedData(string key, string data)
        {
            Main.Api.setEntitySyncedData(Handler, key, data);
        }

        public dynamic GetSyncedData(string key)
        {
            return Main.Api.getEntitySyncedData(Handler, key);
        }

        /// <summary>
        /// Может ли автомобиль поместить в себя предмет
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool GetIsItemCanBeCarried(Item item)
        {
            return true;
        }

        /// <summary>
        /// Возвращает возможный переносимый вес
        /// </summary>
        /// <returns></returns>
        public int GetCarryingCapacity()
        {
            return 50;
        }

        /// <summary>
        /// Устанавливает информацию о себе для клиента
        /// </summary>
        public void SetUpPresentation()
        {
            SetSyncedData("STORING_OBJECT_TYPE", "vehicle");
            SetSyncedData("STORING_OBJECT_NAME", GetModelName());
        }
    }
}

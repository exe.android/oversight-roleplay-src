﻿using MySql.Data.MySqlClient;
using oversight.system;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.entityGroup;
using oversight.exceptions;

namespace oversight.entity.bank
{
    class BankAccount
    {
        public static int GetAccountId(int entityId, eEntityType entityType)
        {
            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `bank_cards` WHERE `owner_type` = @owner_type AND `owner_id` = @owner_id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@owner_type", entityType.ToString());
            command.Parameters.AddWithValue("@owner_id", entityId);

            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                var data = DbConnection.GetRowData(reader);
                int accountId = (int) data.Get("id");
                reader.Close();
                return accountId;
            }

            reader.Close();
            throw new BankAccountException("Не найден банковский счёт получателя");
        }

        public static int GetBalance(int id)
        {
            MySqlCommand command = new MySqlCommand("SELECT `balance` FROM `bank_cards` WHERE `id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@id", id);
            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                try
                {
                    reader.Read();
                    Dictionary<string, object> data = DbConnection.GetRowData(reader);
                    reader.Close();
                    return (int) data.Get("balance");
                } catch (System.Data.Common.DbException e)
                {
                    throw new Exception("Невозможно произвести чтение из базы данных");
                }
            }
            return 0;
        }
    }
}

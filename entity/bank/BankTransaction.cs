﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using oversight.exceptions;
using MySql.Data.MySqlClient;
using oversight.entityGroup;
using oversight.system;

namespace oversight.entity.bank
{
    class BankTransaction
    {
        private int _from, _to, _tax;
        private double _amount, _taxAmount;
        private string _comment;

        public BankTransaction(int from, int to, double amount, int tax, string comment)
        {
            _from = from;
            _to = to;
            _tax = tax;
            _amount = amount;
            _taxAmount = amount / 100 * tax;
            _comment = comment;
        }

        private Player GetBankAccountOwner(int ownerId)
        {
            return Players.GetAllPlayers().FirstOrDefault(p => p.GetBankAccountId() == ownerId);
        }

        /// <summary>
        /// Производит транзакцию по ранее заданным параметрам в конструкторе. Выбросит экспешн, если что-то не так.
        /// </summary>
        /// <exception cref="BankTransactionException"></exception>
        public void Execute()
        {
            double currentFromBalance = BankAccount.GetBalance(_from);
            double currentToBalance = BankAccount.GetBalance(_to);
            Player payerEntity = GetBankAccountOwner(_from);

            if (currentFromBalance - _amount < 0)
            {
                if (payerEntity != null)
                {
                    Main.Api.sendNotificationToPlayer(payerEntity.Handler, "~r~Ошибка:\r~w~Недостаточно средств на банковском счёте");
                }
                throw new BankTransactionException("Недостаточно средств");
            }

            MySqlCommand command = new MySqlCommand("UPDATE `bank_cards` SET `balance` = `balance` - @amount WHERE `id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@amount", _amount);
            command.Parameters.AddWithValue("@id", _from);
            command.ExecuteNonQuery();

            command = new MySqlCommand("UPDATE `bank_cards` SET `balance` = `balance` + @amount WHERE `id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@amount", _amount - _taxAmount);
            command.Parameters.AddWithValue("@id", _to);
            command.ExecuteNonQuery();

            if (_taxAmount > 0)
            {
                // Начисляем списанный налог на счёт казны
                command = new MySqlCommand("UPDATE `bank_cards` SET `balance` = `balance` + @tax_amount WHERE `id` = @id LIMIT 1", DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("@tax_amount", _taxAmount);
                command.Parameters.AddWithValue("@id", 1);
                command.ExecuteNonQuery();
            }

            if (payerEntity != null)
            {
                Main.Api.sendNotificationToPlayer(payerEntity.Handler, string.Format("~y~Банк: \r~w~С вашего счёта списано: ~g~${0}~w~ ({1})", _amount, _comment));
                Main.Api.sendNotificationToPlayer(payerEntity.Handler, string.Format("~y~Банк: \r~w~Ваш баланс: ~g~${0}~w~", currentFromBalance - _amount));
            }

            WriteLog();
        }

        private void WriteLog()
        {
            MySqlCommand command = new MySqlCommand("INSERT INTO `bank_operations_logs` VALUES (NULL, @from, @to, @amount, @tax, @tax_amount, @comment, NOW())", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@from", _from);
            command.Parameters.AddWithValue("@amount", _amount);
            command.Parameters.AddWithValue("@to", _to);
            command.Parameters.AddWithValue("@tax", _tax);
            command.Parameters.AddWithValue("@tax_amount", _taxAmount);
            command.Parameters.AddWithValue("@comment", _comment);
            command.ExecuteNonQuery();

            if (_taxAmount > 0)
            {
                // Записываем поступление в казну в логи
                command = new MySqlCommand("INSERT INTO `bank_operations_logs` VALUES (NULL, @from, @to, @amount, @tax, @tax_amount, @comment, NOW())", DbConnection.GetInstance().Connection);
                command.Prepare();

                command.Parameters.AddWithValue("from", _to);
                command.Parameters.AddWithValue("@amount", _taxAmount);
                // ID счёта администрации - 1
                command.Parameters.AddWithValue("to", 1);
                command.Parameters.AddWithValue("tax", 0);
                command.Parameters.AddWithValue("tax_amount", 0);
                command.Parameters.AddWithValue("@comment", "Налог");
                command.ExecuteNonQuery();
            }
        }
    }
}

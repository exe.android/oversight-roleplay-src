﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using GTANetworkServer;
using oversight.entity;
using oversight.entityGroup;
using oversight.system;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Threading;
using oversight.entity.business;
using oversight.exceptions;
using oversight.entity.props;
using oversight.itemstorage;
using oversight.system.chat;

namespace oversight
{
    delegate void PlayerRentingHouseEvent(Player player);

    class Main: Script
    {
        public static API Api;

        // EVENTS
        public static event PlayerRentingHouseEvent OnPlayerRentingHouse;
        public static event PlayerHiringToJobEvent OnPlayerHiringToJob;
        public static event PlayerOpeningInventory OnPlayerOpeningInventory;
        public static event PlayerUsingItemSelf OnPlayerUsingItemSelf;
        public static event PlayerChangingChatType OnPlayerChangingChatType;
        // =======================================================

        public Main()
        {
            Api = API;
            API.onResourceStart += OnResourceStart;
            API.onPlayerConnected += OnPlayerConnected;
            API.onPlayerDisconnected += OnPlayerDisconnected;
            API.onClientEventTrigger += OnClientEventTrigger;

            API.onEntityEnterColShape += Props.onEntityEnterColShapeHandler;
            API.onEntityExitColShape += Props.onEntityExitColShapeHandler;

            API.onResourceStop += OnResourceStop;
        }

        public void OnPlayerDisconnected(Client user, string reason)
        {
            Player player = Players.Get(user);
            player.SavePlayer();
            Players.Remove(user);
        }

        public void OnPlayerConnected(Client player)
        {
            API.triggerClientEvent(player, "showLoginForm");
            Player account = new Player(player);
            Players.Add(account);
            // bool result = await Auth.getInstance().auth("Insight", "123", account);

            API.setEntityPosition(player, new Vector3(-1812.974, 4711.104, 50.62144));
            API.freezePlayer(player, true);
            API.setEntityCollisionless(player, true);
            API.setEntityInvincible(player, true);
            API.setEntityTransparency(player, 0);
        }

        public void OnResourceStart()
        {
            API.consoleOutput("Oversight Roleplay Loading...");
            ServerWorker server = ServerWorker.GetInstance();

            server.LoadServer();
            Main.Api.setTime(18, 00);
        }

        public void OnClientEventTrigger(Client user, string eventName, params object[] args)
        {
            Player player = Players.Get(user);

            switch (eventName)
            {
                case "processLogIn":
                    {
                        Player account = Players.Get(user);
                        bool result = Auth.GetInstance().auth(args[0].ToString(), args[1].ToString(), account);
                        API.triggerClientEvent(user, "logInResult", result);
                        break;
                    }

                case "processRegistration":
                    {
                        try
                        {
                            Auth.GetInstance().Register(args[0].ToString(), args[1].ToString(), args[2].ToString(), args[3].ToString(), args[4].ToString());
                        } catch (ArgumentException)
                        {
                            API.triggerClientEvent(user, "registerResult", false, "Указаны не все поля");
                        } catch (RegistrationException e)
                        {
                            API.triggerClientEvent(user, "registerResult", false, e.Message.ToString());
                        } catch (System.IndexOutOfRangeException)
                        {
                            API.triggerClientEvent(user, "registerResult", false, "Указаны не все поля");
                        }
                        break;
                    }

                case "vehicleEngineStateChanged":
                    {
                        Vehicles.OnVehicleEngineStateChanged(user, (bool) args[0]);
                        break;
                    }
                case "savePedComponents":
                    {
                        Player account = Players.Get(user);
                        PedCustomizer.SavePed(account);
                        API.triggerClientEvent(user, "spawnNormally");
                        API.setEntityPositionFrozen(user, false);
                        API.setEntityDimension(user, 0);
                        break;
                    }

                case "saveCamera":
                    {
                        try
                        {
                            Vector3 pos = (Vector3) args[0];
                            Vector3 rot = (Vector3) args[1];

                            string str =
                                $"pos: new Vector3({pos.X.ToString().Replace(",", ".")},{pos.Y.ToString().Replace(",", ".")},{pos.Z.ToString().Replace(",", ".")});";
                            using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(@"C:\Users\Insight\Desktop\cam.txt", true))
                            {
                                file.WriteLine(str);
                            }

                            str =
                                $"rot: new Vector3({rot.X.ToString().Replace(",", ".")},{rot.Y.ToString().Replace(",", ".")},{rot.Z.ToString().Replace(",", ".")});";
                            using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(@"C:\Users\Insight\Desktop\cam.txt", true))
                            {
                                file.WriteLine(str);
                            }
                        }
                        catch (ArgumentNullException e)
                        {
                            API.sendNotificationToPlayer(user, "Не удалось сохранить", true);
                        }
                        API.sendNotificationToPlayer(user, "Камера сохранена", true);
                        break;
                    }

                case "playerPressedKey":
                    {
                        switch ((string) args[0])
                        {
                            case "Q":
                                {
                                    Props.CheckPlayerCuttingTree(player);
                                    break;
                                }
                            case "H":
                                {
                                    Props.CheckPlayerLoadingLogPileToTrailer(player);
                                    break;
                                }

                            case "E":
                            {
                                Props.CheckPlayerTryingAttachLogToVehicle(player);
                                break;
                            }

                            case "Y":
                            {
                                Props.CheckPlayerTryingDettachLogFromVehicle(player);
                                break;
                            }
                        }
                        break;
                    }

                case "playerHiringToWork":
                    try
                    {
                        string jobName = (string) args[0];
                        OnPlayerHiringToJob(player, jobName);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        
                    }
                    break;

                case "playerRentingHouse":
                    OnPlayerRentingHouse(player);
                    break;

                case "playerOpeningInventory":
                    OnPlayerOpeningInventory(player);
                    break;

                case "playerUsingItemSelf":
                    OnPlayerUsingItemSelf(player, (int) args[0]);
                    break;

                case "playerChangingChatType":
                    OnPlayerChangingChatType(player, (string) args[0]);
                    break;
            }
        }

        [Command("Save")]
        public void SavePlayerPositionToFile(Client sender)
        {
            Vector3 currentPlayerPosition = API.getEntityPosition(sender);
            try
            {
                string str =
                    $"treePoints.Add(new Vector3({currentPlayerPosition.X.ToString().Replace(",", ".")}f,{currentPlayerPosition.Y.ToString().Replace(",", ".")}f,{currentPlayerPosition.Z.ToString().Replace(",", ".")}f));";
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\Insight\Desktop\pos.txt", true))
                {
                    file.WriteLine(str);
                }
            } catch (ArgumentNullException e)
            {
                API.sendNotificationToPlayer(sender, "Не удалось сохранить", true);
            }
            API.sendNotificationToPlayer(sender, "Позиция сохранена", true);
        }

        [Command("tp")]
        public void TeleportToPlayer(Client sender, string locationName)
        {
            Vector3 locationPosition = Data.TeleportLocations.Get(locationName);
            if (locationPosition == null)
            {
                Main.Api.sendChatMessageToPlayer(sender, "~y~[SERVER]~s~: Такая локация не найдена");
            }
            else
            {
                API.setEntityPosition(sender, locationPosition);
            }
        }

        [Command("SavePos")]
        public void SavePlayerPosition(Client sender)
        {
            MySqlCommand command = new MySqlCommand("UPDATE `users` SET `spawnpos_x` = @X, `spawnpos_y` = @Y, `spawnpos_z` = @Z WHERE `id` = @Id LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            Vector3 currentPlayerPosition = API.getEntityPosition(sender);
            command.Parameters.AddWithValue("@X", currentPlayerPosition.X);
            command.Parameters.AddWithValue("@Y", currentPlayerPosition.Y);
            command.Parameters.AddWithValue("@Z", currentPlayerPosition.Z);
            command.Parameters.AddWithValue("@Id", Players.Get(sender).Id);

            command.ExecuteNonQuery();
            API.sendNotificationToPlayer(sender, "Ваша позиция сохранена в базе");
        }

        [Command("weapon")]
        public void GetWeapon(Client sender)
        {
            API.givePlayerWeapon(sender, WeaponHash.Hatchet, 10, true, true);
        }

        [Command("veh")]
        public void CreateVeh(Client sender)
        {
            Vector3 pos = API.getEntityPosition(sender);
            pos.X += 1;
            pos.Y += 1;
            pos.Z += 1;
            API.createVehicle(VehicleHash.Volatus, pos, new Vector3(0,0,0), 30, 24);
        }

        [Command("park")]
        public void ParkVeh(Client sender)
        {
            NetHandle vehHandle = API.getPlayerVehicle(sender);
            OsVehicle vehicle = Vehicles.Get(vehHandle);

            Vector3 pos = Api.getEntityPosition(vehHandle);
            Vector3 heading = Api.getEntityRotation(vehHandle);

            vehicle.SavePosition(pos, heading);
            API.sendNotificationToPlayer(sender, "Позиция транспорта сохранена в базу данных");
        }

        [Command("createVehicle")]
        public void CreateVehicle(Client sender, int hash, string owner, int ownerId)
        {
            if (!Main.Api.isPlayerInAnyVehicle(sender))
            {
                Main.Api.sendNotificationToPlayer(sender, "Вы должны находиться в машине");
                return;
            }

            MySqlCommand command = new MySqlCommand("INSERT INTO `vehicles` (`id`, `owner_type`, `owner_id`, `model`) VALUES (NULL, @owner_type, @owner_id, @model)", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@owner_type", owner);
            command.Parameters.AddWithValue("@owner_id", ownerId);
            command.Parameters.AddWithValue("@model", hash);

            command.ExecuteNonQuery();
            int newVehicleId = (int) command.LastInsertedId;
            command.Dispose();

            NetHandle vehicleHandle = Main.Api.getPlayerVehicle(sender);
            Vector3 position = API.getEntityPosition(vehicleHandle);
            Vector3 rotation = API.getEntityRotation(vehicleHandle);

            command = new MySqlCommand("INSERT INTO `vehicles_data` VALUES (NULL, @vehicleId, @spawnPosX, @spawnPosY, @spawnPosZ, @rotationX, @rotationY, @rotationZ, 1, 1, 'RLP', 1, 10, 0)", DbConnection.GetInstance().Connection);
            command.Parameters.AddWithValue("@vehicleId", newVehicleId);
            command.Parameters.AddWithValue("@spawnPosX", position.X);
            command.Parameters.AddWithValue("@spawnPosY", position.Y);
            command.Parameters.AddWithValue("@spawnPosZ", position.Z);

            command.Parameters.AddWithValue("@rotationX", rotation.X);
            command.Parameters.AddWithValue("@rotationY", rotation.Y);
            command.Parameters.AddWithValue("@rotationZ", rotation.Z);
            command.ExecuteNonQuery();
            command.Dispose();

            API.sendChatMessageToPlayer(sender, "Автомобили будут перезагружены через 5 сек");
            Action action = Vehicles.ReloadVehicles;
            action.DelayFor(5000);
        }

        private void OnResourceStop()
        {

        }
    }
}

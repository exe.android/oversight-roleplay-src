﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace oversight.system
{
    class Config
    {
        const string ResourceFolder = "resources/oversight/";

        private XmlDocument _xml;

        private Config() { }

        private void LoadXml()
        {
            if (_xml == null)
            {
                _xml = new XmlDocument();
                try
                {
                    _xml.Load(ResourceFolder + "settings.xml");

                } catch (XmlException e)
                {
                    Main.Api.consoleOutput($"[XML] Error while loading settings.xml: {e.Message.ToString()} ");
                }
            }
        }
        
        /// <summary>
        /// Отдает значение из конфигурационного файла по категории и имени значения
        /// </summary>
        /// <param name="category">Категория</param>
        /// <param name="name">Имя значения</param>
        /// <returns>Строку со значением, либо null</returns>
        public static string GetValue(string category, string name)
        {
            // Подгружаем XML, если не загружен
            Config config = new Config();
            config.LoadXml();

            // Выбираем значение
            try
            {
                XmlNode node = config._xml.DocumentElement.SelectSingleNode($"/{category}/{name}");
                return node.InnerText;
            } catch (NullReferenceException  e)
            {
                Main.Api.consoleOutput($"[XML] No entry for XPATH way: /{category}/{name}");
                return null;
            }            
        }
    }
}

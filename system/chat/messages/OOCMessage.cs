﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;

namespace oversight.system.chat.messages
{
    class OOCMessage: BaseChatMessage
    {
        public override string GetFormattedMessage(string message, Player byPlayer)
        {
            return string.Format("{0}: (( {1} ))", byPlayer.GetName(), message);
        }
    }
}

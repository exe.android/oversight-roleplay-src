﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.system.chat.messages
{
    class MessageFabric
    {
        public static BaseChatMessage GetChatMessageHandler(eChatType chatType)
        {
            switch (chatType)
            {
                case eChatType.common:
                    return new CommonMessage();
                case eChatType.me:
                    return new MeMessage();
                case eChatType.ooc:
                    return new OOCMessage();
                case eChatType.shout:
                    return new ShoutMessage();
            }
            return new CommonMessage(); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;

namespace oversight.system.chat.messages
{
    class CommonMessage: BaseChatMessage
    {
        public override string GetFormattedMessage(string message, Player byPlayer)
        {
            return string.Format("{0} говорит: {1}", byPlayer.GetName(), message); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;

namespace oversight.system.chat.messages
{
    class MeMessage: BaseChatMessage
    {
        public override string GetFormattedMessage(string message, Player byPlayer)
        {
            return string.Format("<span style='color: #c293e7;'>*{0} {1}</span>", Main.Api.getPlayerName(byPlayer.Handler), message);
        }
    } 
}

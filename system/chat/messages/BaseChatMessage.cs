﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using oversight.entity;

namespace oversight.system.chat.messages
{
    abstract class BaseChatMessage
    {
        public abstract string GetFormattedMessage(string message, Player byPlayer);
    }
}

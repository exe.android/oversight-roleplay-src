﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using oversight.entity;
using oversight.entityGroup;
using oversight.system.chat.messages;

namespace oversight.system.chat
{
    public enum eChatType
    {
        common,
        ooc,
        me,
        doing,
        shout
    }

    delegate void PlayerChangingChatType(Player player, string chatTypeString);

    /// <summary>
    /// Класс отвечает за работу с чатом
    /// </summary>
    class ChatWorker
    {
        private static Dictionary<int, eChatType> PlayerChatTypes = new Dictionary<int, eChatType>();

        public static void Start()
        {
            Main.Api.onChatMessage += OnChatMessage;
            Main.OnPlayerChangingChatType += OnPlayerChangingChatType;
        }

        /// <summary>
        /// Callback на попытку отправить сообщение в чат
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <param name="e"></param>
        private static void OnChatMessage(Client player, string message, CancelEventArgs e)
        {
            Player oPlayer = Players.Get(player);
            if (oPlayer == null)
            {
                e.Cancel = true;
                return;
            }

            SendMessageToNearbyPlayers(oPlayer, message, GetPlayerCurrentChatType(oPlayer));
            e.Cancel = true;
        }

        /// <summary>
        /// Игрок меняет тип сообщения
        /// </summary>
        /// <param name="player"></param>
        /// <param name="chatTypeString"></param>
        private static void OnPlayerChangingChatType(Player player, string chatTypeString)
        {
            SetPlayerCurrentChatType(player, (eChatType) Enum.Parse(typeof(eChatType), chatTypeString));
        }

        /// <summary>
        /// Возвращает текущий выбранный тип сообщений
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        private static eChatType GetPlayerCurrentChatType(Player player)
        {
            if (PlayerChatTypes.ContainsKey(player.Id))
            {
                return PlayerChatTypes.First(row => row.Key.Equals(player.Id)).Value;
            }
            return eChatType.common; 
        }

        /// <summary>
        /// Устанавливает текущий тип сообщений
        /// </summary>
        /// <param name="player"></param>
        /// <param name="type"></param>
        private static void SetPlayerCurrentChatType(Player player, eChatType type)
        {
            PlayerChatTypes.Set(player.Id, type);
        }

        /// <summary>
        /// Отправляет сообщение ближайшим игрокам
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <param name="chatType"></param>
        private static void SendMessageToNearbyPlayers(Player player, string message, eChatType chatType)
        {
            Vector3 playerPosition = Main.Api.getEntityPosition(player.Handler);
            List<Player> players = Players.GetPlayersAtRangeOfPoint(playerPosition, 30);
            foreach (Player playerAtRange in players)
            {
                string formattedMessage = GetFormattedMessageWithChatType(message, player, chatType);
                Main.Api.sendChatMessageToPlayer(playerAtRange.Handler, formattedMessage);
            }
        }

        /// <summary>
        /// Возвращает отформатированное, в зависимости от выбранного типа сообщений, сообщение
        /// </summary>
        /// <param name="message"></param>
        /// <param name="chatType"></param>
        /// <returns></returns>
        private static string GetFormattedMessageWithChatType(string message, Player byPlayer, eChatType chatType)
        {
            BaseChatMessage messageObject = MessageFabric.GetChatMessageHandler(chatType);
            Main.Api.consoleOutput(messageObject.GetType().ToString());
            return messageObject.GetFormattedMessage(message, byPlayer);
        }
    }
}

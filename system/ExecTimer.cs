﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.system
{
    class ExecTimer
    {
        private Stopwatch _watcher;
        private long _endTime;

        public void Start()
        {
            _watcher = Stopwatch.StartNew();
        }

        public long Stop()
        {
            _watcher.Stop();
            _endTime = _watcher.ElapsedMilliseconds;
            return _endTime;
        }
    }
}

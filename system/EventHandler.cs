﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.system
{
    public static class EventHandler
    {
        public static async void DelayFor(this Action act, int delayInMilliseconds)
        {
            await Task.Delay(delayInMilliseconds);
            act();
        }
    }
}

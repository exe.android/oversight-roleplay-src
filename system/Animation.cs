﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.system
{
    class Animation
    {
        public string Key;
        public string Dictionary;
        public string AnimName;

        public Animation(string key, string dictionary, string animName)
        {
            Key = key;
            Dictionary = dictionary;
            AnimName = animName;
        }
    }
}

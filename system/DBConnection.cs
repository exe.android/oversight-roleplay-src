﻿using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Clusters;

namespace oversight.system
{
    public class DbConnection
    {
        private DbConnection() {}

        public string Username;
        public string Password;
        public string DbName;
        public string Host;

        private static DbConnection _instance = null;
        private MySqlConnection _connection = null;

        private static IMongoClient _mongoClient;
        private static IMongoDatabase _mongoDatabase;

        public IMongoDatabase MongoDatabase => _mongoDatabase;

        public MySqlConnection Connection
        {
            get { return _connection; }
        }

        public static DbConnection GetInstance()
        {
            if (_instance == null)
                _instance = new DbConnection();
            return _instance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ServerException"></exception>
        /// <returns></returns>
        public bool Connect()
        {
            bool result = true;
            if (Connection == null)
            {
                if (string.IsNullOrEmpty(DbName))
                    result = false;
                string connstring = string.Format("Server={3}; database={0}; UID={1}; password={2}; CharSet=utf8;", DbName, Username, Password, Host);
                _connection = new MySqlConnection(connstring);
                try
                {
                    _connection.Open();
                    Main.Api.consoleOutput($"[MYSQL] Successfully connected to {Host}");

                    _mongoClient = new MongoClient();
                    _mongoDatabase = _mongoClient.GetDatabase("oversight");

                } catch (MySqlException e)
                {
                    Main.Api.consoleOutput($"[MYSQL] {e.Message.ToString()}");
                    throw new ServerException(e.Message.ToString());
                }

                result = true;
            }

            return result;
        }

        public void Close()
        {
            Connection.Close();
        }

        public static Dictionary<string, object> GetRowData(System.Data.Common.DbDataReader reader)
        {
            int currentCol = 0;
            Dictionary<string, object> rowData = new Dictionary<string, object>();

            while (currentCol < reader.FieldCount)
            {
                string currentColName = reader.GetName(currentCol);
                if (rowData.ContainsKey(currentColName))
                {
                    currentCol++;
                    continue;
                }

                rowData.Add(currentColName, reader.GetValue(currentCol));
                currentCol++;
            }

            return rowData;
        }
    }
}
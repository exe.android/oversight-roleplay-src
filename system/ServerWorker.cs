﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using GTANetworkServer;
using oversight.entity;
using MySql.Data.MySqlClient;
using oversight.entity.business;
using oversight.entityGroup;
using oversight.entity.props;
using oversight.inventory;
using oversight.itemstorage;
using oversight.system.chat;

namespace oversight.system
{
    class ServerWorker
    {
        enum ServerLoadingState
        {
            WaitingForLoad, Loading, Loaded, Error
        }

        private ServerLoadingState _loadingState;
        private static ServerWorker _instance;

        private ServerWorker()
        {

        }

        public static ServerWorker GetInstance()
        {
            if (_instance == null)
                _instance = new ServerWorker();
            return _instance;
        }

        /// <summary>
        /// Загружает сервер (объекты, авто, дома и.т.п)
        /// </summary>
        public void LoadServer()
        {
            if (_loadingState != ServerLoadingState.WaitingForLoad)
            {
                Main.Api.consoleOutput("[SEVERE] Server already loading or loaded");
                return;
            }

            ExecTimer timer = new ExecTimer();
            timer.Start();
            Data.Init();

            _loadingState = ServerLoadingState.Loading;

            DbConnection dbConnection = DbConnection.GetInstance();
            dbConnection.Username = Config.GetValue("database", "user");
            dbConnection.Password = Config.GetValue("database", "password");
            dbConnection.Host = Config.GetValue("database", "host");
            dbConnection.DbName = Config.GetValue("database", "dbName");

            try
            {
                dbConnection.Connect();
            } catch (ServerException e)
            {
                Main.Api.consoleOutput("[SEVERE] Loading stopped due no MySQL connection");
                _loadingState = ServerLoadingState.Error;
                return;
            }

            Vehicles.LoadVehicles(false);
            Businesses.LoadBusinesses();
            Houses.LoadHouses();
            ItemsTemplateWorker.InitInventory();
            ItemsCollection.LoadCollection();

            PedCustomizer.Init();
            Props.LoadProps();

            long execTime = timer.Stop();

            Main.Api.consoleOutput($"[SERVER] Server was loaded in {execTime} ms.");
            _loadingState = ServerLoadingState.Loaded;

            // Запускаем потоки
            Vehicles.StartChekingMileageThread();
            Props.StartDeletingCommonGtaObjectsThread();
            Houses.StartHandlingThread();
            Players.StartListeners();
            ItemsEventsWorker.StartListener();
            ChatWorker.Start();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using System.Security.Cryptography;

namespace oversight.system
{
    class Utils
    {
        public static double RangeOfTwoVectors(Vector3 vector1, Vector3 vector2)
        {
            return Math.Sqrt(Math.Pow(vector2.X - vector1.X, 2) + Math.Pow(vector2.Y - vector1.Y, 2) + Math.Pow(vector2.Z - vector1.Z, 2));
        }

        public static String GetHashedString(String value)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return String.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(value))
                  .Select( item => item.ToString("x2")));
            } 
        }

        public static bool IsPedLooksOnEntity(NetHandle ped, NetHandle entity)
        {
            return false;
        }
    }
}

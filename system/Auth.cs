﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using oversight.entity;
using oversight.entityGroup;
using GTANetworkServer;
using GTANetworkShared;
using System.Text.RegularExpressions;
using oversight.exceptions;
using System.Net.Mail;
using System.Security.Cryptography;

namespace oversight.system
{
    class Auth
    {
        private static Auth _instance;
        

        private Auth() { }

        public static Auth GetInstance()
        {
            if (_instance == null)
                _instance = new Auth();
            return _instance;
        }

        public async Task Register(string login, string password, string email, string name, string surname)
        {
            string namePattern = @"[A-z]{3,10}";
            Regex reg = new Regex(namePattern);
            if (!reg.IsMatch(name) || !reg.IsMatch(surname))
            {
                throw new RegistrationException("Имя или фамилия должны быть длиннее 3х и короче 11 символов, а так же не могут содержать цифры и символы");
            }
            try
            {
                MailAddress address = new MailAddress(email);
            } catch (FormatException)
            {
                throw new RegistrationException("Неверный формат e-mail");
            }
            if (password.Length < 5 || login.Length > 50)
                throw new RegistrationException("Пароль не может быть короче 5-ти или длиннее 30-ти символов");
            if (login.Length < 3 || login.Length > 15)
                throw new RegistrationException("Логин не может быть короче 2-х или длиннее 15-ти символов");
            string hashedPassword = Utils.GetHashedString(password);

            if (Players.IsPlayerWithNameExists(name, surname))
                throw new RegistrationException("Игрок с таким именем и фамилией уже существует");

            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `users` WHERE `username` = @username LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@username", login);
            System.Data.Common.DbDataReader reader = await command.ExecuteReaderAsync();
            
            if (reader.HasRows)
            {
                reader.Close();
                throw new RegistrationException("Такой логин уже занят");
            }
            reader.Close();

            command = new MySqlCommand("INSERT INTO `users` (`id`, `username`, `name`, `surname`, `password`, `email`) VALUES (NULL, @username, @name, @surname, @password, @email)", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@username", login);
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@surname", surname);
            command.Parameters.AddWithValue("@password", hashedPassword);
            command.Parameters.AddWithValue("@email", email);

            await command.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Авторизует игрока по логину и паролю
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public bool auth(string login, string password, Player player)
        {
            MySqlCommand command = new MySqlCommand("SELECT `id` FROM `users` WHERE `username` = @Username AND `password` = @Password LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();

            command.Parameters.AddWithValue("@Username", login);
            command.Parameters.AddWithValue("@Password", Utils.GetHashedString(password));

            System.Data.Common.DbDataReader reader = command.ExecuteReader();
            bool result = reader.HasRows;
            if (result)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> data = DbConnection.GetRowData(reader);

                    int playerId = (int)data.Get("id");
                    player.Id = playerId;
                }

                reader.Close();
                Players.LoadAccount(player);
            }
            reader.Close();
            return result;
        }
    }

}

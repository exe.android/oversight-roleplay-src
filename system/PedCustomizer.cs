﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkShared;
using System.Dynamic;
using System.Web.Script.Serialization;
using oversight.entity;
using MySql.Data.MySqlClient;

namespace oversight.system
{
    class PedCustomizer
    {
        private static List<string> _componentsList = new List<string>();

        public static void InitializePedComponents(Player player)
        {
            NetHandle ent = player.Handler;
            Main.Api.setEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA", true);

            Main.Api.setEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_SHAPE_MIX", 0f);
            Main.Api.setEntitySyncedData(ent, "GTAO_SKIN_MIX", 0f);
            Main.Api.setEntitySyncedData(ent, "GTAO_HAIR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_HAIR_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_EYE_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_EYEBROWS", 0);

            //main.api.setEntitySyncedData(ent, "GTAO_MAKEUP", 0); // No lipstick by default. 
            //main.api.setEntitySyncedData(ent, "GTAO_LIPSTICK", 0); // No makeup by default.

            Main.Api.setEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_MAKEUP_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2", 0);

            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_TSHIRT_VARIATION", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_CLOTHES_VARIATION", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_TORSO", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG", 0);
            Main.Api.setEntitySyncedData(ent, "GTAO_PED_COMPONENT_LEG_VARIATION", 0);

            var list = new float[21];


            for (var i = 0; i < 21; i++)
            {
                list[i] = 0;
            }

            Main.Api.setEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST", list);
        }

        public static void RemovePedComponents(NetHandle ent)
        {
            Main.Api.setEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA", false);
            Main.Api.resetEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID");
            Main.Api.resetEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID");
            Main.Api.resetEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID");
            Main.Api.resetEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID");
            Main.Api.resetEntitySyncedData(ent, "GTAO_SHAPE_MIX");
            Main.Api.resetEntitySyncedData(ent, "GTAO_SKIN_MIX");
            Main.Api.resetEntitySyncedData(ent, "GTAO_HAIR_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_EYE_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_EYEBROWS");
            Main.Api.resetEntitySyncedData(ent, "GTAO_MAKEUP");
            Main.Api.resetEntitySyncedData(ent, "GTAO_LIPSTICK");
            Main.Api.resetEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_MAKEUP_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR");
            Main.Api.resetEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2");
            Main.Api.resetEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2");
            Main.Api.resetEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2");
            Main.Api.resetEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST");
        }

        public static void Init()
        {
            _componentsList.Add("GTAO_SHAPE_FIRST_ID");
            _componentsList.Add("GTAO_SHAPE_SECOND_ID");
            _componentsList.Add("GTAO_SKIN_FIRST_ID");
            _componentsList.Add("GTAO_HAIR");
            _componentsList.Add("GTAO_HAIR_COLOR");
            _componentsList.Add("GTAO_HAIR_HIGHLIGHT_COLOR");
            _componentsList.Add("GTAO_EYE_COLOR");
            _componentsList.Add("GTAO_PED_COMPONENT_TSHIRT");
            _componentsList.Add("GTAO_PED_COMPONENT_TSHIRT_VARIATION");
            _componentsList.Add("GTAO_PED_COMPONENT_CLOTHES");
            _componentsList.Add("GTAO_PED_COMPONENT_CLOTHES_VARIATION");
            _componentsList.Add("GTAO_PED_COMPONENT_TORSO");
            _componentsList.Add("GTAO_PED_COMPONENT_LEG");
            _componentsList.Add("GTAO_PED_COMPONENT_LEG_VARIATION");
        }
        
        public static async void LoadPed(Player player)
        {
           
            MySqlCommand command = new MySqlCommand("SELECT * FROM `users_customize` WHERE `player_id` = 1 LIMIT 1", DbConnection.GetInstance().Connection);
            command.Prepare();
            command.Parameters.AddWithValue("@player_id", player.Id);
            System.Data.Common.DbDataReader reader = await command.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                await reader.ReadAsync();

                Dictionary<string, object> data = DbConnection.GetRowData(reader);

                NetHandle ent = player.Handler;
                Main.Api.setEntitySyncedData(ent, "GTAO_HAS_CHARACTER_DATA", true);

                foreach (string component in _componentsList)
                {
                    Main.Api.setEntitySyncedData(ent, component, data.Get(component));
                }

                var list = new string[21];
                var shapes = new float[21];
                var listString = (string)data.Get("SHAPES");

                char delimiter = ';';
                list = listString.Split(delimiter);

                for (int i = 0; i < 21; i++)
                {
                    shapes[i] = float.Parse(list[i]);
                }

                Main.Api.setEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST", shapes);
                UpdatePlayerComponents(player);
            }
            reader.Close();
        }

        public static async void SavePed(Player player)
        {
            NetHandle ent = player.Handler;
            var list = Main.Api.getEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST");
            string listString = string.Join(";", list);

            // Строим строку UPDATE запроса
            string query = null;
            foreach (string component in _componentsList)
            {
                string concatString = string.Format("`{0}` = @{0}, ", component);
                query += concatString;
            }

            string fullQuery = "INSERT INTO `users_customize` SET " +
                    query +
                    " `SHAPES` = @SHAPES," +
                    " `player_id` = @player_id";

            MySqlCommand command = 
                new MySqlCommand(fullQuery, DbConnection.GetInstance().Connection);

            command.Prepare();

            foreach (string component in _componentsList)
            {
                Main.Api.consoleOutput(component + " - " + (int)Main.Api.getEntitySyncedData(ent, component));
                command.Parameters.AddWithValue("@" + component, (int)Main.Api.getEntitySyncedData(ent, component));
            }
            command.Parameters.AddWithValue("@SHAPES", listString);
            command.Parameters.AddWithValue("@player_id", player.Id);

            await command.ExecuteNonQueryAsync();
            Main.Api.sendNotificationToPlayer(player.Handler, "Внешность персонажа сохранена");
        }

        public static bool IsPlayerComponentsValid(NetHandle ent)
        {
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SHAPE_FIRST_ID")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SHAPE_SECOND_ID")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SKIN_FIRST_ID")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SKIN_SECOND_ID")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SHAPE_MIX")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_SKIN_MIX")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_HAIR_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_EYE_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_EYEBROWS")) return false;
            //if (!main.api.hasEntitySyncedData(ent, "GTAO_MAKEUP")) return false; // Player may have no makeup
            //if (!main.api.hasEntitySyncedData(ent, "GTAO_LIPSTICK")) return false; // Player may have no lipstick
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_MAKEUP_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_EYEBROWS_COLOR2")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_MAKEUP_COLOR2")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_LIPSTICK_COLOR2")) return false;
            if (!Main.Api.hasEntitySyncedData(ent, "GTAO_FACE_FEATURES_LIST")) return false;

            return true;
        }

        public static void UpdatePlayerComponents(Player player)
        {
            NetHandle ent = player.Handler;
            Main.Api.triggerClientEventForAll("UPDATE_CHARACTER");
            Main.Api.consoleOutput("HAIR: " + (int)Main.Api.getEntitySyncedData(ent, "GTAO_HAIR"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace oversight.exceptions
{
    [Serializable]
    public class ItemChangeAmountException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public ItemChangeAmountException()
        {
        }

        public ItemChangeAmountException(string message) : base(message)
        {
        }

        public ItemChangeAmountException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ItemChangeAmountException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}

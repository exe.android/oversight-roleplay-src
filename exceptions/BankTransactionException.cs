﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oversight.exceptions
{
    [Serializable]
    public class BankTransactionException : Exception
    {
        public BankTransactionException() { }
        public BankTransactionException(string message) : base(message) { }
        public BankTransactionException(string message, Exception inner) : base(message, inner) { }
        protected BankTransactionException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
